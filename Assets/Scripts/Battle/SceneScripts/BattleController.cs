﻿using UnityEngine;
using System.Collections;

public class BattleController : MonoBehaviour
{
	ControllerManager controllerManager;
	
	public ControllerManager.Key _menuCharacterLeftKey = ControllerManager.Key.LEFT;
	
	public ControllerManager.Key _menuCharacterRightKey = ControllerManager.Key.RIGHT;
	
	public ControllerManager.Key _menuItemNextKey = ControllerManager.Key.DOWN;
	
	public ControllerManager.Key _menuItemPrevKey = ControllerManager.Key.UP;
	
	public ControllerManager.Key _menuItemSelectKey = ControllerManager.Key.ACTION;
	
	public ControllerManager.Key _cancelKey = ControllerManager.Key.CANCEL;

	// Use this for initialization
	void Start ()
	{
		controllerManager = GameObject.FindGameObjectWithTag("SceneManager").GetComponent<ControllerManager>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(controllerManager.getState(gameObject,_menuCharacterLeftKey).isOnStateMax(State.DOWN,0)) GetComponent<BattleSceneManager>().nextCharacter();
		if(controllerManager.getState(gameObject,_menuCharacterRightKey).isOnStateMax(State.DOWN,0)) GetComponent<BattleSceneManager>().prevCharacter();
		
		if(controllerManager.getState(gameObject,_menuItemNextKey).isOnStateMax(State.DOWN,0)) GetComponent<BattleMenu>().nextItem();
		if(controllerManager.getState(gameObject,_menuItemPrevKey).isOnStateMax(State.DOWN,0)) GetComponent<BattleMenu>().prevItem();

		if(controllerManager.getState(gameObject,_menuItemSelectKey).isOnStateMax(State.DOWN,0)) GetComponent<BattleMenu>().select();
		if(controllerManager.getState(gameObject,_cancelKey).isOnStateMax(State.DOWN,0)) GetComponent<BattleSceneManager>().undo();
	}
}
