﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Reflection;
using System.Collections.Generic;

#if UNITY_EDITOR

using UnityEditor;

#endif

public class ActionTypeBehaviorCallScript : ActionTypeBehavior
{
	public ActionTypeBehaviorCallScript(InspectorEventComponent.InspectorEvent.InspectorEventAction iec) : base(iec)
	{
		this.propertyHeight = 16*2;
	}
	
	public override void resize()
	{
		Array.Resize(ref getInspectorEventAction().extraString,1);
		Array.Resize(ref getInspectorEventAction().extraGameObjects,1);
	}
	
	public GameObject getGameObject()
	{
		return safeGetGameObject(0);
	}
	
	public string getEventName()
	{
		return safeGetString(0);
	}
	
	public GameObject setGameObject(GameObject gb)
	{
		return safeSetGameObject(0,gb);
	}
	
	public string setEventName(string eventName)
	{
		return safeSetString(0, eventName);
	}
	
	public EventComponent getEventComponent()
	{
		EventComponent[] components = getGameObject().GetComponents<EventComponent>();
		string eventName = getEventName();
		
		foreach(EventComponent component in components)
		{
			if(component.eventName.Equals(eventName))
			{
				return component;
			}
		}
		
		return null;
	}
	
	public override string ToString()
	{
		bool error = false;
		
		GameObject gb = getGameObject();
		
		string gbName = "";
		string eventName = getEventName();
		
		if(eventName.Equals(""))
		{
			error = true;
			eventName = "UNDEFINED_SCRIPT";
		}
		
		if(gb == null)
		{
			error = true;
			gbName = "UNDEFINED_GAMEOBJECT";
		}
		else
		{
			gbName = gb.name;
		}
		
		string ret = "Call script " + eventName + " from object " + gbName;
		
		if(error)
		{
			ret = "(!) " + ret;
		}
		
		return ret;
	}
	
	public override bool run(InspectorEventComponent component)
	{
		EventComponent component2 = getEventComponent();
		
		if(component2 is InspectorEventComponent)
		{
			InspectorEventComponent iComponent = (InspectorEventComponent) component2;
			
			component.pushInspectorEventComponent(iComponent);
		}
		else
		{
			component2.onEvent(component.gameObject);
		}
		
		return false;
	}
	
	#if UNITY_EDITOR
	
	public override void createExtraFields(ref Rect rect)
	{
		rect.height = 16;
	
		GameObject gb = getGameObject();
		
		GameObject ngb = (GameObject) EditorGUI.ObjectField(new Rect(rect),"GameObject", gb, typeof(GameObject), true);
		setGameObject(ngb);
		
		if(gb!=ngb && !((ngb!=null && ngb.Equals(gb)) || (gb!=null && gb.Equals(ngb))))
		{
			setDirty();
		}
		
		rect.y += 16;
	
		string a = getEventName();
		
		string b = EditorGUI.TextField(new Rect(rect),"Event Name", a);
		setEventName(b);
		
		if(!a.Equals(b))
		{
			setDirty();
		}
		
		rect.y += 16;
	}
	
	#endif
}
