﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Reflection;
using System.Collections.Generic;

public class ActionTypeBehaviorEndDialog : ActionTypeBehavior
{
	public ActionTypeBehaviorEndDialog(InspectorEventComponent.InspectorEvent.InspectorEventAction iec) : base(iec)
	{}
	
	public override string ToString()
	{
		return "End Dialog";
	}
	
	public override bool run(InspectorEventComponent component)
	{
		GameObject.FindGameObjectWithTag("SceneManager").GetComponent<DialogController>().endDialog();
		
		return false;
	}
}
