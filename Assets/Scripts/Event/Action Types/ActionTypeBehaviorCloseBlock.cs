﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Reflection;
using System.Collections.Generic;

public class ActionTypeBehaviorCloseBlock : ActionTypeBehavior
{
	public ActionTypeBehaviorCloseBlock(InspectorEventComponent.InspectorEvent.InspectorEventAction iec) : base(iec)
	{
		this.isCloseBlock = true;
	}
	
	public override string ToString()
	{
		return "}";
	}
	
	public override bool preRun(InspectorEventComponent component)
	{
		component.ifResults.Pop();
		
		return true;
	}
}
