﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;

[XmlRoot("GroupGeneratorManager")]
public class GroupGeneratorManager
{
	public SerializableDictionary<string,GroupGeneratorBase> generators = new SerializableDictionary<string,GroupGeneratorBase>();

	public GroupGeneratorManager()
	{
	}

	public GroupGeneratorManager(GroupGeneratorManager that)
	{
		foreach (string s in that.generators.Keys)
		{
			generators.Add(s,that.generators[s].clone());
		}
	}

	public CharacterGroup generate(string id)
	{
		return generators[id].generate();
	}
}
