﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TargetBuilderSingleEnemy : TargetBuilder
{
	public CharacterMask target = null;

	int key = -1;

	public override Target build()
	{
		if(target != null)
		{
			return new Target(new List<PlayableCharacter>(),new List<CharacterMask>{target}, key);
		}

		return null;
	}

	public int getKey()
	{
		if(key == -1)
		{
			key = random.Next();
			return key;
		}

		return -1;
	}
}
