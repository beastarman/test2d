﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TargetBuilderNoOne : TargetBuilder
{
	public override Target build()
	{
		return new Target(new List<PlayableCharacter>(),new List<CharacterMask>());
	}
}
