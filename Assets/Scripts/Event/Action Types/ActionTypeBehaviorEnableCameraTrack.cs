﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Reflection;
using System.Collections.Generic;

public class ActionTypeBehaviorEnableCameraTrack : ActionTypeBehavior
{
	public ActionTypeBehaviorEnableCameraTrack(InspectorEventComponent.InspectorEvent.InspectorEventAction iec) : base(iec)
	{
	}
	
	public override void resize()
	{
		Array.Resize(ref getInspectorEventAction().extraString,0);
		Array.Resize(ref getInspectorEventAction().extraGameObjects,0);
	}
	
	public override string ToString()
	{
		return "Enable camera track";
	}
	
	public override bool run(InspectorEventComponent component)
	{
		CameraTrack track = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraTrack>();
		
		if(track != null)
		{
			track.enabled = true;
		}
		
		return false;
	}
}
