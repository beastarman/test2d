﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Reflection;
using System.Collections.Generic;

#if UNITY_EDITOR

using UnityEditor;

#endif

public class ActionTypeBehaviorDestroy : ActionTypeBehavior {

	public ActionTypeBehaviorDestroy(InspectorEventComponent.InspectorEvent.InspectorEventAction iec) : base(iec)
	{
		this.propertyHeight = 16;
	}
	
	public override void resize()
	{
		Array.Resize(ref getInspectorEventAction().extraString,0);
		Array.Resize(ref getInspectorEventAction().extraGameObjects,1);
	}
	
	public GameObject getCharacter()
	{
		return safeGetGameObject(0);
	}
	
	public GameObject setCharacter(GameObject obj)
	{
		return safeSetGameObject(0,obj);
	}
	
	public override string ToString()
	{
		GameObject c = getCharacter();
		
		if(c != null)
		{
			return "Destroy " + c.name;
		}
		else
		{
			return "(!) Destroy No one";
		}
	}
	
	public override bool run(InspectorEventComponent component)
	{
		GameObject.Destroy(getCharacter());
		
		return false;
	}
	
	#if UNITY_EDITOR
	
	public override void createExtraFields(ref Rect rect)
	{
		rect.height = 16;
	
		GameObject a = getCharacter();
		
		string label = "Target";
		
		GameObject b = (GameObject) EditorGUI.ObjectField(new Rect(rect),label, a, typeof(GameObject), true);
		setCharacter(b);
		
		if(a!=b && ((a!=null && !a.Equals(b)) || (b!=null && !b.Equals(a))))
		{
			setDirty();
		}
		
		rect.y += 16;
	}
	
	#endif
}
