﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CharacterGroup
{
	public List<CharacterLocation> characters = new List<CharacterLocation>();

	public bool isAlive()
	{
		foreach (CharacterLocation pc in characters)
		{
			if(pc.pc.isAlive()) return true;
		}

		return false;
	}
	
	public List<PlayableCharacter> getAliveCharacters()
	{
		List<PlayableCharacter> ret = new List<PlayableCharacter>();
		
		foreach (CharacterLocation cm in characters)
		{
			if(cm.pc.isAlive()) ret.Add(cm.pc);
		}
		
		return ret;
	}

	public bool contains(PlayableCharacter pc)
	{
		foreach (CharacterLocation cm in characters)
		{
			if(pc == cm.pc) return true;
		}

		return false;
	}

	public class CharacterLocation
	{
		public PlayableCharacter pc;
		public Vector2 location;

		public CharacterLocation(PlayableCharacter pc,Vector2 location)
		{
			this.pc = pc;
			this.location = location;
		}
	}
}
