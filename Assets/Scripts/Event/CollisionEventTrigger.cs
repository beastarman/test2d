﻿using UnityEngine;
using System.Collections;

public class CollisionEventTrigger : EventTrigger
{
	public enum TriggerTime
	{
		ENTER,
		STAY,
		EXIT,
	}

	public TriggerTime triggerTime;
	
	void OnTriggerEnter2D(Collider2D other)
	{
		if(triggerTime == TriggerTime.ENTER)
		{
			other.gameObject.transform.GetComponent<Rigidbody2D>().velocity = new Vector2();

			trigger(other.gameObject);
		}
	}
	
	void OnTriggerStay2D(Collider2D other)
	{
		if(triggerTime == TriggerTime.STAY)
		{
			other.gameObject.transform.GetComponent<Rigidbody2D>().velocity = new Vector2();

			trigger(other.gameObject);
		}
	}
	
	void OnTriggerExit2D(Collider2D other)
	{
		if(triggerTime == TriggerTime.EXIT)
		{
			other.gameObject.transform.GetComponent<Rigidbody2D>().velocity = new Vector2();

			trigger(other.gameObject);
		}
	}
}
