﻿using UnityEngine;
using System.Collections;

public abstract class BattleSceneAction : MonoBehaviour
{
	protected BattleManager.StackCommandIssuer commandIssuer
	{
		get
		{
			return gameObject.GetComponent<BattleSceneManager>().issuer;
		}
	}

	public int order = 100;
	public string name;
	public bool isEnabled = true;

	public void act(PlayableCharacter pc)
	{
		runAction(pc);
	}

	protected abstract void runAction(PlayableCharacter pc);
	
	public void finishAction()
	{
		GetComponent<BattleSceneManager>().nextInCommand();
	}
	
	public void cancelAction()
	{
	}
}
