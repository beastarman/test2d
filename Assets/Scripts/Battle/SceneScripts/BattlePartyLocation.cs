﻿using UnityEngine;
using System.Collections;

public class BattlePartyLocation : MonoBehaviour {

	public BattleSceneManager battleSceneManager;
	
	private float _position;
	
	public float position
	{
		get
		{
			return _position;
		}
		
		set
		{
			while(value<0f)value+=1f;
			while(value>=1f)value-=1f;
			_position = value;
			GetComponent<BattleTransform>().position = battleSceneManager.getPartyPosition(value);
		}
	}
}
