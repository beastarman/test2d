﻿using UnityEngine;
using System.Collections;

public class SpriteResources
{
	public static readonly SpriteDatabase.SpriteID STATUSFACE = new SpriteDatabase.SpriteID("StatusFace","fd6c259c-611c-4aa1-b4e3-6c3ecdae762d");
	public static readonly SpriteDatabase.SpriteID FALLENDOWN = new SpriteDatabase.SpriteID("FallenDown","be5d1570-734b-4f6d-8b91-71b9136dd95a");

	public static SpriteDatabase.SpriteID[] sprites
	{
		get
		{
			return new SpriteDatabase.SpriteID[]{STATUSFACE,FALLENDOWN};
		}
	}

	public static SpriteDatabase.SpriteID reverse(string id)
	{
		foreach (SpriteDatabase.SpriteID s in sprites)
		{
			if(s.id.Equals(id))
			{
				return s;
			}
		}

		return null;
	}
}
