﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Reflection;
using System.Collections.Generic;

#if UNITY_EDITOR

using UnityEditor;

#endif

public class ActionTypeBehaviorLog : ActionTypeBehavior
{
	public ActionTypeBehaviorLog(InspectorEventComponent.InspectorEvent.InspectorEventAction iec) : base(iec)
	{
		this.propertyHeight = 16*3;
	}
	
	public override void resize()
	{
		Array.Resize(ref getInspectorEventAction().extraString,1);
		Array.Resize(ref getInspectorEventAction().extraGameObjects,0);
	}
	
	public string getMessage()
	{
		return safeGetString(0);
	}
	
	public string setMessage(string msg)
	{
		return safeSetString(0,msg);
	}
	
	public override string ToString()
	{
		string s = getMessage();
		
		if(s.Length > 0)
		{
			return "Log: " + s;
		}
		else
		{
			return "(!) Empty Log";
		}
	}
	
	public override bool run(InspectorEventComponent component)
	{
		Debug.Log(getMessage());
		
		return false;
	}
	
	#if UNITY_EDITOR
	
	public override void createExtraFields(ref Rect rect)
	{
		rect.height = 16*3;
		
		Rect r = EditorGUI.PrefixLabel(new Rect(rect),new GUIContent("Log Message"));
		
		int i = EditorGUI.indentLevel;
		EditorGUI.indentLevel = 0;
		
		string a = getMessage();
		string b = EditorGUI.TextArea(r,a);
		setMessage(b);
		
		if(!a.Equals(b))
		{
			setDirty();
		}
		
		EditorGUI.indentLevel = i;
		
		rect.y += 16*3;
	}
	
	#endif
}
