﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Reflection;
using System.Collections.Generic;

public class ActionTypeBehaviorWaitCoroutines : ActionTypeBehavior
{
	public ActionTypeBehaviorWaitCoroutines(InspectorEventComponent.InspectorEvent.InspectorEventAction iec) : base(iec)
	{}
	
	public override string ToString()
	{
		return "Wait for Coroutines";
	}
	
	public override bool run(InspectorEventComponent component)
	{
		return true;
	}
	
	public override bool update(InspectorEventComponent component)
	{
		return component.runningCoroutines<=0;
	}
}
