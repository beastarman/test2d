﻿
using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Reflection;
using System.Collections.Generic;

public class ActionTypeBehaviorLoadGame : ActionTypeBehavior
{
	public ActionTypeBehaviorLoadGame(InspectorEventComponent.InspectorEvent.InspectorEventAction iec) : base(iec)
	{
	}
	
	public override void resize()
	{
	}
	
	public override string ToString()
	{
		return "Load Game";
	}
	
	public override bool run(InspectorEventComponent component)
	{
		string scene = GlobalGameController.Instance.loadGame();
		
		ScreenFader fader = GameObject.FindGameObjectWithTag("SceneManager").GetComponent<ScreenFader>();

		if(fader != null)
		{
			GlobalGameController.Instance.LoadLevel(fader,scene);
		}
		else
		{
			Application.LoadLevel(scene);
		}
		
		return false;
	}
}
