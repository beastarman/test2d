﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Reflection;
using System.Collections.Generic;

public class ActionTypeBehaviorStartDialog : ActionTypeBehavior
{
	public ActionTypeBehaviorStartDialog(InspectorEventComponent.InspectorEvent.InspectorEventAction iec) : base(iec)
	{}
	
	public override string ToString()
	{
		return "Start Dialog";
	}
	
	public override bool run(InspectorEventComponent component)
	{
		GameObject.FindGameObjectWithTag("SceneManager").GetComponent<DialogController>().startDialog();
		
		return false;
	}
}
