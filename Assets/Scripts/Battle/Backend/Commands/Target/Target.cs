﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Target
{
	static System.Random random = new System.Random();

	private List<PlayableCharacter> allies = new List<PlayableCharacter>();
	private List<CharacterMask> enemies = new List<CharacterMask>();

	int key = -1;
	
	public Target(List<PlayableCharacter> _allies, List<CharacterMask> _enemies)
	{
		allies = _allies;
		enemies = _enemies;
	}
	
	public Target(List<PlayableCharacter> _allies, List<CharacterMask> _enemies, int _key)
	{
		allies = _allies;
		enemies = _enemies;
		key = _key;
	}

	public int lockValues()
	{
		if(key < 0)
		{
			key = random.Next();
			return key;
		}
		else
		{
			return -1;
		}
	}
	
	public List<PlayableCharacter> getAllies(int _key)
	{
		if(key == _key)
		{
			return allies;
		}
		else
		{
			return null;
		}
	}
	
	public List<CharacterMask> getEnemies(int _key)
	{
		if(key == _key)
		{
			return enemies;
		}
		else
		{
			return null;
		}
	}
}
