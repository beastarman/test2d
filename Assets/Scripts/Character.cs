﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Character : MonoBehaviour
{
	public Vector3 directionFacing = new Vector3(0,-1,0);

	public CharacterDirection[] spriteDirections;

	// Use this for initialization
	void Start () {
	
	}

	void updateSprite()
	{
		Sprite finalSprite = null;
		float minAngle = 999;

		foreach(CharacterDirection dir in spriteDirections)
		{
			float curAngle = Mathf.Abs(Vector2.Angle(directionFacing,dir.direction));

			if(curAngle < minAngle)
			{
				minAngle = curAngle;
				finalSprite = dir.sprite;
			}
		}

		if(finalSprite != null)
		{
			GetComponent<SpriteRenderer>().sprite = finalSprite;
		}
	}

	// Update is called once per frame
	void Update ()
	{
		updateSprite();
	}
	
	public void move(Vector3 dir)
	{
		transform.position += dir;
		directionFacing = dir.normalized;
	}
}
