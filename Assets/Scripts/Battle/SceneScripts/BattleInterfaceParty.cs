﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class BattleInterfaceParty : MonoBehaviour
{
	BattleManager.StackCommandIssuer commandIssuer
	{
		get
		{
			return gameObject.GetComponent<BattleSceneManager>().issuer;
		}
	}

	[Header("Party elements")]

	public RectTransform partyStatusImage;
	public List<Image> smallItems = new List<Image>();
	public List<Image> bigItems = new List<Image>();
	public Image faceset;
	public Text hpCounter;
	public List<Image> hpBars = new List<Image>();

	public float selectedScale = 1.5f;
	public float animationTime = 0.1f;
	public Image OK;

	[Header("Enemy elements")]

	public RectTransform enemyStatusImage;
	public List<Image> enemyHpBars = new List<Image>();
	public Text enemyName;

	List<CharacterStatusIndicator> indicators = new List<CharacterStatusIndicator>();
	List<EnemyStatusIndicator> enemyIndicators = new List<EnemyStatusIndicator>();

	
	void Start()
	{
		CharacterStatusIndicator reference = new CharacterStatusIndicator(this, commandIssuer.ci.characterGroup.characters[0].pc,partyStatusImage);

		indicators.Add(reference);

		for(int i=1; i<commandIssuer.ci.characterGroup.characters.Count; i++)
		{
			CharacterStatusIndicator indicator = CharacterStatusIndicator.newFromRef(this,commandIssuer.ci.characterGroup.characters[i].pc,reference);
			indicators.Add(indicator);
			reference = indicator;
		}

		select(0);

		EnemyStatusIndicator enemyRef = new EnemyStatusIndicator(this, commandIssuer.ci.enemyGroup.characters[0].pc,enemyStatusImage);
		
		enemyIndicators.Add(enemyRef);

		for(int i=1; i<commandIssuer.ci.enemyGroup.characters.Count; i++)
		{
			EnemyStatusIndicator indicator = EnemyStatusIndicator.newFromRef(this,commandIssuer.ci.enemyGroup.characters[i].pc,enemyRef);
			enemyIndicators.Add(indicator);
			enemyRef = indicator;
		}
	}

	public void select(int position)
	{
		for(int i=0; i<indicators.Count; i++)
		{
			if(i==position)
			{
				indicators[i].selected=1f;
			}
			else
			{
				indicators[i].selected=0f;
			}
		}
	}

	public void update()
	{
		foreach(CharacterStatusIndicator csi in indicators)
		{
			csi.updateStats();
		}

		foreach(EnemyStatusIndicator esi in enemyIndicators)
		{
			esi.updateStats();
		}
	}

	class CharacterStatusIndicator
	{
		BattleInterfaceParty bip;
		public RectTransform transform;
		public CharacterStatusIndicator reference;
		CharacterStatusIndicator next;
		PlayableCharacter playableCharacter;

		Vector2 originalPosition;
		Vector2 originalSize;
		Vector3 originalScale;

		float _selected = 0f;
		private IEnumerator switchCoroutine = null;
		public float selected
		{
			get
			{
				return _selected;
			}
			set
			{
				if(switchCoroutine != null) bip.StopCoroutine(switchCoroutine);
				switchCoroutine = selectSmooth(_selected,value);
				bip.StartCoroutine(switchCoroutine);

				_selected = value;
			}
		}

		public CharacterStatusIndicator(BattleInterfaceParty bip,PlayableCharacter playableCharacter,RectTransform transform):this(bip,playableCharacter,transform,null){}

		private CharacterStatusIndicator(BattleInterfaceParty bip,PlayableCharacter playableCharacter,RectTransform transform,CharacterStatusIndicator reference)
		{
			this.bip = bip;
			this.playableCharacter = playableCharacter;
			this.transform = transform;
			this.reference = reference;

			if(reference != null) reference.next = this;

			originalPosition = transform.anchoredPosition;
			originalSize = transform.sizeDelta;
			originalScale = transform.localScale;

			if(playableCharacter.spriteDatabase.sprites.ContainsKey(SpriteResources.STATUSFACE.id))
			{
				transform.Find(bip.faceset.name).GetComponent<Image>().color = new Color(1f,1f,1f,1f);
				transform.Find(bip.faceset.name).GetComponent<Image>().sprite = playableCharacter.spriteDatabase.sprites[SpriteResources.STATUSFACE.id].sprite;
			}
			else
			{
				transform.Find(bip.faceset.name).GetComponent<Image>().color = new Color(0f,0f,0f,0f);
			}

			updateStats();
		}

		public static CharacterStatusIndicator newFromRef(BattleInterfaceParty bip, PlayableCharacter playableCharacter, CharacterStatusIndicator reference)
		{
			RectTransform clone = Instantiate(reference.transform,reference.transform.position,reference.transform.rotation) as RectTransform;
			clone.SetParent(reference.transform.parent);
			clone.localScale = reference.transform.localScale;

			CharacterStatusIndicator temp = new CharacterStatusIndicator(bip,playableCharacter,clone,reference);
			temp.reposition();
			return temp;
		}

		public void updateStats()
		{
			transform.Find(bip.hpCounter.name).GetComponent<Text>().text = ""+playableCharacter.state.curHP;

			foreach (Image img in bip.hpBars)
			{
				transform.Find(img.name).GetComponent<Image>().fillAmount = ((float)playableCharacter.state.curHP)/((float)playableCharacter.state.maxHP);
			}
		}

		void resize(float scale)
		{
			transform.localScale = originalScale*((1f-scale) + scale*bip.selectedScale);

			Image r;
			Color c;
			
			foreach(Image small in bip.smallItems)
			{
				r = transform.Find(small.name).GetComponent<Image>();
				c = r.color;
				c.a = (1f - scale);
				r.color = c;
			}
			
			foreach(Image big in bip.bigItems)
			{
				r = transform.Find(big.name).GetComponent<Image>();
				c = r.color;
				c.a = scale;
				r.color = c;
			}
		}

		void reposition()
		{
			if(reference != null)
			{
				Vector2 pos = transform.anchoredPosition;
				pos.y = reference.transform.anchoredPosition.y - (transform.sizeDelta.y*transform.localScale.y);
				transform.anchoredPosition = pos;
			}
			else
			{
				Vector2 pos = originalPosition;
				pos.y = originalPosition.y - (transform.sizeDelta.y*transform.localScale.y) + originalSize.y;
				transform.anchoredPosition = pos;
			}

			Image ok = transform.Find(bip.OK.name).GetComponent<Image>();

			if(bip.commandIssuer.addedCommand(playableCharacter))
			{
				Color c = ok.color;
				c.a = 1f;
				ok.color = c;
			}
			else
			{
				Color c = ok.color;
				c.a = 0;
				ok.color = c;
			}

			updateStats();
		}

		void recursiveReposition()
		{
			reposition();

			if(next != null) next.recursiveReposition();
		}

		private IEnumerator selectSmooth(float source, float target)
		{
			float animationTime = bip.animationTime;
			float beginTime = Time.time;

			yield return null;

			while(true)
			{
				float step = Mathf.Min(1f,(Time.time - beginTime) / animationTime);

				float scale = (1f-step)*source + step*target;
				resize(scale);
				recursiveReposition();

				if(step == 1f) break;

				yield return null;
			}
		}
	}

	class EnemyStatusIndicator
	{
		BattleInterfaceParty bip;
		public RectTransform transform;
		public EnemyStatusIndicator reference;
		EnemyStatusIndicator next;
		CharacterMask characterMask;
		
		Vector2 originalPosition;
		Vector2 originalSize;
		Vector3 originalScale;

		public EnemyStatusIndicator(BattleInterfaceParty bip,CharacterMask characterMask,RectTransform transform):this(bip,characterMask,transform,null){}
		
		private EnemyStatusIndicator(BattleInterfaceParty bip,CharacterMask characterMask,RectTransform transform,EnemyStatusIndicator reference)
		{
			this.bip = bip;
			this.characterMask = characterMask;
			this.transform = transform;
			this.reference = reference;
			
			if(reference != null) reference.next = this;
			
			originalPosition = transform.anchoredPosition;
			originalSize = transform.sizeDelta;
			originalScale = transform.localScale;
			
			updateStats();
		}
		
		public static EnemyStatusIndicator newFromRef(BattleInterfaceParty bip, CharacterMask characterMask, EnemyStatusIndicator reference)
		{
			RectTransform clone = Instantiate(reference.transform,reference.transform.position,reference.transform.rotation) as RectTransform;
			clone.SetParent(reference.transform.parent);
			clone.localScale = reference.transform.localScale;
			
			EnemyStatusIndicator temp = new EnemyStatusIndicator(bip,characterMask,clone,reference);
			temp.reposition();
			return temp;
		}

		public void updateStats()
		{
			transform.Find(bip.enemyName.name).GetComponent<Text>().text = characterMask.name;

			foreach (Image img in bip.enemyHpBars)
			{
				transform.Find(img.name).GetComponent<Image>().fillAmount = (float)characterMask.HP;
			}
		}

		void reposition()
		{
			if(reference != null)
			{
				Vector2 pos = transform.anchoredPosition;
				pos.y = reference.transform.anchoredPosition.y - (transform.sizeDelta.y*transform.localScale.y);
				transform.anchoredPosition = pos;
			}
			else
			{
				Vector2 pos = originalPosition;
				pos.y = originalPosition.y - (transform.sizeDelta.y*transform.localScale.y) + originalSize.y;
				transform.anchoredPosition = pos;
			}

			updateStats();
		}
	}
}
