﻿using UnityEngine;
using System.Collections;

public class BattleActionInfo : BattleAction
{
	string info;

	public BattleActionInfo(string _info)
	{
		info = _info;
	}

	public override string ToString()
	{
		return info;
	}
}
