﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

#if UNITY_EDITOR

using UnityEditor;

#endif

[XmlInclude(typeof(RandomGroupGenerator))]
[XmlInclude(typeof(GroupGeneratorSimple))]
public abstract class GroupGeneratorBase 
{
	public GroupGeneratorBase()
	{
	}
	
	public CharacterGroupMask generateMask()
	{
		return new CharacterGroupMask(generate());
	}

	public abstract CharacterGroup generate();

	public abstract GroupGeneratorBase clone();

	#if UNITY_EDITOR

	public abstract string getID();

	public virtual bool OnGUI(string id)
	{
		return false;
	}

	#endif
}
