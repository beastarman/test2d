﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

public class GUIExtension
{
	public static string Layout_StringPopup(string selected, string[] displayedOptions, params GUILayoutOption[] options)
	{
		int i = Mathf.Max(0,(new List<string>(displayedOptions)).IndexOf(selected));
		i = EditorGUILayout.Popup(i,displayedOptions,options);
		return displayedOptions[i];
	}
	
	public static string Layout_StringPopup(string label, string selected, string[] displayedOptions, params GUILayoutOption[] options)
	{
		int i = Mathf.Max(0,(new List<string>(displayedOptions)).IndexOf(selected));
		i = EditorGUILayout.Popup(label,i,displayedOptions,options);
		return displayedOptions[i];
	}
	
	public static T Layout_GenericPopup<T>(T selected, Dictionary<T,string> displayedOptions, params GUILayoutOption[] options)
	{
		string[] array = new string[displayedOptions.Count];
		
		displayedOptions.Values.CopyTo(array,0);
		
		string s = Layout_StringPopup(displayedOptions[selected],array,options);
		
		return DictionaryExtensions.FindKeyByValue(displayedOptions,s);
	}
	
	public static T Layout_GenericPopup<T>(string label, T selected, Dictionary<T,string> displayedOptions, params GUILayoutOption[] options)
	{
		string[] array = new string[displayedOptions.Count];
		
		displayedOptions.Values.CopyTo(array,0);
		
		string s = Layout_StringPopup(label, displayedOptions[selected],array,options);
		
		return DictionaryExtensions.FindKeyByValue(displayedOptions,s);
	}
	
	public static T Layout_GenericPopup<T>(T selected, Dictionary<string,T> displayedOptions, params GUILayoutOption[] options)
	{
		string[] array = new string[displayedOptions.Count];
		
		displayedOptions.Keys.CopyTo(array,0);
		
		string s = Layout_StringPopup(DictionaryExtensions.FindKeyByValue(displayedOptions,selected),array,options);
		
		return displayedOptions[s];
	}
	
	public static T Layout_GenericPopup<T>(string label, T selected, Dictionary<string,T> displayedOptions, params GUILayoutOption[] options)
	{
		string[] array = new string[displayedOptions.Count];
		
		displayedOptions.Keys.CopyTo(array,0);
		
		string s = Layout_StringPopup(label,DictionaryExtensions.FindKeyByValue(displayedOptions,selected),array,options);
		
		return displayedOptions[s];
	}
	
	public static T Layout_GenericPopup<T>(T selected, T[] displayedOptions, params GUILayoutOption[] options)
	{
		Dictionary<string,T> dict = new Dictionary<string, T>();
		
		foreach (T option in displayedOptions)
		{
			dict.Add(option.ToString(),option);
		}
		
		return Layout_GenericPopup(selected,dict,options);
	}
	
	public static T Layout_GenericPopup<T>(string label, T selected, T[] displayedOptions, params GUILayoutOption[] options)
	{
		Dictionary<string,T> dict = new Dictionary<string, T>();
		
		foreach (T option in displayedOptions)
		{
			dict.Add(option.ToString(),option);
		}
		
		return Layout_GenericPopup(label,selected,dict,options);
	}
	
	public static T Layout_GenericPopup<T>(T selected, List<T> displayedOptions, params GUILayoutOption[] options)
	{
		Dictionary<string,T> dict = new Dictionary<string, T>();
		
		foreach (T option in displayedOptions)
		{
			dict.Add(option.ToString(),option);
		}
		
		return Layout_GenericPopup(selected,dict,options);
	}
	
	public static T Layout_GenericPopup<T>(string label, T selected, List<T> displayedOptions, params GUILayoutOption[] options)
	{
		Dictionary<string,T> dict = new Dictionary<string, T>();
		
		foreach (T option in displayedOptions)
		{
			dict.Add(option.ToString(),option);
		}
		
		return Layout_GenericPopup(label,selected,dict,options);
	}
}
