﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class GroupGeneratorSimple : GroupGeneratorBase 
{
	public List<LazyCharGenerator> group = new List<LazyCharGenerator>();
	
	public GroupGeneratorSimple() : base()
	{
	}
	
	public override CharacterGroup generate()
	{
		CharacterGroup charGroup = new CharacterGroup();
		
		foreach (LazyCharGenerator factory in group)
		{
			charGroup.characters.Add(new CharacterGroup.CharacterLocation(
				GlobalGameController.Instance.charactersManager.playableCharacterFactory[factory.factoryId].generatePlayableCharacter(factory.level),
				factory.location
			));
		}
		
		return charGroup;
	}

	public override GroupGeneratorBase clone()
	{
		GroupGeneratorSimple that = new GroupGeneratorSimple();

		foreach (LazyCharGenerator l in group)
		{
			that.group.Add(new LazyCharGenerator(l));
		}

		return that;
	}

	#if UNITY_EDITOR

	public static string classId = "Simple Generator";
	
	public override string getID()
	{
		return classId;
	}
	
	int selectedCharacterPos = -1;

	LazyCharGenerator selecterCharacter
	{
		get
		{
			if(selectedCharacterPos >= 0 && selectedCharacterPos < group.Count)
			{
				return group[selectedCharacterPos];
			}

			return null;
		}
	}

	string[] _factories = null;
	string[] factories
	{
		get
		{
			if(_factories == null)
			{
				_factories = new string[GlobalGameController.Instance.charactersManager.playableCharacterFactory.Count];
				GlobalGameController.Instance.charactersManager.playableCharacterFactory.Keys.CopyTo(_factories,0);
			}

			return _factories;
		}
	}

	public override bool OnGUI(string id)
	{
		bool isDirty = base.OnGUI(id);

		EditorGUILayout.BeginHorizontal();
		{
			Rect r = EditorGUILayout.BeginVertical();
			{
				Color t = GUI.backgroundColor;
				GUI.backgroundColor = Color.white;
				GUI.Box(r,"");
				GUI.backgroundColor = t;
				
				GUIStyle style = new GUIStyle();
				style.padding = new RectOffset(4,4,2,2);
				GUIStyle selected = new GUIStyle(style);
				selected.normal.background = MakeTex(1000,20,new Color(0.5f,0.5f,1.0f));
				selected.active.background = MakeTex(1000,20,new Color(0.5f,0.5f,1.0f));

				int charSel = 0;
				foreach (LazyCharGenerator character in group)
				{
					string buttonLabel = (charSel+1) + ". " + character.name + " Level " + character.level + " (" + character.factoryId + ")";

					if(selectedCharacterPos == charSel)
					{
						GUILayout.Button(buttonLabel,selected);
					}
					else
					{
						if(GUILayout.Button(buttonLabel,style))
						{
							selectedCharacterPos = charSel;
							GUI.FocusControl("");
						}
					}
					
					charSel++;
				}
				
				if(GUILayout.Button("Add Character...",style))
				{
					if(group.Count == 0)
					{
						Dictionary<string,CharacterFactory>.Enumerator ie = GlobalGameController.Instance.charactersManager.playableCharacterFactory.GetEnumerator();
						ie.MoveNext();

						group.Add(new LazyCharGenerator(ie.Current.Key,1,ie.Current.Value.generatePlayableCharacter().name,new Vector2(0.5f,0.5f)));
					}
					else
					{
						LazyCharGenerator gen = group[group.Count - 1];

						group.Add(new LazyCharGenerator(
							gen.factoryId,
							gen.level,
						    GlobalGameController.Instance.charactersManager.playableCharacterFactory[gen.factoryId].generatePlayableCharacter().name,
							new Vector2(0.5f,0.5f)
						));
					}
					
					selectedCharacterPos = charSel;

					isDirty = true;

					GUI.FocusControl("");
				}
				
				if(GUILayout.Button("",style))
				{
					selectedCharacterPos = -1;
					
					GUI.FocusControl("");
				}
			}
			EditorGUILayout.EndVertical();

			r = EditorGUILayout.GetControlRect(false, 155, GUILayout.Width(150));
			{
				r.height-=5;

				Color t = GUI.backgroundColor;
				GUI.backgroundColor = Color.gray;
				GUI.Box(r,"");
				GUI.backgroundColor = t;

				r.position += new Vector2(15,15);
				r.size -= new Vector2(30,30);

				Drawing.DrawLine(r.position+new Vector2(0*r.width/5,0),r.position+new Vector2(0*r.width/5,r.height));
				Drawing.DrawLine(r.position+new Vector2(1*r.width/5,0),r.position+new Vector2(1*r.width/5,r.height));
				Drawing.DrawLine(r.position+new Vector2(2*r.width/5,0),r.position+new Vector2(2*r.width/5,r.height));
				Drawing.DrawLine(r.position+new Vector2(3*r.width/5,0),r.position+new Vector2(3*r.width/5,r.height));
				Drawing.DrawLine(r.position+new Vector2(4*r.width/5,0),r.position+new Vector2(4*r.width/5,r.height));
				Drawing.DrawLine(r.position+new Vector2(5*r.width/5,0),r.position+new Vector2(5*r.width/5,r.height));

				Drawing.DrawLine(r.position+new Vector2(0,0*r.height/5),r.position+new Vector2(r.width,0*r.height/5));
				Drawing.DrawLine(r.position+new Vector2(0,1*r.height/5),r.position+new Vector2(r.width,1*r.height/5));
				Drawing.DrawLine(r.position+new Vector2(0,2*r.height/5),r.position+new Vector2(r.width,2*r.height/5));
				Drawing.DrawLine(r.position+new Vector2(0,3*r.height/5),r.position+new Vector2(r.width,3*r.height/5));
				Drawing.DrawLine(r.position+new Vector2(0,4*r.height/5),r.position+new Vector2(r.width,4*r.height/5));
				Drawing.DrawLine(r.position+new Vector2(0,5*r.height/5),r.position+new Vector2(r.width,5*r.height/5));

				isDirty |= handleMouseEvents(r);

				int i = 0;
				foreach (LazyCharGenerator character in group)
				{
					if(i != selectedCharacterPos)
					{
						drawCharLocation(r,character,i);
					}

					i++;
				}

				if(selectedCharacterPos >= 0 && selectedCharacterPos < group.Count)
				{
					drawCharLocation(r,group[selectedCharacterPos],selectedCharacterPos);
				}
			}
		}
		EditorGUILayout.EndHorizontal();

		if(selecterCharacter != null)
		{
			string newFactory = Layout_StringPopup("Factory",selecterCharacter.factoryId,factories);

			if(!newFactory.Equals(selecterCharacter.factoryId))
			{
				isDirty|=true;

				selecterCharacter.factoryId = newFactory;
				selecterCharacter.name = GlobalGameController.Instance.charactersManager.playableCharacterFactory[newFactory].generatePlayableCharacter().name;
			}

			int newLevel = EditorGUILayout.IntSlider("Level",selecterCharacter.level,1,100);
			isDirty|=newLevel!=selecterCharacter.level;
			selecterCharacter.level = newLevel;

			string newName = EditorGUILayout.TextField("Name",selecterCharacter.name);
			isDirty|=!newName.Equals(selecterCharacter.name);
			selecterCharacter.name = newName;
		}

		return isDirty;
	}

	int movingChar = -1;

	public bool isInLocation(Vector2 position, Vector2 location)
	{
		return (position.x > location.x-0.1f && position.x < location.x+0.1f) && (position.y > location.y-0.1f && position.y < location.y+0.1f);
	}

	public void handleMouseDown(Vector2 position)
	{
		if(selectedCharacterPos >=0 && selectedCharacterPos < group.Count && isInLocation(position,group[selectedCharacterPos].location))
		{
			movingChar = selectedCharacterPos;

			return;
		}

		for(int i=group.Count-1; i>=0; i--)
		{
			if(isInLocation(position,group[i].location))
			{
				movingChar = i;
				selectedCharacterPos = i;

				return;
			}
		}
	}

	public bool handleMouseEvents(Rect r)
	{
		if(Event.current.type == EventType.mouseDown)
		{
			Vector2 position = Event.current.mousePosition - r.position;

			position.x /= r.width;
			position.y /= r.height;

			if(position.x > -0.1 && position.x <1.1 && position.y > -0.1 && position.y <1.1)
			{
				handleMouseDown(position);
			}

			return false;
		}

		if(Event.current.type == EventType.mouseDrag && movingChar != -1)
		{
			Vector2 position = Event.current.mousePosition - r.position;
			
			position.x /= r.width;
			position.y /= r.height;

			position.x = Mathf.Max(Mathf.Min(position.x,1f),0f);
			position.y = Mathf.Max(Mathf.Min(position.y,1f),0f);

			group[movingChar].location = position;

			return true;
		}

		if(Event.current.type == EventType.mouseUp)
		{
			movingChar = -1;
		}

		return false;
	}

	private void drawCharLocation(Rect r, LazyCharGenerator character, int i)
	{
		Rect buttonRect = new Rect(
			r.position + new Vector2(r.size.x * character.location.x,r.size.y * character.location.y) - (r.size * 0.1f),
			r.size * 0.2f
		);

		GUI.Button(buttonRect,"" + (i + 1));
	}

	public static string Layout_StringPopup(string label, string selected, string[] displayedOptions, params GUILayoutOption[] options)
	{
		int i = Mathf.Max(0,(new List<string>(displayedOptions)).IndexOf(selected));
		i = EditorGUILayout.Popup(label,i,displayedOptions,options);
		return displayedOptions[i];
	}

	private Texture2D MakeTex(int width, int height, Color col)
	{
		Color[] pix = new Color[width*height];
		
		for(int i = 0; i < pix.Length; i++)
			pix[i] = col;
		
		Texture2D result = new Texture2D(width, height);
		result.SetPixels(pix);
		result.Apply();
		
		return result;
	}

	public class Drawing
	{
		//****************************************************************************************************
		//  static function DrawLine(rect : Rect) : void
		//  static function DrawLine(rect : Rect, color : Color) : void
		//  static function DrawLine(rect : Rect, width : float) : void
		//  static function DrawLine(rect : Rect, color : Color, width : float) : void
		//  static function DrawLine(Vector2 pointA, Vector2 pointB) : void
		//  static function DrawLine(Vector2 pointA, Vector2 pointB, color : Color) : void
		//  static function DrawLine(Vector2 pointA, Vector2 pointB, width : float) : void
		//  static function DrawLine(Vector2 pointA, Vector2 pointB, color : Color, width : float) : void
		//  
		//  Draws a GUI line on the screen.
		//  
		//  DrawLine makes up for the severe lack of 2D line rendering in the Unity runtime GUI system.
		//  This function works by drawing a 1x1 texture filled with a color, which is then scaled
		//   and rotated by altering the GUI matrix.  The matrix is restored afterwards.
		//****************************************************************************************************
		
		public static Texture2D lineTex;
		
		public static void DrawLine(Rect rect) { DrawLine(rect, GUI.contentColor, 1.0f); }
		public static void DrawLine(Rect rect, Color color) { DrawLine(rect, color, 1.0f); }
		public static void DrawLine(Rect rect, float width) { DrawLine(rect, GUI.contentColor, width); }
		public static void DrawLine(Rect rect, Color color, float width) { DrawLine(new Vector2(rect.x, rect.y), new Vector2(rect.x + rect.width, rect.y + rect.height), color, width); }
		public static void DrawLine(Vector2 pointA, Vector2 pointB) { DrawLine(pointA, pointB, GUI.contentColor, 1.0f); }
		public static void DrawLine(Vector2 pointA, Vector2 pointB, Color color) { DrawLine(pointA, pointB, color, 1.0f); }
		public static void DrawLine(Vector2 pointA, Vector2 pointB, float width) { DrawLine(pointA, pointB, GUI.contentColor, width); }
		public static void DrawLine(Vector2 pointA, Vector2 pointB, Color color, float width)
		{
			// Save the current GUI matrix, since we're going to make changes to it.
			Matrix4x4 matrix = GUI.matrix;
			
			// Generate a single pixel texture if it doesn't exist
			if (!lineTex) { lineTex = new Texture2D(1, 1); }
			
			// Store current GUI color, so we can switch it back later,
			// and set the GUI color to the color parameter
			Color savedColor = GUI.color;
			GUI.color = color;
			
			// Determine the angle of the line.
			float angle = Vector3.Angle(pointB - pointA, Vector2.right);
			
			// Vector3.Angle always returns a positive number.
			// If pointB is above pointA, then angle needs to be negative.
			if (pointA.y > pointB.y) { angle = -angle; }
			
			// Use ScaleAroundPivot to adjust the size of the line.
			// We could do this when we draw the texture, but by scaling it here we can use
			//  non-integer values for the width and length (such as sub 1 pixel widths).
			// Note that the pivot point is at +.5 from pointA.y, this is so that the width of the line
			//  is centered on the origin at pointA.
			GUIUtility.ScaleAroundPivot(new Vector2((pointB - pointA).magnitude, width), new Vector2(pointA.x, pointA.y + 0.5f));
			
			// Set the rotation for the line.
			//  The angle was calculated with pointA as the origin.
			GUIUtility.RotateAroundPivot(angle, pointA);
			
			// Finally, draw the actual line.
			// We're really only drawing a 1x1 texture from pointA.
			// The matrix operations done with ScaleAroundPivot and RotateAroundPivot will make this
			//  render with the proper width, length, and angle.
			GUI.DrawTexture(new Rect(pointA.x, pointA.y, 1, 1), lineTex);
			
			// We're done.  Restore the GUI matrix and GUI color to whatever they were before.
			GUI.matrix = matrix;
			GUI.color = savedColor;
		}
	}

	#endif

	public class LazyCharGenerator
	{
		public string factoryId;
		public int level;
		public string name;
		public Vector2 location;
		
		public LazyCharGenerator() : this("",1,"",new Vector2())
		{
		}
		
		public LazyCharGenerator(LazyCharGenerator l) : this(l.factoryId,l.level,l.name,l.location)
		{
		}

		public LazyCharGenerator(string factoryId,int level,string name,Vector2 location)
		{
			this.factoryId = factoryId;
			this.level = level;
			this.name = name;
			this.location = location;
		}
		
	}
}
