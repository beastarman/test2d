﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;

#if UNITY_EDITOR

using UnityEditor;

#endif

[XmlInclude(typeof(CharacterFactoryLinear))]
public class CharacterFactory
{
	public string name;

	public CacheableSprite sprite = new CacheableSprite();
	public SpriteDatabase spriteDatabase = new SpriteDatabase();

	public CharacterFactory() : this("?")
	{
	}

	public CharacterFactory(string nName)
	{
		name = nName;
	}
	
	public CharacterFactory(CharacterFactory factory) : this(factory.name)
	{
		sprite = new CacheableSprite(factory.sprite);
		spriteDatabase = new SpriteDatabase(factory.spriteDatabase);
	}

	public PlayableCharacter generatePlayableCharacter()
	{
		return generatePlayableCharacter(1);
	}
	
	public virtual PlayableCharacter generatePlayableCharacter(int level)
	{
		return new PlayableCharacter(sprite,spriteDatabase,level);
	}
	
	public virtual CharacterFactory Clone()
	{
		return new CharacterFactory(this);
	}
	
	#if UNITY_EDITOR

	public virtual bool OnGUI()
	{
		bool isDirty = false;

		string temp = EditorGUILayout.TextField("Name", name);
		isDirty |= temp != name;
		name = temp;

		return isDirty;
	}

	#endif
}
