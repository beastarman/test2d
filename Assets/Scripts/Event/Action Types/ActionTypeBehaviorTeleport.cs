﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Reflection;
using System.Collections.Generic;

#if UNITY_EDITOR

using UnityEditor;

#endif

public class ActionTypeBehaviorTeleport : ActionTypeBehavior
{
	public ActionTypeBehaviorTeleport(InspectorEventComponent.InspectorEvent.InspectorEventAction iec) : base(iec)
	{
		this.propertyHeight = 16*2;
	}
	
	public override void resize()
	{
		Array.Resize(ref getInspectorEventAction().extraGameObjects,2);
		Array.Resize(ref getInspectorEventAction().extraString,0);
	}
	
	public GameObject getWho()
	{
		return safeGetGameObject(0);
	}
	
	public GameObject setWho(GameObject g)
	{
		return safeSetGameObject(0,g);
	}
	
	public GameObject getWhere()
	{
		return safeGetGameObject(1);
	}
	
	public GameObject setWhere(GameObject g)
	{
		return safeSetGameObject(1,g);
	}
	
	public override string ToString()
	{
		GameObject who = getWho();
		GameObject where = getWhere();
		bool error = false;
		
		string ret = "Teleport ";
		
		if(who != null)
		{
			ret += who.name + " to ";
		}
		else
		{
			error = true;
			ret += "UNDEFINED to ";
		}
		
		if(where != null)
		{
			ret += where.name;
		}
		else
		{
			error = true;
			ret += "UNDEFINED";
		}
		
		if(error) ret = "(!) " + ret;
		
		return ret;
	}
	
	public override bool run(InspectorEventComponent component)
	{
		Vector3 pos = getWhere().transform.position;
		
		pos.z = getWho().transform.position.z;
		
		getWho().transform.position = pos;
		
		return false;
	}
	
	#if UNITY_EDITOR
	
	public override void createExtraFields(ref Rect rect)
	{
		rect.height = 16;
	
		GameObject who = getWho();
		
		GameObject newDoctor = (GameObject) EditorGUI.ObjectField(new Rect(rect),"Who", who, typeof(GameObject), true);
		setWho(newDoctor);
		
		if(who!=newDoctor && !((newDoctor!=null && newDoctor.Equals(who)) || (who!=null && who.Equals(newDoctor))))
		{
			setDirty();
		}
		
		rect.y += 16;
	
		who = getWhere();
		
		newDoctor = (GameObject) EditorGUI.ObjectField(new Rect(rect),"Where", who, typeof(GameObject), true);
		setWhere(newDoctor);
		
		if(who!=newDoctor && !((newDoctor!=null && newDoctor.Equals(who)) || (who!=null && who.Equals(newDoctor))))
		{
			setDirty();
		}
		
		rect.y += 16;
	}
	
	#endif
}
