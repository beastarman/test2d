﻿using UnityEngine;
using System.Collections;

interface CommandIssuerInterface
{
	void addCommand(Command c);
	
	void removeCommand(PlayableCharacter pc);
	
	bool addedCommand(PlayableCharacter pc);
	
	bool isLocked();
	
	bool issue();
	
	bool issuedCommand();
	
	void reset();
}
