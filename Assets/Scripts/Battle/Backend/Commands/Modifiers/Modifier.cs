﻿using UnityEngine;
using System.Collections;

public class Modifier
{
	public uint turns;

	public Modifier(uint _turns)
	{
		turns = _turns;
	}

	public virtual void onHit(ref Hit hit)
	{
		return;
	}
}
