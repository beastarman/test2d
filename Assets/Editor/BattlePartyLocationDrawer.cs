﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Linq;
using System;
using System.Reflection;
using System.Collections.Generic;

[CustomEditor(typeof(BattlePartyLocation))]
public class BattlePartyLocationDrawer : Editor
{
	public override void OnInspectorGUI()
	{
		serializedObject.Update();
		
		BattlePartyLocation bt = (BattlePartyLocation)serializedObject.targetObject;
		
		bt.position = EditorGUILayout.FloatField("Position",bt.position);
		
		serializedObject.ApplyModifiedProperties ();
	}
}
