﻿using UnityEngine;
using System.Collections;

public class EventTrigger : MonoBehaviour
{
	public string eventName;

	public void trigger(GameObject obj)
	{
		EventComponent[] events = gameObject.GetComponents<EventComponent>();

		foreach(EventComponent e in events)
		{
			if(e.eventName.Equals(eventName))
			{
				e.onEvent(obj);

				return;
			}
		}
	}
}
