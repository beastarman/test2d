﻿using UnityEngine;
using System.Collections;

public class CommandDefend : Command
{
	public CommandDefend(PlayableCharacter source) : base(source)
	{
	}

	public override string getName()
	{
		return "Defend";
	}

	public override int getPriority()
	{
		return int.MaxValue;
	}
	
	public override BattleAction doIt()
	{
		source.addModifier(new ModifierDefend());

		return new BattleActionInfo(source.name + " is defending");
	}
	
	public class Builder : CommandBuilder
	{
		public Builder(PlayableCharacter pc) : base(pc)
		{
		}

		public override void setTarget(Target target)
		{
		}

		public override Command build()
		{
			return new CommandDefend(source);
		}
		
		public override TargetBuilder getTargetBuilder()
		{
			return new TargetBuilderNoOne();
		}
		
		public override string getName()
		{
			return "Defend";
		}
		
		public override string getDescription()
		{
			return "Defend from every attack on this turn";
		}
	}
}
