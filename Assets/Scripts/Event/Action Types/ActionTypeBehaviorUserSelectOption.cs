﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Reflection;
using System.Collections.Generic;

public class ActionTypeBehaviorUserSelectOption : ActionTypeBehavior
{
	public ActionTypeBehaviorUserSelectOption(InspectorEventComponent.InspectorEvent.InspectorEventAction iec) : base(iec)
	{}
	
	public override string ToString()
	{
		return "User selects an option";
	}
	
	public override bool run(InspectorEventComponent component)
	{
		return true;
	}
	
	public override bool update(InspectorEventComponent component)
	{
		DialogController dialogController = GameObject.FindGameObjectWithTag("SceneManager").GetComponent<DialogController>();
		
		if(component.down.isOnStateMax(State.DOWN,0))
		{
			dialogController.highlightNext();
		}
		else if(component.up.isOnStateMax(State.DOWN,0))
		{
			dialogController.highlightPrev();
		}
		else if(component.next.isOnStateMax(State.DOWN,0))
		{
			int lastOption = dialogController.hideOptions();
			
			GlobalGameController.Instance.getMemory().put(LAST_SELECTED_OPTION,lastOption);
			
			return true;
		}
		
		return false;
	}
}
