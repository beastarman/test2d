﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Reflection;
using System.Collections.Generic;

public class ActionTypeBehaviorElif : ActionTypeBehaviorIf
{
	public ActionTypeBehaviorElif(InspectorEventComponent.InspectorEvent.InspectorEventAction iec) : base(iec)
	{
		this.isOpenBlock = true;
		this.isCloseBlock = true;
		this.propertyHeight = 16*4;
	}
	
	public override string ToString()
	{
		string op = getOp();
		
		if(op.Length > 0)
		{
			return "} Elif (" + (isNot()?"NOT ":"")  + getArg1() + " " + op + " " + getArg2() + ") {";
		}
		else
		{
			return "} Elif(NOT_DEFINED) {";
		}
	}
	
	public override bool preRun(InspectorEventComponent component)
	{
		int last = component.ifResults.Pop();
		
		if(last == 0)
		{
			if(runTest())
			{
				component.ifResults.Push(1);
			}
			else
			{
				component.ifResults.Push(0);
			}
		}
		else
		{
			component.ifResults.Push(last+1);
		}
		
		return true;
	}
}
