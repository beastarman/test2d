﻿using UnityEngine;
using System.Collections;

public class BattleSceneActionAttack : BattleSceneAction
{
	public SingleEnemyPicker picker = null;
	CommandBuilder builder = null;

	protected override void runAction(PlayableCharacter pc)
	{
		builder = new CommandAttack.Builder(pc);

		picker.focus(builder);
	}

	void Update()
	{
		if(builder!=null && picker!=null && !picker.isFocused)
		{
			Command c = builder.build();

			builder = null;

			if(c == null)
			{
				cancelAction();
			}
			else
			{
				commandIssuer.addCommand(c);

				finishAction();
			}
		}
	}
}
