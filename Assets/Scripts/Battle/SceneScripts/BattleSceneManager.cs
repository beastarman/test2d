﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BattleSceneManager : MonoBehaviour
{
	public BattleManager.StackCommandIssuer issuer
	{
		get
		{
			return GetComponent<BattleManager>().getCommandIssuer();
		}
	}

	public Canvas canvas;
	public List<GameObject> party;
	public List<GameObject> enemies;

	[Header("Grid settings")]

	public Vector2 enemyLine;
	public double enemyLineWidth;

	public Vector2 partyLine;
	public double partyLineWidth;

	public double _zUnitHeight = 1f;
	public double zUnitHeight
	{
		get
		{
			return (double)Mathf.Max(0.01f,(float)_zUnitHeight);
		}
	}

	public int columns = 10;

	[Space(16)]

	public float partyMarginBelow = 0.5f;
	public float partyMarginAround = 1f;

	public int enemySpace = 5;

	[Header("Render settings")]
	public float spriteScale = 1.0f;
	public float spriteScaleEnd = 2.0f;

	public float currentCharacterLocation = 0f;
	public float leftCharacterLocation = -0.2f;
	public float rightCharacterLocation = 0.2f;
	public float backCharacterLocation = 0.5f;

	// Use this for initialization
	void Start ()
	{
		for(int i=0; i<issuer.ci.characterGroup.characters.Count; i++)
		{
			PlayableCharacter charMask = issuer.ci.characterGroup.characters[i].pc;
			GameObject gameObject = new GameObject(charMask.name);
			gameObject.AddComponent<RectTransform>();
			gameObject.layer = LayerMask.NameToLayer("UI");

			SpriteRenderer renderer = gameObject.AddComponent<SpriteRenderer>();
			renderer.sprite = charMask.sprite;
			gameObject.transform.SetParent(canvas.transform);

			gameObject.AddComponent<BattleTransform>().battleSceneManager = this;

			BattlePartyLocation bpt = gameObject.AddComponent<BattlePartyLocation>();
			bpt.battleSceneManager = this;

			party.Add(gameObject);
		}

		for(int i=0; i<party.Count; i++)
		{
			party[i].GetComponent<BattlePartyLocation>().position = getPartyPositionFloat(i);
		}

		for(int i=0; i<issuer.ci.enemyGroup.characters.Count; i++)
		{
			CharacterGroupMask.CharacterMaskLocation charMaskLoc = issuer.ci.enemyGroup.characters[i];
			CharacterMask charMask = charMaskLoc.pc;

			GameObject gameObject = new GameObject(charMask.name);
			gameObject.AddComponent<RectTransform>();
			gameObject.layer = LayerMask.NameToLayer("UI");

			SpriteRenderer renderer = gameObject.AddComponent<SpriteRenderer>();
			renderer.sprite = charMask.sprite;
			gameObject.transform.SetParent(canvas.transform);

			BattleTransform bt = gameObject.AddComponent<BattleTransform>();
			bt.battleSceneManager = this;
			bt.position = new Vector2(charMaskLoc.location.x*getColumns(), charMaskLoc.location.y*enemySpace);
			
			enemies.Add(gameObject);
		}
	}

	int getRows()
	{
		int rows = 1;
		double oinc = 1f + (zUnitHeight / Mathf.Abs(enemyLine.y - partyLine.y));
		double inc = oinc;

		while(zUnitHeight*inc < Mathf.Abs(enemyLine.y - partyLine.y))
		{
			inc *= oinc;
			rows++;
		}

		return (int)Mathf.Max(1f,rows);
	}

	int getColumns()
	{
		return (int) Mathf.Max(1+(partyMarginAround*2),columns);
	}

	public Vector3 gridToScreen(float x, float y)
	{
		return gridToScreen(new Vector2(x,y));
	}
	
	private Vector3 canvasToWorldSpace(Vector2 position)
	{
		RectTransform rect = canvas.GetComponent<RectTransform>();
		
		return new Vector3(
			position.x*rect.lossyScale.x+rect.position.x,
			position.y*rect.lossyScale.y+rect.position.y,
			rect.position.z + position.y*100
			);
	}

	public Vector3 gridToScreen(Vector2 position)
	{
		Vector2 ret = new Vector3();

		double oinc = 1f + (zUnitHeight / Mathf.Abs(enemyLine.y - partyLine.y));
		double inc = 1.0;

		inc *= Mathf.Pow((float)oinc,position.y);

		inc--;

		ret.y = (float)(enemyLine.y - (zUnitHeight * inc));

		Vector3 ea = new Vector3((float)(enemyLine.x - enemyLineWidth / 2),(float)enemyLine.y,0);
		Vector3 pa = new Vector3((float)(partyLine.x - partyLineWidth / 2),(float)partyLine.y,0);

		ea.x += (float)((((double)position.x) / getColumns()) * enemyLineWidth);
		pa.x += (float)((((double)position.x) / getColumns()) * partyLineWidth);

		ret.x = (float)((ea + ((pa-ea)*(float)((zUnitHeight * inc)/Mathf.Abs(enemyLine.y - partyLine.y)))).x);

		return canvasToWorldSpace(ret);
	}

	public float gridToScale(Vector2 position)
	{
		double oinc = 1f + (zUnitHeight / Mathf.Abs(enemyLine.y - partyLine.y));
		double inc = 1.0;
		
		inc *= Mathf.Pow((float)oinc,position.y);
		
		inc--;
		
		float scale = (float)((zUnitHeight * inc) / Mathf.Abs(enemyLine.y - partyLine.y));

		float ya = spriteScale;
		//float yb = ya * (float)(partyLineWidth / enemyLineWidth); // Correct Scale
		float yb = spriteScaleEnd;

		return yb * scale + ya * (1f - scale);
	}

	public float getPartyPositionFloat(int i)
	{
		while(i<0) i += party.Count;

		i = i % party.Count;

		if(i == 1 && currentPartyMember == 1 && party.Count == 2) return leftCharacterLocation;

		if(i==0)
		{
			return currentCharacterLocation;
		}
		else if(i==1)
		{
			return rightCharacterLocation;
		}
		else if(i == issuer.ci.characterGroup.characters.Count-1)
		{
			return leftCharacterLocation;
		}
		else
		{
			return backCharacterLocation;
		}
	}

	public Vector2 getPartyPosition(float position)
	{
		int col = getColumns();
		int row = getRows();

		float r = position * Mathf.PI * 2;

		float x;
		float y;
		
		x = (Mathf.Sin(r)+1)/2f;
		y = (Mathf.Cos(r)+1)/2f;
		
		x = partyMarginAround+x*(col-(partyMarginAround*2));
		y = row - (partyMarginBelow + y*(col-(partyMarginAround*2)));
		
		return new Vector2(x,y);
	}

	public PlayableCharacter getCurrentPlayableCharacter()
	{
		return issuer.ci.characterGroup.characters[currentPartyMember].pc;
	}

	public void nextInCommand()
	{
		int partyMember = (currentPartyMember + 1) % issuer.ci.characterGroup.characters.Count;

		while(partyMember != currentPartyMember)
		{
			if(!issuer.addedCommand(issuer.ci.characterGroup.characters[partyMember].pc))
			{
				break;
			}

			partyMember = (partyMember + 1) % issuer.ci.characterGroup.characters.Count;
		}

		if(currentPartyMember == partyMember)
		{
			currentPartyMember = partyMember;

			issuer.issue();
		}
		else
		{
			currentPartyMember = partyMember;
		}
	}
	
	public void nextCharacter()
	{
		currentPartyMember++;
	}
	
	public void prevCharacter()
	{
		currentPartyMember--;
	}

	private IEnumerator coroutine = null;
	private int _currentPartyMember;
	public int currentPartyMember
	{
		get
		{
			return _currentPartyMember;
		}
		set
		{
			if(coroutine != null)
			{
				StopCoroutine(coroutine);
			}

			bool inc = value < _currentPartyMember;

			while(value<0) value+=party.Count;
			
			_currentPartyMember = value%party.Count;

			coroutine = chanceCurrentPartyMember(_currentPartyMember,inc);

			StartCoroutine(coroutine);

			GetComponent<BattleInterfaceParty>().select(_currentPartyMember);
		}
	}

	public void positionParty(int newMember)
	{
		for(int i=0; i<party.Count; i++)
		{
			BattlePartyLocation bpl = party[i].GetComponent<BattlePartyLocation>();
			bpl.position = getPartyPositionFloat(i-newMember);
		}
	}

	public float animationTime = 1f;
	public float reverseAnimationThreshold = 0.7f;

	public IEnumerator chanceCurrentPartyMember(int newMember, bool inc)
	{
		float animationTime = this.animationTime;

		float beginTime = Time.time;
		List<float> beginPositions = new List<float>();
		List<float> targetPositions = new List<float>();
		int i = 0;

		for(i=0; i<party.Count; i++)
		{
			BattlePartyLocation bpl = party[i].GetComponent<BattlePartyLocation>();

			float btemp = bpl.position;
			float temp = getPartyPositionFloat(i-newMember);

			if(inc && temp < btemp) temp+=1f;
			if(!inc && temp > btemp) temp-=1f;

			if(Mathf.Abs(temp - btemp) > reverseAnimationThreshold)
			{
				if(temp > btemp) temp -= 1f;
				else temp += 1f;
			}

			beginPositions.Add(btemp);
			targetPositions.Add(temp);
		}

		yield return null;

		float step;

		while(beginTime + animationTime > Time.time)
		{
			step = (Time.time - beginTime)/animationTime;

			step = AnimationTransactions.SinTransaction(step);

			for(i=0; i<party.Count; i++)
			{
				BattlePartyLocation bpl = party[i].GetComponent<BattlePartyLocation>();

				float a = beginPositions[i];
				float b = targetPositions[i];

				if(b > a)
				{
					bpl.position = a + ((b-a)*step);
				}
				else
				{
					bpl.position = a - ((a-b)*step);
				}
			}

			yield return null;
		}

		step = 1f;
		
		for(i=0; i<party.Count; i++)
		{
			BattlePartyLocation bpl = party[i].GetComponent<BattlePartyLocation>();
			bpl.position = targetPositions[i];
		}

		coroutine = null;
	}

	void OnDrawGizmos()
	{
		enemyAreaGizmos();

		gridGizmos();

		partyCircleGizmos();
	}

	void enemyAreaGizmos()
	{
		int col = getColumns();

		Vector3[] vertices = new Vector3[]{
			gridToScreen(col,col),
			gridToScreen(0,col),
			gridToScreen(0,0),
			gridToScreen(col,0)
		};
		
		int i=vertices.Length;
		
		Vector2[] uvs = new Vector2[vertices.Length];
		
		while (i < uvs.Length)
		{
			uvs[i] = new Vector2(vertices[i].x, vertices[i].z);
			i++;
		}
		
		Mesh mesh = new Mesh();
		mesh.vertices = vertices;
		mesh.triangles = new int[] {0,1,2, 0,2,3};
		mesh.normals = new Vector3[] {new Vector3(0,0,1),new Vector3(0,0,1),new Vector3(0,0,1),new Vector3(0,0,1)};
		mesh.uv = uvs;

		Color c = Color.red;
		c.a = 0.3f;

		Gizmos.color = c;
		Gizmos.DrawMesh(mesh);
	}

	void gridGizmos()
	{
		Gizmos.color = Color.white;

		int col = getColumns();
		int row = getRows();
		
		for(int i=0; i<=col; i++)
		{
			Gizmos.DrawLine(gridToScreen(i,0),gridToScreen(i,row));
		}
		
		for(int j=0; j<=row; j++)
		{
			Gizmos.DrawLine(gridToScreen(0,j),gridToScreen(col,j));
		}
	}

	void partyCircleGizmos()
	{
		Gizmos.color = Color.cyan;

		Vector3 prev = gridToScreen(getPartyPosition(0));

		for(int r = 1; r <= 32; r++)
		{
			Vector3 cur = gridToScreen(getPartyPosition(((float)r) / 32f));
			Gizmos.DrawLine(prev,cur);
			prev = cur;
		}
	}

	public void undo()
	{
		PlayableCharacter pc = issuer.popCommand();

		if(pc == null)
			return;

		int newPartyMember = issuer.ci.characterGroup.characters.FindIndex(delegate(CharacterGroup.CharacterLocation obj)
		{
			return obj.pc == pc;
		});

		Debug.Log(newPartyMember);

		if(newPartyMember >= 0)
		{
			currentPartyMember = newPartyMember;
		}
	}
}
