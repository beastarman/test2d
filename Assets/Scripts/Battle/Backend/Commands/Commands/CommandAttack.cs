﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CommandAttack : Command
{
	CharacterMask enemy;

	public CommandAttack(PlayableCharacter source, CharacterMask _enemy) : base(source)
	{
		enemy = _enemy;
	}
	
	public override string getName()
	{
		return "Attack";
	}

	public override BattleAction doIt()
	{
		int damage = enemy.takeHit(new Hit(source.state.attack));

		return new BattleActionHit(source, enemy, damage);
	}

	public class Builder : CommandBuilder
	{
		public Builder(PlayableCharacter pc) : base(pc)
		{
		}

		int key = -1;

		CharacterMask mask = null;

		public override void setTarget(Target target)
		{
			List<CharacterMask> enemy = target.getEnemies(key);
			
			if(enemy == null || enemy.Count == 0) return;
			
			mask = enemy[0];
		}

		public override Command build()
		{
			if(mask == null) return null;

			return new CommandAttack(source,mask);
		}
		
		public override TargetBuilder getTargetBuilder()
		{
			TargetBuilderSingleEnemy t = new TargetBuilderSingleEnemy();

			key = t.getKey();

			return t;
		}
		
		public override string getName()
		{
			return "Attack";
		}
		
		public override string getDescription()
		{
			return "Attacks one enemy with a melee attack";
		}
	}
}
