﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Linq;
using System;
using System.Reflection;
using System.Collections.Generic;

[CustomEditor(typeof(BattleTransform))]
public class BattleTransformDrawer : Editor
{
	public override void OnInspectorGUI()
	{
		serializedObject.Update();

		BattleTransform bt = (BattleTransform)serializedObject.targetObject;

		bt.position = EditorGUILayout.Vector2Field("Position",bt.position);

		serializedObject.ApplyModifiedProperties ();
	}
}
