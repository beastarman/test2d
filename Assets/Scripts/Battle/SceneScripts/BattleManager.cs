﻿using UnityEngine;
using System.Collections;
using System.Threading;
using System.Collections.Generic;

public class BattleManager : MonoBehaviour
{
	Battle battle = null;
	CharacterGroup enemy;
	
	// Use this for initialization
	void Start ()
	{
	}

	public void generateBattle(string groupID)
	{
		enemy = GlobalGameController.Instance.groupGeneratorManager.generate(groupID);

		battle = new Battle(GlobalGameController.Instance.mainGroup,enemy);

		Thread t = new Thread(new ThreadStart(new AI(new AIBaseRandom(),battle.getCommandIssuer(enemy)).assignCommands));
		t.Start();
	}

	public StackCommandIssuer getCommandIssuer()
	{
		if(battle == null)
		{
			generateBattle(GlobalGameController.Instance.getGeneratorIntent());
		}

		return new StackCommandIssuer(battle.getCommandIssuer(GlobalGameController.Instance.mainGroup),this);
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(battle!=null && battle.state == Battle.BattleState.FIGHT)
		{
			Debug.Log(battle.advanceFight());

			GetComponent<BattleInterfaceParty>().update();

			if(battle.state == Battle.BattleState.ISSUE)
			{
				Thread t = new Thread(new ThreadStart(new AI(new AIBaseRandom(),battle.getCommandIssuer(enemy)).assignCommands));
				t.Start();
			}
		}
	}

	private class AI
	{
		AIBase aiBase;
		CommandIssuer issuer;
		
		public AI(AIBase _aiBase, CommandIssuer _issuer)
		{
			aiBase = _aiBase;
			issuer = _issuer;
		}
		
		public void assignCommands()
		{
			foreach (PlayableCharacter pc in issuer.characterGroup.getAliveCharacters())
			{
				issuer.addCommand(aiBase.getCommand(pc,issuer));
			}
			
			issuer.issue();
		}
		
		public bool isValid()
		{
			return aiBase != null;
		}
	}

	Dictionary<CommandIssuer,Stack<PlayableCharacter>> stacks = new Dictionary<CommandIssuer,Stack<PlayableCharacter>>();
	public Stack<PlayableCharacter> getStack(CommandIssuer ci)
	{
		if(!stacks.ContainsKey(ci))
		{
			stacks[ci] = new Stack<PlayableCharacter>();
		}

		return stacks[ci];
	}

	public class StackCommandIssuer : CommandIssuerInterface
	{
		public CommandIssuer ci;
		BattleManager battleManager;

		public StackCommandIssuer(CommandIssuer ci, BattleManager battleManager)
		{
			this.ci = ci;
			this.battleManager = battleManager;
		}

		public void addCommand (Command c)
		{
			battleManager.getStack(ci).Push(c.source);

			ci.addCommand(c);
		}

		public PlayableCharacter popCommand()
		{
			if(battleManager.getStack(ci).Count == 0)
				return null;

			PlayableCharacter pc = battleManager.getStack(ci).Pop();

			removeCommand(pc);

			return pc;
		}

		public void removeCommand (PlayableCharacter pc)
		{
			ci.removeCommand(pc);
		}

		public bool addedCommand (PlayableCharacter pc)
		{
			return ci.addedCommand(pc);
		}

		public bool isLocked ()
		{
			return ci.isLocked();
		}

		public bool issue ()
		{
			return ci.issue();
		}

		public bool issuedCommand ()
		{
			return ci.issuedCommand();
		}

		public void reset ()
		{
			ci.reset();
		}
		
	}
}
