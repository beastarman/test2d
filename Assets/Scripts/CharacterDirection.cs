﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Reflection;
using System.Collections.Generic;

[Serializable]
public class CharacterDirection
{
	public Vector2 direction;
	public Sprite sprite;
}
