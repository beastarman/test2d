﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class QuestWindow : EditorWindow
{
	[MenuItem ("Game/Quest Chart &q")]
	static void Init()
	{
		EditorWindow window = EditorWindow.GetWindow (typeof (QuestWindow), true, "Quest Chart");

		window.wantsMouseMove = true;
	}

	Rect questButton = new Rect(0,0,200,64);
	
	public Vector2 scrollPosition = Vector2.zero;
	public Vector2 scrollPosition2 = Vector2.zero;
	
	bool dirty = false;
	
	Dictionary<string,QuestState> _quests = null;
	
	private Dictionary<string,QuestState> getQuests()
	{
		if(_quests == null)
		{
			_quests = new Dictionary<string,QuestState>();
			Dictionary<string,QuestState> quests = GlobalGameController.Instance.getQuests();
			
			foreach(string key in quests.Keys)
			{
				_quests.Add(key,new QuestState(quests[key]));
			}
		}
		
		return _quests;
	}
	
	private List<string> getQuestsArray()
	{
		List<string> ret = new List<string>();
		
		foreach(string s in getQuests().Keys)
		{
			ret.Add(s);
		}
		
		ret.Sort();
		
		return ret;
	}
	
	void save()
	{
		foreach(KeyValuePair<string,string> pair in renames)
		{
			Debug.LogWarning("Renaming " + pair.Key + " to " + pair.Value + ". Do not forget to update on user scripts.");
		}
		
		GlobalGameController.Instance.setQuests(getQuests());
		_quests = null;
		dirty = false;
	}

	public QuestState currentQuest = null;
	string newName = "";

	Rect getRect(QuestState q)
	{
		Rect questBox = new Rect(questButton);
		questBox.x = q.position.x-questBox.width/2;
		questBox.y = q.position.y-questBox.height/2;

		return questBox;
	}

	QuestState questDown = null;
	Vector2 mouseDiff = Vector2.zero;

	void manageDown(Vector2 padding)
	{
		Rect questArea = new Rect(220, 0, position.width - 220, position.height);
		
		if(questArea.Contains(Event.current.mousePosition))
		{
			Vector2 mousePosition = Event.current.mousePosition - new Vector2(questArea.x,questArea.y) + scrollPosition2 + padding;

			Dictionary<string,QuestState> quests = getQuests();
			foreach(QuestState q in quests.Values)
			{
				if(getRect(q).Contains(mousePosition))
				{
					questDown = q;
					mouseDiff = mousePosition-questDown.position;
				}
			}
		}
	}

	void manageDrag(Vector2 padding)
	{
		if(questDown == null) return;

		Rect questArea = new Rect(220, 0, position.width - 220, position.height);

		if(questArea.Contains(Event.current.mousePosition))
		{
			Vector2 mousePosition = Event.current.mousePosition - new Vector2(questArea.x,questArea.y) + scrollPosition2 + padding;

			if(questDown.position != mousePosition-mouseDiff)
			{
				questDown.position = mousePosition-mouseDiff;
				
				dirty = true;
			}
		}
	}
	
	void completeQuest(string id)
	{
		QuestState q = getQuests()[id];
		
		foreach(string s in q.requires)
		{
			completeQuest(s);
		}
		
		GlobalGameController.Instance.finishQuest(id);
	}
	
	bool fold = false;
	
	List<KeyValuePair<string,string>> renames = new List<KeyValuePair<string,string>>();

	void OnGUI ()
	{
		Dictionary<string,QuestState> quests = getQuests();
		
		Rect area = new Rect(-100,-32,200,64);
		
		foreach(QuestState q in quests.Values)
		{
			area.x = Mathf.Min(area.x, q.position.x-questButton.width);
			area.y = Mathf.Min(area.y, q.position.y-questButton.height);
			
			area.width = Mathf.Max(area.width, q.position.x+questButton.width-area.x);
			area.height = Mathf.Max(area.height, q.position.y+questButton.height-area.y);
		}

		if(Event.current.type == EventType.mouseDown)
		{
			manageDown(new Vector2(area.x,area.y));
		}
		
		if(Event.current.type == EventType.mouseDrag)
		{
			manageDrag(new Vector2(area.x,area.y));
		}
		
		if(Event.current.type == EventType.mouseUp)
		{
			questDown = null;
		}

		scrollPosition = GUI.BeginScrollView(new Rect(0, 0, 220, position.height), scrollPosition, new Rect(0, 0, 200, 400));
		{
			GUILayout.BeginVertical(GUILayout.Width(200));
			{
				if(GUILayout.Button("Create quest",GUILayout.ExpandWidth(true)))
				{
					int i = 0;
					
					string baseName = "quest";
					
					while(quests.ContainsKey(baseName+(++i)));
					
					QuestState q = new QuestState(baseName+i);
					q.position = scrollPosition2 + new Vector2(questButton.width,questButton.height);
					
					quests.Add(q.id,q);
					
					dirty = true;
				}

				if(currentQuest != null)
				{
					GUILayout.Label(currentQuest.id,EditorStyles.boldLabel);
					
					GUILayout.BeginHorizontal();
					{
						GUILayout.Label("New Id",GUILayout.Width(50));
						newName = EditorGUILayout.TextField("",newName,GUILayout.Width(80));
						
						bool validName = true;
						
						if(quests.ContainsKey(newName) || newName.Trim().Length==0) validName = false;
						
						if(GUILayout.Button(validName?"Save Id":"Invalid",GUILayout.Width(70)) && validName && !currentQuest.id.Equals(newName))
						{
							quests.Remove(currentQuest.id);
							quests.Add(newName,currentQuest);
							
							foreach(QuestState q in quests.Values)
							{
								for(int i=0; i<q.requires.Length; i++)
								{
									if(q.requires[i].Equals(currentQuest.id))
									{
										q.requires[i] = newName;
									}
								}
							}
							
							renames.Add(new KeyValuePair<string,string>(currentQuest.id,newName));
							currentQuest.id = newName;
						}
					}
					GUILayout.EndHorizontal();
					
					string descriptionTemp = EditorGUILayout.TextArea(currentQuest.description);
					if(currentQuest.description != descriptionTemp)
					{
						currentQuest.description = descriptionTemp;
						dirty = true;
					}
					
					Vector2 positionTemp = EditorGUILayout.Vector2Field("Position",currentQuest.position);
					if(currentQuest.position != positionTemp)
					{
						currentQuest.position = positionTemp;
						dirty = true;
					}
					
					Color colorTemp = EditorGUILayout.ColorField("Color",currentQuest.color);
					if(currentQuest.color != colorTemp)
					{
						currentQuest.color = colorTemp;
						dirty = true;
					}
					
					fold = EditorGUILayout.Foldout(fold,"Dependences (" + currentQuest.requires.Length + ")");
					
					if(fold)
					{
						EditorGUI.indentLevel++;
						
						List<string> questList = getQuestsArray();
						
						if(GUILayout.Button("Add dependence"))
						{
							List<string> ss = new List<string>(currentQuest.requires);
							ss.Insert(0,questList[0]);
							currentQuest.requires = ss.ToArray();
							
							dirty = true;
						}
						
						questList.Add("REMOVE_DEPENDENCE");
						
						for(int i=0; i<currentQuest.requires.Length; i++)
						{
							List<string> localQuestList = new List<string>(questList);
							List<string> reqsTemp = new List<string>(currentQuest.requires);
							
							localQuestList.RemoveAll(s => (s.Equals(currentQuest.id) || (!s.Equals(currentQuest.requires[i]) && reqsTemp.Contains(s))));
							
							int j = localQuestList.IndexOf(currentQuest.requires[i]);
							
							if(j<0) j=0;
							
							if(j == localQuestList.Count-1)
							{
								List<string> ss = new List<string>(currentQuest.requires);
								ss.RemoveAt(i--);
								currentQuest.requires = ss.ToArray();
							}
							else
							{
								int jj = EditorGUILayout.Popup(j,localQuestList.ToArray());
								
								if(j != jj)
								{
									dirty = true;
								}
								
								j = jj;
								
								currentQuest.requires[i] = localQuestList[j];
							}
						}
						
						EditorGUI.indentLevel--;
					}
				
					if(GUILayout.Button("Complete quest"))
					{
						completeQuest(currentQuest.id);
					}
					
					EditorGUILayout.Space();
					EditorGUILayout.Space();
				
					if(GUILayout.Button("Remove quest"))
					{
						quests.Remove(currentQuest.id);
						
						foreach(QuestState q in quests.Values)
						{
							List<string> ss = new List<string>(q.requires);
							ss.RemoveAll(s => (s.Equals(currentQuest.id)));
							q.requires = ss.ToArray();
						}
						
						dirty = true;
					}
				}
				
				EditorGUILayout.Space();
				EditorGUILayout.Space();
				
				if(GUILayout.Button("Save quests",GUILayout.ExpandWidth(true)))
				{
					save();
				}
			}
			GUILayout.EndVertical();
		}
		GUI.EndScrollView();

		Color t = GUI.backgroundColor;
		GUI.backgroundColor = Color.gray;

		GUI.Box(new Rect(220, 0, position.width - 220, position.height),"");

		GUI.backgroundColor = t;
		
		scrollPosition2 = GUI.BeginScrollView(new Rect(220, 0, position.width - 220, position.height), scrollPosition2, area);
		{
			foreach(QuestState q in quests.Values)
			{
				foreach(string req in q.requires)
				{
					if(!quests.ContainsKey(req) || req.Equals(q.id))
					{
						continue;
					}
					
					Vector2 middle = (q.position + quests[req].position)/2f;
					
					middle.x = Mathf.Ceil(middle.x);
					middle.y = Mathf.Ceil(middle.y);
					
					Drawing.DrawLine(q.position,middle,10f);
					Drawing.DrawLine(middle,quests[req].position);
				}
			}
			
			foreach(QuestState q in quests.Values)
			{	
				Rect questBox = getRect(q);

				t = GUI.backgroundColor;
				GUI.backgroundColor = q.color;
				
				bool done = false;
				
				try
				{
					done = GlobalGameController.Instance.isQuestCompleted(q.id);
				}
				catch
				{
				}
				
				string buttonLabel = q.id  + (done?"*":"");
				buttonLabel += "\n"+q.description;

				if(GUI.Button(questBox,buttonLabel))
				{
					currentQuest = q;
					newName = q.id;
					GUI.FocusControl ("");
					
					if(Event.current.button == 1)
					{
						completeQuest(q.id);
					}
				}

				GUI.backgroundColor = t;
			}
			
			GUI.SetNextControlName("");
		}
		GUI.EndScrollView();
	}
	
	void OnDestroy()
	{
		if(dirty)
		{
			if(EditorUtility.DisplayDialog("Save changes?","Changes were made. Do you want to save them?","Save","Discard"))
			{
				save();
			}
		}
	}
}
