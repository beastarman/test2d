﻿using UnityEngine;
using System.Collections;

public class TitleStretch : MonoBehaviour
{
	void Start()
	{
		transform.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
	}
}
