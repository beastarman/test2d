﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Command
{
	public PlayableCharacter source;

	public Command(PlayableCharacter source)
	{
		this.source = source;
	}

	public virtual string getName()
	{
		return "Wait";
	}

	public virtual int getPriority()
	{
		return source.state.speed;
	}

	public virtual BattleAction doIt()
	{
		return new BattleAction();
	}
}
