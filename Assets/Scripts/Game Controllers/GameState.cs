﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class GameState
{
	public string scene;
	public Memory memory;
	public List<string> completedQuests;
	
	public static GameState crateFromCurrentState()
	{
		GameState gs = new GameState();
		
		gs.scene = Application.loadedLevelName;
		gs.memory = new Memory(GlobalGameController.Instance.getMemory());
		
		Dictionary<string,QuestState> quests = GlobalGameController.Instance.getQuests();
		
		gs.completedQuests = new List<string>();
		
		foreach(QuestState q in quests.Values)
		{
			if(q.done) gs.completedQuests.Add(q.id);
		}
		
		return gs;
	}
}
