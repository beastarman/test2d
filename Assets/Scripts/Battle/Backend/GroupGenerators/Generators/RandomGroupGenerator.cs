﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR

using UnityEditor;

#endif

public class RandomGroupGenerator : GroupGeneratorBase
{
	HashSet<RandomGenerator> groups = new HashSet<RandomGenerator>();
	private static System.Random random = new System.Random();

	private float totalChances
	{
		get
		{
			float ret = 0f;

			foreach(RandomGenerator rg in groups)
			{
				ret += rg.rate;
			}

			return ret;
		}
	}

	public HashSet<RandomGenerator> generatorGroups
	{
		get
		{
			return groups;
		}
		set
		{
			foreach(RandomGenerator gen in value)
			{
				addGroup(gen.generator,gen.rate);
			}
		}
	}
	
	public RandomGroupGenerator() : base()
	{
	}

	public void addGroup(string group, float rate)
	{
		this.groups.Add(new RandomGenerator(group,rate));
	}

	public override CharacterGroup generate()
	{
		float diceRoll = ((float)random.NextDouble())*totalChances;

		CharacterGroup mask = new CharacterGroup();

		foreach(RandomGenerator group in groups)
		{
			if(group.rate >= diceRoll)
			{
				mask = GlobalGameController.Instance.groupGeneratorManager.generators[group.generator].generate();

				break;
			}
			else
			{
				diceRoll -= group.rate;
			}
		}

		return mask;
	}

	public override GroupGeneratorBase clone()
	{
		RandomGroupGenerator that = new RandomGroupGenerator();

		foreach (RandomGenerator g in groups)
		{
			that.addGroup(g.generator,g.rate);
		}

		return that;
	}

	#if UNITY_EDITOR

	public static string classId = "Random Generator";
	
	public override string getID()
	{
		return classId;
	}
	
	public override bool OnGUI(string id)
	{
		bool isDirty = base.OnGUI(id);

		EditorGUILayout.BeginHorizontal();
		{
			EditorGUILayout.BeginVertical(GUILayout.Width(300));
			{
				GUILayout.Space(16);

				RandomGenerator toRemove = null;

				foreach(RandomGenerator group in groups)
				{
					List<string> generatorsList = new List<string>();

					foreach(string s in GlobalGameController.Instance.groupGeneratorManager.generators.Keys)
					{
						if(!s.Equals(id))
						{
							generatorsList.Add(s);
						}
					}

					string[] generators = new string[generatorsList.Count];
					generatorsList.CopyTo(generators,0);

					string newGenerator = Layout_StringPopup("Generator",group.generator,generators);
					isDirty|=!group.generator.Equals(newGenerator);
					group.generator=newGenerator;

					float newRate = EditorGUILayout.FloatField("Rate",group.rate);
					isDirty|=group.rate!=newRate;
					group.rate=newRate;

					if(GUILayout.Button("Remove"))
					{
						toRemove = group;
					}

					GUILayout.Space(16);
				}

				if(toRemove != null)
				{
					groups.Remove(toRemove);
					isDirty = true;
				}

				if(GUILayout.Button("Add new generator"))
				{
					Dictionary<string,GroupGeneratorBase>.Enumerator ie = GlobalGameController.Instance.groupGeneratorManager.generators.GetEnumerator();
					ie.MoveNext();

					groups.Add(new RandomGenerator(ie.Current.Key,1));

					isDirty|=true;
				}
			}
			EditorGUILayout.EndVertical();
		}
		EditorGUILayout.EndHorizontal();

		return isDirty;
	}

	public static string Layout_StringPopup(string label, string selected, string[] displayedOptions, params GUILayoutOption[] options)
	{
		int i = Mathf.Max(0,(new List<string>(displayedOptions)).IndexOf(selected));
		i = EditorGUILayout.Popup(label,i,displayedOptions,options);
		return displayedOptions[i];
	}

	#endif

	public class RandomGenerator
	{
		public string generator;
		public float rate;
		
		public RandomGenerator() : this("",0)
		{
		}
		
		public RandomGenerator(RandomGenerator that) : this(that.generator,that.rate)
		{
		}

		public RandomGenerator(string generator,float rate)
		{
			this.generator = generator;
			this.rate = rate;
		}
	}
}
