﻿using UnityEngine;
using System.Collections;

public class ModifierDefend : Modifier
{
	public ModifierDefend() : base(1)
	{
	}

	public override void onHit(ref Hit hit)
	{
		hit.strength /= 2;
	}
}
