﻿using UnityEngine;
using System.Collections;

public class BattleActionHit : BattleAction
{
	PlayableCharacter source;
	CharacterMask target;
	int damage;

	public BattleActionHit(PlayableCharacter _source, CharacterMask _target, int _damage)
	{
		source = _source;
		target = _target;
		damage = _damage;
	}

	public override string ToString()
	{
		return source.name + " deals " + damage + " damage points to " + target.name;
	}
}
