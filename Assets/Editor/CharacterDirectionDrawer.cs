﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Reflection;
using System.Collections.Generic;

#if UNITY_EDITOR

using UnityEditor;

#endif

[CustomPropertyDrawer(typeof(CharacterDirection))]
public class CharacterDirectionDrawer : PropertyDrawer
{
	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
	{
		EditorGUI.BeginProperty(position,label,property);
		
		position = EditorGUI.PrefixLabel(position,label);
		
		int indent = EditorGUI.indentLevel;
		EditorGUI.indentLevel = 0;
		
		Rect vecPosition = new Rect(position);
		vecPosition.width/=2;
		
		Rect sprPosition = new Rect(vecPosition);
		sprPosition.x+=sprPosition.width;
		
		EditorGUI.PropertyField(new Rect(vecPosition),property.FindPropertyRelative("direction"),GUIContent.none);
		EditorGUI.PropertyField(new Rect(sprPosition),property.FindPropertyRelative("sprite"),GUIContent.none);
		
		EditorGUI.indentLevel = indent;
		
		EditorGUI.EndProperty();
	}
}