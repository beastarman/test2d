﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;

public class ControllerManager : MonoBehaviour
{
	public List<GameObject> startFocusRequests;

	public List<GameObject> focusRequests;

	public enum Key
	{
		UP,
		DOWN,
		LEFT,
		RIGHT,
		ACTION,
		CANCEL,
	}

	private static Dictionary<Key,State> states = new Dictionary<Key,State>();

	private static KeyCode[] getKeyCodes(Key key)
	{
		switch(key)
		{
			case Key.UP:
				return new KeyCode[]{KeyCode.UpArrow};
			case Key.DOWN:
				return new KeyCode[]{KeyCode.DownArrow};
			case Key.LEFT:
				return new KeyCode[]{KeyCode.LeftArrow};
			case Key.RIGHT:
				return new KeyCode[]{KeyCode.RightArrow};
			case Key.ACTION:
				return new KeyCode[]{KeyCode.Z,KeyCode.LeftControl};
			case Key.CANCEL:
				return new KeyCode[]{KeyCode.X,KeyCode.LeftAlt};
			default:
				return new KeyCode[]{};
		}
	}
	
	private static bool getKeyDown(Key key)
	{
		bool state = false;
		
		foreach (KeyCode keyCode in getKeyCodes(key))
		{
			if(Input.GetKey(keyCode) && !Input.GetKeyDown(keyCode)) return false;

			state |= Input.GetKeyDown(keyCode);
		}
		
		return state;
	}
	
	private static bool getKey(Key key)
	{
		bool state = false;
		
		foreach (KeyCode keyCode in getKeyCodes(key))
		{
			state |= Input.GetKey(keyCode);
		}
		
		return state;
	}
	
	private static bool getKeyUp(Key key)
	{
		bool state = false;
		
		foreach (KeyCode keyCode in getKeyCodes(key))
		{
			if(Input.GetKey(keyCode)) return false;

			state |= Input.GetKeyUp(keyCode);
		}
		
		return state;
	}

	private State getState(Key key)
	{
		if(!states.ContainsKey(key))
		{
			states.Add(key,new State());
		}

		return states[key];
	}

	public State getState(GameObject obj, Key key)
	{
		if(focusRequests.Count>0 && focusRequests[focusRequests.Count-1].Equals(obj))
		{
			return getState(key);
		}
		else return new State();
	}
	
	void Awake()
	{
		focusRequests = new List<GameObject>();
		
		foreach(GameObject obj in startFocusRequests)
		{
			focusRequests.Add(obj);
		}
	}

	void Update()
	{
		foreach (Key key in Enum.GetValues(typeof(Key)).Cast<Key>())
		{
			getState(key).passTime(Time.deltaTime);
			getState(key).setState(ControllerManager.getKey(key));
		}
	}

	List<GameObject> requestFocusPending = new List<GameObject>();
	public void requestFocus(GameObject obj)
	{
		requestFocusPending.Add(obj);
	}

	List<GameObject> releaseFocusPending = new List<GameObject>();
	public void releaseFocus(GameObject obj)
	{
		releaseFocusPending.Add(obj);
	}

	void LateUpdate()
	{
		foreach (GameObject obj in requestFocusPending)
		{
			focusRequests.Add(obj);
		}
		requestFocusPending.Clear();
		
		foreach (GameObject obj in releaseFocusPending)
		{
			focusRequests.Remove(obj);
		}
		releaseFocusPending.Clear();
	}
}
