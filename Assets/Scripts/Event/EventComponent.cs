﻿using UnityEngine;
using System.Collections;

public abstract class EventComponent : MonoBehaviour
{
	public string eventName;

	public abstract bool onEvent(GameObject obj);
}
