﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;

#if UNITY_EDITOR

using UnityEditor;

#endif

public class CacheableSprite
{
	private Sprite _sprite;
	private string _spritePath;

	public CacheableSprite()
	{
	}

	public CacheableSprite(string s)
	{
		spritePath = s;
	}

	public CacheableSprite(CacheableSprite s)
	{
		spritePath = s.spritePath;
	}


	#if UNITY_EDITOR

	public CacheableSprite(Sprite s)
	{
		sprite = s;
	}

	#endif

	[XmlIgnoreAttribute]
	public Sprite sprite
	{
		#if UNITY_EDITOR
		
		set
		{
			string[] path = AssetDatabase.GetAssetPath(value).Split(new char[]{'/'},3);
			
			if(path.Length == 3 && path[0].Equals("Assets") && path[1].Equals("Resources"))
			{
				string name = path[2];
				
				_spritePath = name.Substring(0,name.LastIndexOf('.'));
				_sprite = value;
			}
		}
		
		#endif
		
		get
		{
			if(_sprite == null && _spritePath != null)
			{
				_sprite = (Sprite) Resources.Load(_spritePath,typeof(Sprite));
			}
			
			return _sprite;
		}
	}
	
	public string spritePath
	{
		get
		{
			return _spritePath;
		}
		
		set
		{
			if(_spritePath!=null && !_spritePath.Equals(value)) _sprite = null;
			_spritePath = value;
		}
	}

	public void unCacheSprite()
	{
		_sprite = null;
	}
}
