﻿using UnityEngine;
using System.Collections;

public class TitleScreenController : MonoBehaviour
{
	public UnityEngine.UI.Text[] labels;
	
	public ControllerManager.Key _moveDownButton = ControllerManager.Key.DOWN;
	private State moveDown
	{
		get
		{
			return controllerManager.getState(gameObject,_moveDownButton);
		}
	}
	
	public ControllerManager.Key _moveUpButton = ControllerManager.Key.UP;
	private State moveUp
	{
		get
		{
			return controllerManager.getState(gameObject,_moveUpButton);
		}
	}
	
	public ControllerManager.Key _okButton = ControllerManager.Key.ACTION;
	private State ok
	{
		get
		{
			return controllerManager.getState(gameObject,_okButton);
		}
	}
	
	public GameObject arrow;
	
	ControllerManager controllerManager;
	
	void Start ()
	{
		controllerManager = GameObject.FindGameObjectWithTag("SceneManager").GetComponent<ControllerManager>();
	}
	
	int selected = 0;
	
	void Update()
	{
		if(moveDown.isOnStateMax(State.DOWN,0))
		{
			selected++;
		}
		
		if(moveUp.isOnStateMax(State.DOWN,0))
		{
			selected--;
		}
		
		while(selected < 0)
		{
			selected += labels.Length;
		}
		
		selected %= labels.Length;
		
		Vector3 dif = labels[selected].GetComponent<RectTransform>().sizeDelta;
		dif.x/=2;
		dif.y=0;
		dif.z=0;
		
		arrow.transform.position = labels[selected].transform.position - dif;
		
		if(ok.isOnStateMax(State.DOWN,0))
		{
			EventTrigger et = labels[selected].GetComponent<EventTrigger>();
			
			if(et != null)
			{
				et.trigger(gameObject);
			}
		}
	}
}
