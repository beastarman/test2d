﻿using UnityEngine;
using System.Collections;

public class LayerSortY : MonoBehaviour
{
	void Update ()
	{
		GetComponent<SpriteRenderer>().sortingOrder = Mathf.RoundToInt((GetComponent<Collider2D>().bounds.center.y - GetComponent<Collider2D>().bounds.size.y/2) * 10f) * -1;;
	}
}
