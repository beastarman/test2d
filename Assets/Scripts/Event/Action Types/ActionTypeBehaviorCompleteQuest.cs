﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Reflection;
using System.Collections.Generic;

#if UNITY_EDITOR

using UnityEditor;

#endif

public class ActionTypeBehaviorCompleteQuest : ActionTypeBehavior
{
	public ActionTypeBehaviorCompleteQuest(InspectorEventComponent.InspectorEvent.InspectorEventAction iec) : base(iec)
	{
		this.propertyHeight = 16;
	}
	
	public override void resize()
	{
		Array.Resize(ref getInspectorEventAction().extraString,1);
		Array.Resize(ref getInspectorEventAction().extraGameObjects,0);
	}
	
	public string getQuest()
	{
		return safeGetString(0);
	}
	
	public string setQuest(string quest)
	{
		return safeSetString(0,quest);
	}
	
	public override string ToString()
	{
		string q = getQuest();
		
		if(GlobalGameController.Instance.questExists(q))
		{
			return "Complete Quest: " + q;
		}
		else
		{
			return "(!) Complete undefined quest " + q;
		}
	}
	
	public override bool run(InspectorEventComponent component)
	{
		GlobalGameController.Instance.finishQuest(getQuest());
		
		return false;
	}
	
	#if UNITY_EDITOR
	
	public List<string> getQuestsAsList(ref int current)
	{
		List<string> quests = new List<string>();
		
		string q = getQuest();
		current = 0;
		int i=0;
		
		foreach(KeyValuePair<string,QuestState> quest in GlobalGameController.Instance.getQuests())
		{
			quests.Add(quest.Key);
		}
		
		quests.Sort();
		
		quests.Insert(0,"UNDEFINED QUEST");
		
		foreach(string s in quests)
		{
			if(s.Equals(q))
			{
				current = i;
			}
			
			i++;
		}
		
		return quests;
	}
	
	public override void createExtraFields(ref Rect rect)
	{
		rect.height = 16;
	
		int cur = 0;
		
		List<string> quests = getQuestsAsList(ref cur);
		
		int newq = EditorGUI.Popup(new Rect(rect), "Quest", cur, quests.ToArray());
		
		setQuest(quests[newq]);
		
		if(cur != newq)
		{
			setDirty();
		}
		
		rect.y += 16;
	}
	
	#endif
}
