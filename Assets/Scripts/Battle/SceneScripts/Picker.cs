﻿using UnityEngine;
using System.Collections;

public abstract class Picker : MonoBehaviour
{
	protected ControllerManager controllerManager = null;

	void Start()
	{
		controllerManager = GameObject.FindGameObjectWithTag("SceneManager").GetComponent<ControllerManager>();
	}

	protected BattleManager.StackCommandIssuer issuer
	{
		get
		{
			return GameObject.FindGameObjectWithTag("BattleSceneManager").GetComponent<BattleSceneManager>().issuer;
		}
	}

	protected CommandBuilder builder;

	public bool isFocused = false;

	public void focus(CommandBuilder builder)
	{
		isFocused = true;

		this.builder = builder;

		controllerManager.requestFocus(gameObject);

		onFocus();
	}

	public abstract void onFocus();

	public void releaseFocus()
	{
		controllerManager.releaseFocus(gameObject);

		isFocused = false;
	}
}
