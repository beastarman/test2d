﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterGroupMask
{
	public List<CharacterMaskLocation> characters = new List<CharacterMaskLocation>();

	public CharacterGroupMask() : this(new CharacterGroup())
	{
	}

	public CharacterGroupMask(CharacterGroup cg)
	{
		foreach(CharacterGroup.CharacterLocation c in cg.characters)
		{
			characters.Add(new CharacterMaskLocation(c));
		}
	}

	public bool isAlive()
	{
		foreach (CharacterMaskLocation cm in characters)
		{
			if(cm.pc.isAlive()) return true;
		}

		return false;
	}

	public List<CharacterMask> getAliveCharacters()
	{
		List<CharacterMask> ret = new List<CharacterMask>();

		foreach (CharacterMaskLocation cm in characters)
		{
			if(cm.pc.isAlive()) ret.Add(cm.pc);
		}

		return ret;
	}

	public class CharacterMaskLocation
	{
		public CharacterMask pc;
		public Vector2 location;

		public CharacterMaskLocation(CharacterGroup.CharacterLocation loc)
		{
			this.pc = new CharacterMask(loc.pc);
			this.location = loc.location;
		}
		
		public CharacterMaskLocation(CharacterMask pc,Vector2 location)
		{
			this.pc = pc;
			this.location = location;
		}
	}
}
