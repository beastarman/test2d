﻿using UnityEngine;
using System.Collections;

public class CharacterMask
{
	PlayableCharacter character;

	public CharacterMask(PlayableCharacter pc)
	{
		character = pc;
	}

	public Sprite sprite
	{
		get
		{
			return character.sprite;
		}
	}

	public double HP
	{
		get
		{
			return ((double)character.state.curHP)/ character.state.maxHP;
		}
	}

	public string name
	{
		get
		{
			return character.name;
		}
	}

	public int takeHit(Hit hit)
	{
		return character.takeHit(hit);
	}

	public bool isAlive()
	{
		return HP!=0;
	}
}
