﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Reflection;
using System.Collections.Generic;

[Serializable]
public class InspectorEventComponent : EventComponent
{
	public static string LAST_SELECTED_OPTION = "__LastSelectedOption";
	public static string QUEST_IS_OPEN = "QuestIsOpen";
	
	[Serializable]
	public class InspectorEvent
	{
		[Serializable]
		public class InspectorEventAction
		{
			public enum ActionType
			{
				Skip,
				StartDialog,
				ShowDialogMessage,
				EndDialog,
				CompleteQuest,
				FaceCharacter,
				ShowOptions,
				UserSelectOption,
				If,
				Elif,
				Else,
				CloseBlock,
				LoadScene,
				Teleport,
				Log,
				Destroy,
				Move,
				WaitCoroutines,
				Sleep,
				EnableCameraTrack,
				DisableCameraTrack,
				SaveGame,
				LoadGame,
				PlaySound,
				CallScript,
				StartBattle,
			}

			public ActionType type = ActionType.Skip;
			[Multiline]
			public string[] extraString = new string[]{};
			public UnityEngine.Object[] extraGameObjects = new UnityEngine.Object[]{};
			
			public ActionTypeBehavior getActionTypeBehavior()
			{
				return getActionTypeBehavior(type,this);
			}
			
			public static ActionTypeBehavior getActionTypeBehavior(ActionType at, InspectorEventAction iea)
			{
				switch(at)
				{
					case ActionType.Skip:
						return new ActionTypeBehaviorSkip(iea);
					case ActionType.StartDialog:
						return new ActionTypeBehaviorStartDialog(iea);
					case ActionType.ShowDialogMessage:
						return new ActionTypeBehaviorShowDialogMessage(iea);
					case ActionType.EndDialog:
						return new ActionTypeBehaviorEndDialog(iea);
					case ActionType.CompleteQuest:
						return new ActionTypeBehaviorCompleteQuest(iea);
					case ActionType.FaceCharacter:
						return new ActionTypeBehaviorFaceCharacter(iea);
					case ActionType.ShowOptions:
						return new ActionTypeBehaviorShowOption(iea);
					case ActionType.UserSelectOption:
						return new ActionTypeBehaviorUserSelectOption(iea);
					case ActionType.If:
						return new ActionTypeBehaviorIf(iea);
					case ActionType.Elif:
						return new ActionTypeBehaviorElif(iea);
					case ActionType.Else:
						return new ActionTypeBehaviorElse(iea);
					case ActionType.CloseBlock:
						return new ActionTypeBehaviorCloseBlock(iea);
					case ActionType.LoadScene:
						return new ActionTypeBehaviorLoadScene(iea);
					case ActionType.Teleport:
						return new ActionTypeBehaviorTeleport(iea);
					case ActionType.Log:
						return new ActionTypeBehaviorLog(iea);
					case ActionType.Destroy:
						return new ActionTypeBehaviorDestroy(iea);
					case ActionType.Move:
						return new ActionTypeBehaviorMove(iea);
					case ActionType.WaitCoroutines:
						return new ActionTypeBehaviorWaitCoroutines(iea);
					case ActionType.Sleep:
						return new ActionTypeBehaviorSleep(iea);
					case ActionType.EnableCameraTrack:
						return new ActionTypeBehaviorEnableCameraTrack(iea);
					case ActionType.DisableCameraTrack:
						return new ActionTypeBehaviorDisableCameraTrack(iea);
					case ActionType.SaveGame:
						return new ActionTypeBehaviorSaveGame(iea);
					case ActionType.LoadGame:
						return new ActionTypeBehaviorLoadGame(iea);
					case ActionType.PlaySound:
						return new ActionTypeBehaviorPlaySound(iea);
					case ActionType.CallScript:
						return new ActionTypeBehaviorCallScript(iea);
					case ActionType.StartBattle:
						return new ActionTypeBehaviorStartBattle(iea);
					default:
						return new ActionTypeBehavior(iea);
				}
			}

			public GameObject getAsGameObject(int i)
			{
				try
				{
					return (GameObject)extraGameObjects[i];
				}
				catch(InvalidCastException)
				{
					return null;
				}
			}

			public void assertExtra()
			{
				getActionTypeBehavior().resize();
			}

			public override string ToString ()
			{
				return getActionTypeBehavior().ToString();
			}
		}

		public InspectorEventAction[] actions;
	}

	public InspectorEvent inspectorEvent;

	private bool on = false;
	
	public ControllerManager.Key _nextKey = ControllerManager.Key.ACTION;
	public State next
	{
		get
		{
			return controllerManager.getState(gameObject,_nextKey);
		}
	}
	
	public ControllerManager.Key _downKey = ControllerManager.Key.DOWN;
	public State down
	{
		get
		{
			return controllerManager.getState(gameObject,_downKey);
		}
	}
	
	public ControllerManager.Key _upKey = ControllerManager.Key.UP;
	public State up
	{
		get
		{
			return controllerManager.getState(gameObject,_upKey);
		}
	}
	
	public ControllerManager controllerManager
	{
		get
		{
			return GameObject.FindGameObjectWithTag("SceneManager").GetComponent<ControllerManager>();
		}
	}

	Stack<IEnumerator<InspectorEvent.InspectorEventAction>> eventIteratorStack = new Stack<IEnumerator<InspectorEvent.InspectorEventAction>>();
	
	public void pushInspectorEventComponent(InspectorEventComponent component)
	{
		eventIteratorStack.Push(component.inspectorEvent.actions.Cast<InspectorEvent.InspectorEventAction>().GetEnumerator());
	}

	void Update()
	{
		if(on && eventIteratorStack.Count > 0)
		{
			updateAction(eventIteratorStack.Peek().Current);
		}
	}

	private void Lock()
	{
		controllerManager.requestFocus(gameObject);

		StartCoroutine(turnOn());
	}

	private IEnumerator turnOn()
	{
		yield return null;
		
		on = true;
	}

	private void Unlock()
	{
		eventIteratorStack.Pop();
		
		if(eventIteratorStack.Count == 0)
		{
			controllerManager.releaseFocus(gameObject);
	
			StartCoroutine(turnOff());
		}
	}

	private IEnumerator turnOff()
	{
		yield return new WaitForSeconds(0.1f);

		on = false;
	}

	public Stack<int> ifResults = new Stack<int>();

	private bool RunAction(InspectorEvent.InspectorEventAction action)
	{
		action.assertExtra();
		
		if(action.getActionTypeBehavior().preRun(this))
		{
			return false;
		}

		if(ifResults.Count() > 0 && ifResults.Peek() != 1)
		{
			return false;
		}

		return action.getActionTypeBehavior().run(this);
	}

	private void updateAction(InspectorEvent.InspectorEventAction action)
	{
		if(action.getActionTypeBehavior().update(this))
		{
			runActions();
		}
	}

	private void runActions()
	{
		while(eventIteratorStack.Peek().MoveNext())
		{
			InspectorEvent.InspectorEventAction action = eventIteratorStack.Peek().Current;

			if(RunAction(action))
			{
				return;
			}
		}
		
		Unlock();
	}

	private void startEvent(InspectorEvent e)
	{
		Lock();

		eventIteratorStack = new Stack<IEnumerator<InspectorEvent.InspectorEventAction>>();
		eventIteratorStack.Push(e.actions.Cast<InspectorEvent.InspectorEventAction>().GetEnumerator());

		runActions();
	}

	public override bool onEvent(GameObject obj)
	{
		if(on)
		{
			return false;
		}

		startEvent(inspectorEvent);

		return true;
	}
	
	public void StartEventCoroutine(IEnumerator e)
	{
		StartCoroutine(runCoroutine(e));
	}
	
	public int runningCoroutines = 0;
	
	public IEnumerator runCoroutine(IEnumerator e)
	{
		runningCoroutines++;
		
		yield return StartCoroutine(e);
		
		runningCoroutines--;
	}
}
