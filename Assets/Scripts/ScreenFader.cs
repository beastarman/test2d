﻿using UnityEngine;
using System.Collections;

public class ScreenFader : MonoBehaviour
{
	public float fadeSpeed = 1.5f;

	public string startUpScript;

	private float time = 0;

	private bool exiting = false;
	private string scene;

	ControllerManager controllerManager;

	Texture getTexture()
	{
		Texture2D blackTexture = new Texture2D(1,1);
		blackTexture.SetPixel(0,0,Color.black);
		blackTexture.Apply();

		return blackTexture;
	}

	void Start()
	{
		controllerManager = GetComponent<ControllerManager>();
		
		EventComponent[] components = GetComponents<EventComponent>();
		
		foreach(EventComponent component in components)
		{
			if(component.eventName.Equals(GlobalGameController.Instance.getIntent()) || component.eventName.Equals(startUpScript))
			{
				component.onEvent(gameObject);
			}
		}

		Lock();
	}
	
	void Lock()
	{
		controllerManager.requestFocus(gameObject);
	}

	void Unlock()
	{
		controllerManager.releaseFocus(gameObject);
	}

	public void LoadLevel(string s)
	{
		enabled = true;
		exiting = true;
		scene = s;
		time = 0;

		Lock();
	}

	void OnGUI()
	{
		if(exiting)
		{
			time += Time.deltaTime;
			
			Color c = Color.white;
			c.a = Mathf.Min(1,time/fadeSpeed);
			
			GUI.color = c;
			
			Texture t = getTexture();
			
			GUI.DrawTexture(new Rect(0f, 0f, Screen.width, Screen.height),t);
			
			GUI.color = Color.white;
			
			if(c.a > 0.95)
			{
				enabled = false;
				Unlock();
				Application.LoadLevel(scene);
			}
		}
		else
		{
			time += Time.deltaTime;

			Color c = Color.white;
			c.a = 1 - Mathf.Min(1,time/fadeSpeed);

			GUI.color = c;

			Texture t = getTexture();

			GUI.DrawTexture(new Rect(0f, 0f, Screen.width, Screen.height),t);

			GUI.color = Color.white;

			if(c.a < 0.05)
			{
				if(enabled) Unlock();
				enabled = false;
			}
		}
	}
}
