﻿using UnityEngine;
using System.Collections;

public class AnimationTransactions
{
	public static float SinTransaction(float d)
	{
		return (Mathf.Sin((d - 0.5f) * Mathf.PI) + 1.0f) / 2.0f;
	}

	public static float SinTransaction(float d, float power)
	{
		return Mathf.Pow(SinTransaction(d),power);
	}
}
