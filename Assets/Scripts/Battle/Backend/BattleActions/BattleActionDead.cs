﻿using UnityEngine;
using System.Collections;

public class BattleActionDead : BattleAction
{
	PlayableCharacter character;

	public BattleActionDead(PlayableCharacter pc)
	{
		character = pc;
	}

	public override string ToString()
	{
		return character.name + " is dead";
	}
}
