﻿using UnityEngine;
using System.Collections;

public class RepeatTexture : MonoBehaviour
{
	public float scale = 1;

	// Use this for initialization
	void Start ()
	{
		GetComponent<Renderer>().material.mainTextureScale = new Vector2(transform.localScale.x, transform.localScale.y) / scale;
		GetComponent<Renderer>().material.mainTexture.wrapMode = TextureWrapMode.Repeat;
	}
	
	// Update is called once per frame
	void Update ()
	{
	}
}
