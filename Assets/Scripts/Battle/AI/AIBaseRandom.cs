﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AIBaseRandom : AIBase
{
	private static System.Random random = new System.Random();

	public override Command getCommand(PlayableCharacter character, CommandIssuer issuer)
	{
		List<CommandBuilder> builders = character.getAvailableCommands();
		CommandBuilder builder = builders[random.Next(builders.Count)];
		TargetBuilder tb = builder.getTargetBuilder();
		if(tb is TargetBuilderSingleEnemy)
		{
			if(!issuer.enemyGroup.isAlive()) return null;

			TargetBuilderSingleEnemy temp = (TargetBuilderSingleEnemy)tb;
			temp.target = issuer.enemyGroup.characters[random.Next(issuer.enemyGroup.getAliveCharacters().Count)].pc;
		}
		return builder.build(tb.build());
	}
}
