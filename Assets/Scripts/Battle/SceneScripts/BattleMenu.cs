﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class BattleMenu : MonoBehaviour
{
	public Canvas canvas;

	public Vector2 circleCenter;
	public double circleRadius = 1.0f;
	public double menuCenter = 50f;

	public float animationTime = 0.1f;
	
	public RectTransform baseMenuItem;
	public Text menuItemName;
	
	public Sprite notSelectedMenuItem;
	public Sprite selectedMenuItem;
	public Sprite disabledMenuItem;
	public Sprite menuBorder;
	
	public Color notSelectedMenuItemColor;
	public Color selectedMenuItemColor;
	public Color disabledMenuItemColor;
	
	public List<Image> notSelectedItems = new List<Image>();
	public List<Image> selectedItems = new List<Image>();
	public List<Image> disabledItems = new List<Image>();

	PlayableCharacter currentPlayableCharacter
	{
		get
		{
			return GetComponent<BattleSceneManager>().getCurrentPlayableCharacter();
		}
	}

	Menu _menu = null;
	Menu menu
	{
		get
		{
			if(_menu == null)
			{
				_menu = new Menu(this);
			}
			
			return _menu;
		}
	}

	// Use this for initialization
	void Start ()
	{
		menu.generateTransforms();
	}
	
	public void nextItem()
	{
		currentMenuItem++;
	}
	
	public void prevItem()
	{
		currentMenuItem--;
	}
	
	private IEnumerator coroutine = null;
	private int _currentMenuItem;
	public int currentMenuItem
	{
		get
		{
			return _currentMenuItem;
		}
		set
		{
			while(value >= 0 && value < getAvailableActions().Count && !getAvailableActions()[value].isEnabled)
			{
				if(_currentMenuItem > value)
				{
					value--;
				}
				else
				{
					value++;
				}
			}
			
			if(value < 0 || value >= getAvailableActions().Count)
			{
				return;
			}
			
			_currentMenuItem = value;
			
			if(coroutine != null)
			{
				StopCoroutine(coroutine);
			}
			
			coroutine = chanceCurrentMenuItem(_currentMenuItem);
			
			StartCoroutine(coroutine);
			
			menu.selectItem(_currentMenuItem);
			
			//GetComponent<BattleInterfaceParty>().select(_currentPartyMember);
		}
	}
	
	public float menuTransitionTime = 1f;
	
	IEnumerator chanceCurrentMenuItem(int newPosition)
	{
		double a = menu.position;
		double b = (double)newPosition;
		
		yield return null;
		
		float beginTime = Time.time;
		
		float step;
		
		while(beginTime + animationTime > Time.time)
		{
			step = (Time.time - beginTime) / animationTime;
			
			step = AnimationTransactions.SinTransaction(step,2);
			
			if(b > a)
			{
				menu.setPosition(a + ((b-a)*step));
			}
			else
			{
				menu.setPosition(a - ((a-b)*step));
			}
			
			yield return null;
		}
	}
	
	bool clone = false;
	RectTransform betBaseMenuItemClose()
	{
		RectTransform ret;
		
		if(clone)
		{
			ret = Instantiate(baseMenuItem,baseMenuItem.position,baseMenuItem.rotation) as RectTransform;
			ret.SetParent(baseMenuItem.parent);
			ret.localScale = baseMenuItem.localScale;
		}
		else
		{
			ret = baseMenuItem;
		}
		
		clone = true;
		return ret;
	}

	private Vector3 canvasToWorldSpace(Vector2 position)
	{
		RectTransform rect = canvas.GetComponent<RectTransform>();

		return new Vector3(
			position.x*rect.lossyScale.x+rect.position.x,
			position.y*rect.lossyScale.y+rect.position.y,
			rect.position.z
		);
	}

	public void OnDrawGizmos()
	{
		Gizmos.color = Color.yellow;
		
		Vector3 prev = canvasToWorldSpace(menu.getCoordinates(0.0));
		
		for(int r = 1; r <= 32; r++)
		{
			Vector3 cur = canvasToWorldSpace(menu.getCoordinates(((double)r)/32.0));
			Gizmos.DrawLine(prev,cur);
			prev = cur;
		}
		
		Gizmos.color = Color.magenta;
		
		Vector3 menuCenterPosition = canvasToWorldSpace(menu.getRelativeMenuPosition(0));
		Gizmos.DrawLine(menuCenterPosition - new Vector3(100,0,0),menuCenterPosition + new Vector3(100,0,0));
		
		menuCenterPosition = canvasToWorldSpace(menu.getRelativeMenuPosition(1));
		Gizmos.DrawLine(menuCenterPosition - new Vector3(100,0,0),menuCenterPosition + new Vector3(100,0,0));
		
		menuCenterPosition = canvasToWorldSpace(menu.getRelativeMenuPosition(-1));
		Gizmos.DrawLine(menuCenterPosition - new Vector3(100,0,0),menuCenterPosition + new Vector3(100,0,0));
	}

	List<BattleSceneAction> _actions = null;
	public List<BattleSceneAction> getAvailableActions()
	{
		if(_actions == null)
		{
			_actions = new List<BattleSceneAction>(GetComponents<BattleSceneAction>());
			
			_actions.Sort(delegate(BattleSceneAction p1, BattleSceneAction p2)
			              {
				return p1.order.CompareTo(p2.order);
			});
		}
		
		return _actions;
	}

	public void select()
	{
		getAvailableActions()[menu.selectedItem].act(currentPlayableCharacter);
	}
	
	class Menu
	{
		BattleMenu bip;
		double menuPosition = 0.0;
		
		public double position
		{
			get
			{
				return menuPosition;
			}
		}
		
		Vector2 center
		{
			get
			{
				return bip.circleCenter;
			}
		}
		
		double radius
		{
			get
			{
				return bip.circleRadius;
			}
		}
		
		float selectedItemPosition
		{
			get
			{
				return getCoordinates((bip.menuCenter/100.0)*Mathf.PI*2.0).y;
			}
		}
		
		List<RectTransform> transforms;
		
		List<RectTransform> borderBeforeTransforms;
		List<RectTransform> borderAfterTransforms;
		
		public Menu(BattleMenu bip)
		{
			this.bip = bip;
		}
		
		public void generateTransforms()
		{
			transforms = new List<RectTransform>();
			borderBeforeTransforms = new List<RectTransform>();
			borderAfterTransforms = new List<RectTransform>();
			
			foreach(BattleSceneAction action in bip.getAvailableActions())
			{
				RectTransform newRect = bip.betBaseMenuItemClose();
				newRect.SetParent(bip.baseMenuItem.parent);
				newRect.name = action.name;
				transforms.Add(newRect);
				
				newRect.FindChild(bip.menuItemName.name).GetComponent<Text>().text = action.name.ToUpper();
			}
			
			for(int i=5; i>=1; i--)
			{
				RectTransform newRect = bip.betBaseMenuItemClose();
				newRect.SetParent(bip.baseMenuItem.parent);
				newRect.name = "Border";
				newRect.localScale = new Vector3(((float)i)/5,((float)i)/5,((float)i)/5);
				borderBeforeTransforms.Add(newRect);
				
				var children = new List<GameObject>();
				foreach (Transform child in newRect) children.Add(child.gameObject);
				children.ForEach(child => Destroy(child));
				
				newRect.GetComponent<Image>().sprite = bip.menuBorder;
			}
			
			for(int i=5; i>=1; i--)
			{
				RectTransform newRect = bip.betBaseMenuItemClose();
				newRect.SetParent(bip.baseMenuItem.parent);
				newRect.name = "Border";
				newRect.localScale = new Vector3(((float)i)/5,((float)i)/5,((float)i)/5);
				borderAfterTransforms.Add(newRect);
				
				var children = new List<GameObject>();
				foreach (Transform child in newRect) children.Add(child.gameObject);
				children.ForEach(child => Destroy(child));
				
				newRect.GetComponent<Image>().sprite = bip.menuBorder;
			}
			
			setPosition(0.0);
			selectItem(0);
		}
		
		public Vector2 getCoordinates(double position)
		{
			position *= Mathf.PI * 2;
			
			return center + ((float)radius)*(new Vector2(Mathf.Sin((float)position),Mathf.Cos((float)position)));
		}
		
		public Vector2 getRelativeMenuPosition(double position)
		{
			double y = selectedItemPosition;
			
			y += position*bip.baseMenuItem.sizeDelta.y;
			y -= center.y;
			y /= radius;
			
			while(y>1)
			{
				y -= 1;
			}
			
			double rad = (double)Mathf.Acos((float)y);
			
			return getCoordinates(rad / (2 * Mathf.PI));
		}
		
		public void reposition()
		{
			for(int i=0; i<transforms.Count; i++)
			{
				float pos = (float)(menuPosition - i);

				transforms[i].anchoredPosition = getRelativeMenuPosition(pos);
			}
			
			for(int i=1; i<=5; i++)
			{
				borderBeforeTransforms[i-1].anchoredPosition = getRelativeMenuPosition(menuPosition + (0.5 + i/2.0));
			}
			
			for(int i=1; i<=5; i++)
			{
				borderAfterTransforms[i-1].anchoredPosition = getRelativeMenuPosition(menuPosition - ((transforms.Count - 1) + (0.5 + i/2.0)));
			}
		}
		
		public void setPosition(double newPosition)
		{
			menuPosition = newPosition;
			
			reposition();
		}

		public int _selectedItem = 0;
		public int selectedItem
		{
			get
			{
				return _selectedItem;
			}
		}
		
		public void selectItem(int newPosition)
		{
			_selectedItem = newPosition;

			for(int i=0; i<transforms.Count; i++)
			{
				if(!bip.getAvailableActions()[i].isEnabled)
				{
					setStatus(MenuItemStatus.DISABLED,transforms[i]);
				}
				else if(i == newPosition)
				{
					setStatus(MenuItemStatus.SELECTED,transforms[i]);
				}
				else
				{
					setStatus(MenuItemStatus.ENABLED,transforms[i]);
				}
			}
		}
		
		enum MenuItemStatus
		{
			DISABLED,
			ENABLED,
			SELECTED,
		}
		
		private void setStatus(MenuItemStatus status, RectTransform transform)
		{
			Outline[] outlines = transform.FindChild(bip.menuItemName.name).GetComponents<Outline>();
			Image img = transform.GetComponent<Image>();
			
			foreach(Image image in bip.notSelectedItems)
			{
				Image imgTransform = transform.Find(image.name).GetComponent<Image>();
				Color c = imgTransform.color;
				c.a = 0;
				imgTransform.color = c;
			}
			foreach(Image image in bip.selectedItems)
			{
				Image imgTransform = transform.Find(image.name).GetComponent<Image>();
				Color c = imgTransform.color;
				c.a = 0;
				imgTransform.color = c;
			}
			foreach(Image image in bip.disabledItems)
			{
				Image imgTransform = transform.Find(image.name).GetComponent<Image>();
				Color c = imgTransform.color;
				c.a = 0;
				imgTransform.color = c;
			}
			
			if(status == MenuItemStatus.SELECTED)
			{
				img.sprite = bip.selectedMenuItem;
				
				foreach(Outline outline in outlines)
				{
					outline.effectColor = bip.selectedMenuItemColor;
				}
				
				foreach(Image image in bip.selectedItems)
				{
					Image imgTransform = transform.Find(image.name).GetComponent<Image>();
					Color c = imgTransform.color;
					c.a = 1;
					imgTransform.color = c;
				}
			}
			else if(status == MenuItemStatus.ENABLED)
			{
				img.sprite = bip.notSelectedMenuItem;
				
				foreach(Outline outline in outlines)
				{
					outline.effectColor = bip.notSelectedMenuItemColor;
				}
				
				foreach(Image image in bip.notSelectedItems)
				{
					Image imgTransform = transform.Find(image.name).GetComponent<Image>();
					Color c = imgTransform.color;
					c.a = 1;
				}
			}
			else
			{
				img.sprite = bip.disabledMenuItem;
				
				foreach(Outline outline in outlines)
				{
					outline.effectColor = bip.disabledMenuItemColor;
				}
				
				foreach(Image image in bip.disabledItems)
				{
					Image imgTransform = transform.Find(image.name).GetComponent<Image>();
					Color c = imgTransform.color;
					c.a = 1;
					imgTransform.color = c;
				}
			}
		}
	}
}
