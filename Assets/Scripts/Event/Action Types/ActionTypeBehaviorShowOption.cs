﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Reflection;
using System.Collections.Generic;

#if UNITY_EDITOR

using UnityEditor;

#endif

public class ActionTypeBehaviorShowOption : ActionTypeBehavior
{
	public ActionTypeBehaviorShowOption(InspectorEventComponent.InspectorEvent.InspectorEventAction iec) : base(iec)
	{
		this.propertyHeight = 16*3;
	}
	
	public override void resize()
	{
		Array.Resize(ref getInspectorEventAction().extraString,3);
		Array.Resize(ref getInspectorEventAction().extraGameObjects,0);
	}
	
	public List<string> getOptions()
	{
		List<string> options = new List<string>();
			
		for(int i=0; i<getInspectorEventAction().extraString.Count(); i++)
		{
			if(safeGetString(i).Length > 0)
			{
				options.Add(safeGetString(i));
			}
		}
		
		return options;
	}
	
	public string getOption(int i)
	{
		if(i>3) i=2;
		
		return safeGetString(i);
	}
	
	public string setOption(int i, string s)
	{
		if(i>3) i=2;
		
		return safeSetString(i,s);
	}
	
	public override string ToString()
	{
		List<string> options = getOptions();
		
		if(options.Count()>0)
		{
			return "Show Options: " + string.Join("/",options.ToArray());
		}
		else
		{
			return "(!) Show Empty Options";
		}
	}
	
	public override bool run(InspectorEventComponent component)
	{
		GameObject.FindGameObjectWithTag("SceneManager").GetComponent<DialogController>().ShowOptions(getOptions());
		
		return false;
	}
	
	#if UNITY_EDITOR
	
	public override void createExtraFields(ref Rect rect)
	{
		Rect temp = new Rect(rect);
		temp.height = 16;
		
		rect.height = 16*3;
		
		bool dirty = false;
		
		int i=0;
		int j=0;
		
		for(i=0; i<3; i++)
		{
			string a = getOption(i);
			
			string label = "Option ";
			
			if(a.Length>0)
			{
				label+=j++;
			}
			else
			{
				label+="X";
			}
			
			string b = EditorGUI.TextField(new Rect(temp),label, a);
			setOption(i,b);
			temp.y+=16;
			
			if(!a.Equals(b))
			{
				dirty = true;
			}
		}
		
		if(dirty)
		{
			setDirty();
		}
		
		rect.y += 16*3;
	}
	
	#endif
}
