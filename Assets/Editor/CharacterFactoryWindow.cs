﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class CharacterFactoryWindow : EditorWindow
{
	[MenuItem ("Game/Character Factory &c")]
	static void Init()
	{
		EditorWindow window = EditorWindow.GetWindow (typeof (CharacterFactoryWindow), true, "Character Factory");

		window.wantsMouseMove = true;
	}
	
	bool isDirty = false;
	
	CharactersManager _manager = null;
	CharactersManager manager
	{
		get
		{
			if(_manager == null)
			{
				_manager = new CharactersManager(GlobalGameController.Instance.charactersManager);
			}
			
			return _manager;
		}
	}
	
	string generateFactory()
	{
		string baseName = "factory";
		int i = 0;
		
		while(manager.playableCharacterFactory.ContainsKey(baseName+(++i)));
		
		return baseName+i;
	}
	
	string openFactoryName = null;
	CharacterFactory openFactory = null;
	
	string[] getFactoryTypes()
	{
		List<string> types = new List<string>();
		
		types.Add("Default Character Factory");
		types.Add("Linear Character Factory");
		
		return types.ToArray();
	}
	
	string typeFromClass(System.Type c)
	{
		if(c.Equals(typeof(CharacterFactoryLinear)))
		{
			return "Linear Character Factory";
		}
		else if(c.Equals(typeof(CharacterFactory)))
		{
			return "Default Character Factory";
		}
		
		return "";
	}
	
	CharacterFactory factoryFromType(CharacterFactory f, string type)
	{
		if(type.Equals("Linear Character Factory"))
		{
			return new CharacterFactoryLinear(f);
		}
		else
		{
			return new CharacterFactory(f);
		}
	}
	
	void save()
	{
		isDirty = false;
		GlobalGameController.Instance.setCharacterManager(manager);
	}
	
	string newName;
	int level = 1;
	
	void OnGUI()
	{
		EditorGUILayout.BeginVertical();
		{
			EditorGUILayout.BeginHorizontal();
			{
				if(GUILayout.Button("Save" + (isDirty?"*":""), GUI.skin.label, GUILayout.ExpandWidth(false)))
				{
					save();
				}
			}
			EditorGUILayout.EndHorizontal();
			
			Rect tabArea = EditorGUILayout.BeginHorizontal();
			{
				Color t = GUI.backgroundColor;
				GUI.backgroundColor = Color.gray;
				GUI.Box(tabArea,"");
				GUI.backgroundColor = t;
				
				foreach(string factory in manager.playableCharacterFactory.Keys)
				{
					if(GUILayout.Button(factory, GUILayout.ExpandWidth(false)))
					{
						openFactoryName = factory;
						newName = openFactoryName;
						openFactory = manager.playableCharacterFactory[factory];

						GUI.FocusControl("");
					}
				}
				
				if(GUILayout.Button("Create factory", GUILayout.ExpandWidth(false)))
				{
					openFactoryName = generateFactory();
					newName = openFactoryName;
					openFactory = new CharacterFactory(openFactoryName);
					
					manager.playableCharacterFactory[openFactoryName] = openFactory;
					
					isDirty = true;

					GUI.FocusControl ("");
				}
			}
			EditorGUILayout.EndHorizontal();
			
			if(openFactory != null)
			{
				EditorGUILayout.BeginHorizontal();
				{
					EditorGUILayout.BeginVertical(GUILayout.Width(300));
					{
						GUILayout.Label(openFactoryName,EditorStyles.boldLabel);
						
						GUILayout.BeginHorizontal();
						{
							GUILayout.Label("New Id",GUILayout.Width(50));
							newName = EditorGUILayout.TextField("",newName,GUILayout.Width(180));
							
							bool validName = true;
						
							if(manager.playableCharacterFactory.ContainsKey(newName) || newName.Trim().Length==0) validName = false;
							
							if(GUILayout.Button(validName?"Save Id":"Invalid",GUILayout.Width(70)) && validName && !openFactoryName.Equals(newName))
							{
								manager.playableCharacterFactory[newName] = openFactory;
								manager.playableCharacterFactory.Remove(openFactoryName);
								
								openFactoryName = newName;
								
								isDirty = true;
							}
						}
						GUILayout.EndHorizontal();
						
						string type = typeFromClass(openFactory.GetType());
						string newType = GUIExtension.Layout_StringPopup("Factory Type",type,getFactoryTypes());
						
						if(!type.Equals(newType))
						{
							openFactory = factoryFromType(openFactory,newType);

							manager.playableCharacterFactory[openFactoryName] = openFactory;

							isDirty = true;
						}

						if(openFactory.OnGUI())
						{
							isDirty = true;
						}
					}
					EditorGUILayout.EndVertical();
					
					Rect testArea = EditorGUILayout.BeginVertical(GUILayout.ExpandHeight(true));
					{
						Color t = GUI.backgroundColor;
						GUI.backgroundColor = Color.gray;
						GUI.Box(testArea,"");
						GUI.backgroundColor = t;
						
						isDirty |= drawDetailsGUI();
						
					}
					EditorGUILayout.EndVertical();
				}
				EditorGUILayout.EndHorizontal();
			}

			GUI.SetNextControlName("");
		}
		EditorGUILayout.EndVertical();
	}

	int detailsToShow = 0;

	bool drawDetailsGUI()
	{
		Rect r = EditorGUILayout.BeginHorizontal();
		{
			Color t = GUI.backgroundColor;
			GUI.backgroundColor = Color.white;
			GUI.Box(r,"");
			GUI.backgroundColor = t;

			GUILayout.Space(8);
			detailsToShow = GUILayout.Button("Sprites", GUI.skin.label, GUILayout.ExpandWidth(false))? 0: detailsToShow;
			GUILayout.Space(8);
			detailsToShow = GUILayout.Button("Stats", GUI.skin.label, GUILayout.ExpandWidth(false))? 1: detailsToShow;
		}
		EditorGUILayout.EndHorizontal();

		switch(detailsToShow)
		{
			case 1:
				return drawStatsGUI();
			default:
				return drawSpritesGUI();
		}
	}

	bool drawSpritesGUI()
	{
		bool isDirty = false;

		EditorGUILayout.BeginHorizontal();
		{
			GUILayout.Space(8);

			Sprite s = openFactory.sprite.sprite;

			openFactory.sprite.sprite = spriteBox(s,"Battle idle", false);

			if(s != openFactory.sprite.sprite)
			{
				isDirty |= true;
			}

			string toRemove = null;

			foreach(string id in openFactory.spriteDatabase.sprites.Keys)
			{
				CacheableSprite cacheable = openFactory.spriteDatabase.sprites[id];

				s = cacheable.sprite;

				try
				{
					cacheable.sprite = spriteBox(s,SpriteResources.reverse(id).name,true);
					
					if(s != cacheable.sprite)
					{
						isDirty |= true;
					}
				}
				catch(Remove _)
				{
					toRemove = id;

					isDirty |= true;
				}
			}

			if(toRemove != null)
			{
				openFactory.spriteDatabase.sprites.Remove(toRemove);
			}

			string addSprite = addSpriteBox();
			if(addSprite != null)
			{
				openFactory.spriteDatabase.sprites.Add(addSprite,new CacheableSprite());
				isDirty |= true;
			}
		}
		EditorGUILayout.EndHorizontal();
		
		return isDirty;
	}

	SpriteDatabase.SpriteID selectedSprite = null;

	string addSpriteBox()
	{
		List<SpriteDatabase.SpriteID> availableSprites = new List<SpriteDatabase.SpriteID>();

		foreach(SpriteDatabase.SpriteID sid in SpriteResources.sprites)
		{
			if(!openFactory.spriteDatabase.sprites.ContainsKey(sid.id))
			{
				availableSprites.Add(sid);
			}
		}

		if(availableSprites.Count == 0) return null;

		EditorGUILayout.BeginHorizontal();
		{
			GUILayout.Space(8);
			
			Rect r = EditorGUILayout.BeginVertical(GUILayout.Width(200));
			{
				r.x-=8;
				r.width+=16;
				
				Color color = GUI.backgroundColor;
				GUI.backgroundColor = Color.white;
				GUI.Box(r,"");
				GUI.backgroundColor = color;
				
				GUILayout.Space(4);
				
				GUILayout.Label("Add sprite", EditorStyles.boldLabel);

				if(!availableSprites.Contains(selectedSprite)) selectedSprite = availableSprites[0];

				selectedSprite = GUIExtension.Layout_GenericPopup(selectedSprite,availableSprites);

				if(GUILayout.Button("Add"))
				{
					return selectedSprite.id;
				}
				
				GUILayout.Space(8);
			}
			EditorGUILayout.EndVertical();
			
			GUILayout.Space(8);
		}
		EditorGUILayout.EndHorizontal();

		return null;
	}

	Sprite spriteBox(Sprite currentSprite, string label, bool removable)
	{
		Sprite newSprite = null;

		EditorGUILayout.BeginHorizontal();
		{
			GUILayout.Space(8);
			
			Rect r = EditorGUILayout.BeginVertical(GUILayout.Width(200));
			{
				r.x-=8;
				r.width+=16;
				
				Color color = GUI.backgroundColor;
				GUI.backgroundColor = Color.white;
				GUI.Box(r,"");
				GUI.backgroundColor = color;
				
				GUILayout.Space(4);
				
				GUILayout.Label(label, EditorStyles.boldLabel);
				
				newSprite = (Sprite)EditorGUILayout.ObjectField(currentSprite,typeof(Sprite),false);
				
				int h = 200;
				Texture t = null;
				if(newSprite != null)
				{
					t = newSprite.texture;
					h = Mathf.Min(t.height,(int)(((float)t.height) / (t.width / 200f)));
				}
				GUILayout.Box(t,GUILayout.Width(200),GUILayout.Height(h));

				if(removable)
				{
					if(GUILayout.Button("Remove"))
					{
						throw new Remove();
					}
				}
				
				GUILayout.Space(8);
			}
			EditorGUILayout.EndVertical();
			
			GUILayout.Space(8);
		}
		EditorGUILayout.EndHorizontal();

		return newSprite;
	}

	bool drawStatsGUI()
	{
		EditorGUILayout.Space();
		EditorGUILayout.BeginHorizontal();
		{
			GUILayout.Space(10);
			GUI.Box(EditorGUILayout.BeginVertical(GUILayout.Width(300)),"");
			{
				EditorGUILayout.Space();
				level = EditorGUILayout.IntSlider("Level",level,1,100);
				CharacterState state = openFactory.generatePlayableCharacter(level).state;
				CharacterState level100 = openFactory.generatePlayableCharacter(100).state;
				float div = Mathf.Max(level100.maxHP,level100.attack,level100.defense,level100.speed);
				EditorGUI.ProgressBar(EditorGUILayout.GetControlRect(false,16),state.maxHP / div,"MaxHP (" + state.maxHP + ")");
				EditorGUI.ProgressBar(EditorGUILayout.GetControlRect(false,16),state.attack / div,"Attack (" + state.attack + ")");
				EditorGUI.ProgressBar(EditorGUILayout.GetControlRect(false,16),state.defense / div,"Defense (" + state.defense + ")");
				EditorGUI.ProgressBar(EditorGUILayout.GetControlRect(false,16),state.speed / div,"Speed (" + state.speed + ")");
			}
			EditorGUILayout.EndVertical();
		}
		EditorGUILayout.EndHorizontal();

		return false;
	}
	
	void OnDestroy()
	{
		if(isDirty)
		{
			if(EditorUtility.DisplayDialog("Save changes?","Changes were made. Do you want to save them?","Save","Discard"))
			{
				save();
			}
		}
	}

	class Remove : System.Exception
	{
	};
}
