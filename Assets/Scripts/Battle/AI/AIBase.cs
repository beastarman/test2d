﻿using UnityEngine;
using System.Collections;

public abstract class AIBase
{
	public abstract Command getCommand(PlayableCharacter character, CommandIssuer issuer);
}
