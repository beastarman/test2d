﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Reflection;
using System.Collections.Generic;

#if UNITY_EDITOR

using UnityEditor;

#endif

public class ActionTypeBehaviorIf : ActionTypeBehavior
{
	public static KeyValuePair<string,int> EQUALS = new KeyValuePair<string,int>("==",2);
	public static KeyValuePair<string,int> IEQUALS = new KeyValuePair<string,int>("!=",2);
	public static KeyValuePair<string,int> QUEST_IS_OPEN = new KeyValuePair<string,int>("QuestIsOpen",1);
	public static KeyValuePair<string,int> QUEST_IS_COMPLETED = new KeyValuePair<string,int>("QuestIsCompleted",1);
	
	public ActionTypeBehaviorIf(InspectorEventComponent.InspectorEvent.InspectorEventAction iec) : base(iec)
	{
		this.isOpenBlock = true;
		this.propertyHeight = 16*4;
	}
	
	public override void resize()
	{
		Array.Resize(ref getInspectorEventAction().extraString,4);
		Array.Resize(ref getInspectorEventAction().extraGameObjects,0);
	}
	
	public string getArg1()
	{
		return safeGetString(0);
	}
	
	public string getArg2()
	{
		return safeGetString(2);
	}
	
	public string getOp()
	{
		return safeGetString(1);
	}
	
	public bool isNot()
	{
		return safeGetString(3).Length>0;
	}
	
	public string setArg1(string s)
	{
		return safeSetString(0,s);
	}
	
	public string setArg2(string s)
	{
		return safeSetString(2,s);
	}
	
	public string setOp(string s)
	{
		return safeSetString(1,s);
	}
	
	public bool setNot(bool not)
	{
		if(not)
		{
			safeSetString(3,"not");
		}
		else
		{
			safeSetString(3,"");
		}
		
		return not;
	}
	
	public override string ToString()
	{
		string op = getOp();
		
		if(op.Length > 0)
		{
			return "If (" + (isNot()?"NOT ":"") + getArg1() + " " + op + " " + getArg2() + ") {";
		}
		else
		{
			return "If(NOT_DEFINED) {";
		}
	}
	
	public string parseOperand(string op)
	{
		int _ = 0;
		
		if((op.StartsWith("\"") && op.EndsWith("\"")) || (op.StartsWith("'") && op.EndsWith("'")))
		{
			return op.Substring(1,op.Length-2);
		}
		else if(Int32.TryParse(op,out _))
		{
			return op;
		}
		else
		{
			return GlobalGameController.Instance.getMemory().get(op);
		}
	}
	
	public bool test()
	{
		if(getOp().Equals(EQUALS.Key))
		{
			return parseOperand(getArg1()).Equals(parseOperand(getArg2()));
		}
		else if(getOp().Equals(IEQUALS.Key))
		{
			return !parseOperand(getArg1()).Equals(parseOperand(getArg2()));
		}
		else if(getOp().Equals(QUEST_IS_OPEN.Key))
		{
			string[] quests = getArg2().Split(' ');

			foreach(string quest in quests)
			{
				if(GlobalGameController.Instance.isQuestOpen(quest))
				{
					return true;
				}
			}
		}
		else if(getOp().Equals(QUEST_IS_COMPLETED.Key))
		{
			string[] quests = getArg2().Split(' ');

			foreach(string quest in quests)
			{
				if(GlobalGameController.Instance.isQuestCompleted(quest))
				{
					return true;
				}
			}
		}
		
		return false;
	}
	
	public bool runTest()
	{
		if(isNot())
		{
			return !test();
		}
		else
		{
			return test();
		}
	}
	
	public override bool preRun(InspectorEventComponent component)
	{
		if(component.ifResults.Count()==0 || component.ifResults.Peek()==1)
		{
			bool t = runTest();

			component.ifResults.Push(t?1:0);
		}
		else
		{
			component.ifResults.Push(2);
		}
		
		return true;
	}
	
	#if UNITY_EDITOR
	
	public void Add(Dictionary<string,int> ops, KeyValuePair<string,int> pair, string cur, ref int i)
	{
		if(pair.Key.Equals(cur)) i=ops.Count;
		ops.Add(pair.Key,pair.Value);
	}
	
	public List<string> getOps()
	{
		List<string> ret = new List<string>();
		
		ret.Add(EQUALS.Key);
		ret.Add(IEQUALS.Key);
		ret.Add(QUEST_IS_OPEN.Key);
		ret.Add(QUEST_IS_COMPLETED.Key);
		
		return ret;
	}
	
	public Dictionary<string,int> getPossibleOps(string cur, ref int i)
	{
		i=0;
		
		Dictionary<string,int> ops = new Dictionary<string,int>();
		
		Add(ops,EQUALS,cur,ref i);
		Add(ops,IEQUALS,cur,ref i);
		Add(ops,QUEST_IS_OPEN,cur,ref i);
		Add(ops,QUEST_IS_COMPLETED,cur,ref i);
		
		return ops;
	}
	
	public List<string> getQuestsAsList(ref int current, string q)
	{
		List<string> quests = new List<string>();
		
		current = 0;
		int i=0;
		
		foreach(KeyValuePair<string,QuestState> quest in GlobalGameController.Instance.getQuests())
		{
			quests.Add(quest.Key);
		}
		
		quests.Sort();
		
		quests.Insert(0,"UNDEFINED QUEST");
		
		foreach(string s in quests)
		{
			if(s.Equals(q))
			{
				current = i;
			}
			
			i++;
		}
		
		return quests;
	}
	
	public List<string> getPossibleArgs(string s, ref int i)
	{
		i=0;
		
		List<string> ret = new List<string>();
		
		if(LAST_SELECTED_OPTION.Equals(s)) i = ret.Count;
		ret.Add(LAST_SELECTED_OPTION);
		
		return ret;
	}
	
	public override void createExtraFields(ref Rect rect)
	{
		Rect not_rect = new Rect(rect);
		not_rect.height = 16;
		rect.y+=16;
		
		bool on = isNot();
		bool nn = EditorGUI.Toggle(not_rect,"Not",on);
		setNot(nn);
		
		if(on != nn)
		{
			setDirty();
		}
		
		Rect op1_rect = new Rect(rect);
		op1_rect.height = 16;
		
		Rect op_rect = new Rect(op1_rect);
		op_rect.y+=16;
		
		Rect op2_rect = new Rect(op_rect);
		op2_rect.y+=16;
		
		rect.height = 16*3;
		
		string op = getOp();
		int i=0;
		
		Dictionary<string,int> ops = getPossibleOps(op,ref i);
		List<string> opsList = getOps();
		
		int j = EditorGUI.Popup(op_rect, "Operator", i, opsList.ToArray());
		
		setOp(opsList[j]);
		
		if(i != j)
		{
			setDirty();
		}
		
		if(ops[opsList[j]] == 1)
		{
			if(op.Equals(QUEST_IS_OPEN.Key) || op.Equals(QUEST_IS_COMPLETED.Key))
			{
				string op1 = getArg2();
				int cur = -1;
				
				List<string> quests = getQuestsAsList(ref cur, op1);
				
				int newq = EditorGUI.Popup(op1_rect, "Quest", cur, quests.ToArray());
				
				setArg2(quests[newq]);
				
				if(cur != newq)
				{
					setDirty();
				}
			}
			else
			{
				string op1 = getArg2();
				
				string op2 = EditorGUI.TextField(op1_rect,"Operand",op1);
				
				setArg2(op2);
				
				if(!op1.Equals(op2))
				{
					setDirty();
				}
			}
		}
		else
		{
			Rect opField = new Rect(op1_rect);
			opField.width -= 64;
			
			Rect opButton = new Rect(op1_rect);
			opButton.width = 64;
			opButton.x += opField.width;
			
			string op1 = getArg1();
			string op2 = op1;
			
			if(GUI.Button(opButton,"Toggle"))
			{
				Debug.Log(op1);
				
				if(op1.StartsWith("__"))
				{
					op1 = "";
				}
				else
				{
					op1 = LAST_SELECTED_OPTION;
				}
				
				Debug.Log(op1);
			}
			
			if(op1.StartsWith("__"))
			{
				int cur = 0;
				
				List<string> args = getPossibleArgs(op1,ref cur);
				
				int ncur = EditorGUI.Popup(opField,"Operand 1",cur,args.ToArray());
				
				op2 = args[ncur];
				
				if(cur != ncur)
				{
					setDirty();
				}
			}
			else
			{
				op2 = EditorGUI.TextField(opField,"Operand 1",op1);
			}
			
			setArg1(op2);
			
			if(!op1.Equals(op2))
			{
				setDirty();
			}
			
			op1 = getArg2();
			op2 = op1;
			
			opField = new Rect(op2_rect);
			opField.width -= 64;
			
			opButton = new Rect(op2_rect);
			opButton.width = 64;
			opButton.x += opField.width;
			
			if(GUI.Button(opButton,"Toggle"))
			{
				Debug.Log(op1);
				
				if(op1.StartsWith("__"))
				{
					op1 = "";
				}
				else
				{
					op1 = LAST_SELECTED_OPTION;
				}
				
				Debug.Log(op1);
			}
			
			if(op1.StartsWith("__"))
			{
				int cur = 0;
				
				List<string> args = getPossibleArgs(op1,ref cur);
				
				int ncur = EditorGUI.Popup(opField,"Operand 2",cur,args.ToArray());
				
				op2 = args[ncur];
				
				if(cur != ncur)
				{
					setDirty();
				}
			}
			else
			{
				op2 = EditorGUI.TextField(opField,"Operand 2",op1);
			}
			
			setArg2(op2);
			
			if(!op1.Equals(op2))
			{
				setDirty();
			}
			
			if(!op1.Equals(op2))
			{
				setDirty();
			}
		}
		
		rect.y += 16*3;
	}
	
	#endif
}
