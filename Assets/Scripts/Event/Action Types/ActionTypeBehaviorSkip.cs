﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Reflection;
using System.Collections.Generic;

public class ActionTypeBehaviorSkip : ActionTypeBehavior
{
	public ActionTypeBehaviorSkip(InspectorEventComponent.InspectorEvent.InspectorEventAction iec) : base(iec)
	{}
	
	public override string ToString()
	{
		return "Skip";
	}
}
