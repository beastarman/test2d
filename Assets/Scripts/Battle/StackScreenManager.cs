﻿using UnityEngine;
using System.Collections;

public class StackScreenManager : MonoBehaviour
{
	ControllerManager controller;

	public GameObject focusRequest = null;
	
	void Awake()
	{
		controller = GameObject.FindGameObjectWithTag("SceneManager").GetComponent<ControllerManager>();

		if(focusRequest == null)
		{
			controller.requestFocus(gameObject);
		}
		else
		{
			controller.requestFocus(focusRequest);
		}
	}
}
