﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Reflection;
using System.Collections.Generic;

#if UNITY_EDITOR

using UnityEditor;

#endif

public class ActionTypeBehaviorMove : ActionTypeBehavior
{
	public ActionTypeBehaviorMove(InspectorEventComponent.InspectorEvent.InspectorEventAction iec) : base(iec)
	{
		this.propertyHeight = 16*3;
	}
	
	public override void resize()
	{
		Array.Resize(ref getInspectorEventAction().extraGameObjects,2);
		Array.Resize(ref getInspectorEventAction().extraString,1);
	}
	
	public GameObject getWho()
	{
		return safeGetGameObject(0);
	}
	
	public GameObject setWho(GameObject g)
	{
		return safeSetGameObject(0,g);
	}
	
	public GameObject getWhere()
	{
		return safeGetGameObject(1);
	}
	
	public GameObject setWhere(GameObject g)
	{
		return safeSetGameObject(1,g);
	}
	
	public float getSpeed()
	{
		string s = safeGetString(0);
		
		try
		{
			return Convert.ToSingle(s);
		}
        catch (FormatException)
        {
            return 5;
        }
        catch (OverflowException)
        {
            return 5;
        }
	}
	
	public float setSpeed(float speed)
	{
		safeSetString(0,speed.ToString());
		
		return speed;
	}
	
	public override string ToString()
	{
		GameObject who = getWho();
		GameObject where = getWhere();
		bool error = false;
		
		string ret = "Move ";
		
		if(who != null)
		{
			ret += who.name + " to ";
		}
		else
		{
			error = true;
			ret += "UNDEFINED to ";
		}
		
		if(where != null)
		{
			ret += where.name;
		}
		else
		{
			error = true;
			ret += "UNDEFINED";
		}
		
		if(error) ret = "(!) " + ret;
		
		return ret;
	}
	
	public override bool run(InspectorEventComponent component)
	{
		GameObject who = getWho();
		GameObject where = getWhere();
		
		if(who.transform.position != where.transform.position)
		{
			component.StartEventCoroutine(move());
		}
		
		return false;
	}
	
	public IEnumerator move()
	{
		GameObject who = getWho();
		GameObject where = getWhere();
		
		if(who.transform.position == where.transform.position)
		{
			yield break;
		}
		
		if(who.GetComponent<Collider2D>()!=null)
		{
			who.GetComponent<Collider2D>().enabled=false;
		}
		
		Character c = who.GetComponent<Character>();
		
		while(true)
		{
			Vector3 dir = where.transform.position - who.transform.position;
			dir.z = 0;
			Vector3 odir = dir;
			
			dir = dir.normalized*getSpeed()*Time.deltaTime;
			
			if(dir.magnitude >= odir.magnitude)
			{
				if(c == null)
				{
					who.transform.position += odir;
				}
				else
				{
					c.move(odir);
				}
				
				break;
			}
			else
			{
				if(c == null)
				{
					who.transform.position += dir;
				}
				else
				{
					c.move(dir);
				}
				
				yield return null;
			}
		}
		
		if(who.GetComponent<Collider2D>()!=null)
		{
			who.GetComponent<Collider2D>().enabled=true;
		}
	}
	
	#if UNITY_EDITOR
	
	public override void createExtraFields(ref Rect rect)
	{
		rect.height = 16;
	
		GameObject who = getWho();
		
		GameObject newDoctor = (GameObject) EditorGUI.ObjectField(new Rect(rect),"Who", who, typeof(GameObject), true);
		setWho(newDoctor);
		
		if(who!=newDoctor && !((newDoctor!=null && newDoctor.Equals(who)) || (who!=null && who.Equals(newDoctor))))
		{
			setDirty();
		}
		
		rect.y += 16;
	
		who = getWhere();
		
		newDoctor = (GameObject) EditorGUI.ObjectField(new Rect(rect),"Where", who, typeof(GameObject), true);
		setWhere(newDoctor);
		
		if(who!=newDoctor && !((newDoctor!=null && newDoctor.Equals(who)) || (who!=null && who.Equals(newDoctor))))
		{
			setDirty();
		}
		
		rect.y += 16;
	
		float speed = getSpeed();
		
		float newSpeed = EditorGUI.FloatField(new Rect(rect),"Speed", speed);
		setSpeed(newSpeed);
		
		if(speed != newSpeed)
		{
			setDirty();
		}
		
		rect.y += 16;
	}
	
	#endif
}
