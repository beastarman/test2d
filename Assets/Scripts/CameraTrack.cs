﻿using UnityEngine;
using System.Collections;

public class CameraTrack : MonoBehaviour
{
	public Transform follow;
	public float trackDelay = 0.1f;

	// Use this for initialization
	void Start ()
	{
		Vector3 position = follow.transform.position;
		
		position.z = this.transform.position.z;

		this.transform.position = position;
	}

	IEnumerator delayedFollow(Vector3 position)
	{
		yield return new WaitForSeconds(trackDelay);
		
		position.z = this.transform.position.z;

		this.transform.position = position;
	}

	// Update is called once per frame
	void Update ()
	{
		Vector3 position = follow.transform.position;

		StartCoroutine(delayedFollow(position));
	}
}
