public class State
	{
		public static bool UP = false;
		public static bool DOWN = true;
		
		public float timeWithoutChange;
		public bool state;
		
		public State()
		{
			state = UP;
			timeWithoutChange = 0f;
		}
		
		public bool isOnState(bool newState)
		{
			return state == newState;
		}
	
		public bool isOnStateMin(bool newState, float minTime)
		{
			return isOnState(newState) && minTime <= timeWithoutChange;
		}
	
		public bool isOnStateMax(bool newState, float minTime)
		{
			return isOnState(newState) && minTime >= timeWithoutChange;
		}
		
		public bool setState(bool newState)
		{
			if(state == newState)
			{
				return false;
			}
			else
			{
				state = newState;
				timeWithoutChange = 0f;
				
				return true;
			}
		}
		
		public void passTime(float time)
		{
			timeWithoutChange += time;
		}
	}