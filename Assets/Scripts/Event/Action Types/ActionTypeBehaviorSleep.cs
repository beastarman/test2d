﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Reflection;
using System.Collections.Generic;

#if UNITY_EDITOR

using UnityEditor;

#endif

public class ActionTypeBehaviorSleep : ActionTypeBehavior
{
	public ActionTypeBehaviorSleep(InspectorEventComponent.InspectorEvent.InspectorEventAction iec) : base(iec)
	{
		this.propertyHeight = 16;
	}
	
	public override void resize()
	{
		Array.Resize(ref getInspectorEventAction().extraString,2);
	}
	
	public float getTime()
	{
		string s = safeGetString(0);
		
		try
		{
			return Mathf.Max(0,Convert.ToSingle(s));
		}
        catch (FormatException)
        {
            return 1;
        }
        catch (OverflowException)
        {
            return 1;
        }
	}
	
	public float setTime(float speed)
	{
		safeSetString(0,speed.ToString());
		
		return speed;
	}
	
	public bool getDone()
	{
		return safeGetString(1).Length>0;
	}
	
	public void setDone(bool done)
	{
		if(done)
		{
			safeSetString(1,"DONE");
		}
		else
		{
			safeSetString(1,"");
		}
	}
	
	public override string ToString()
	{
		float time = getTime();
		
		if(time <= 0)
		{
			return "(!) Wait for nothing!";
		}
		else
		{
			return "Wait for " + time + " seconds";
		}
	}
	
	public override bool run(InspectorEventComponent component)
	{
		setDone(false);
		
		component.StartCoroutine(sleep());
		
		return true;
	}
	
	public IEnumerator sleep()
	{
		yield return new WaitForSeconds(getTime());
		
		setDone(true);
	}
	
	public override bool update(InspectorEventComponent component)
	{
		return getDone();
	}
	
	#if UNITY_EDITOR
	
	public override void createExtraFields(ref Rect rect)
	{
		rect.height = 16;
	
		float time = getTime();
		
		float newTime = EditorGUI.FloatField(new Rect(rect),"Time", time);
		setTime(newTime);
		
		if(time != newTime)
		{
			setDirty();
		}
		
		rect.y += 16;
	}
	
	#endif
}
