﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Reflection;
using System.Collections.Generic;

#if UNITY_EDITOR

using UnityEditor;

#endif

public class ActionTypeBehaviorLoadScene : ActionTypeBehavior
{
	public ActionTypeBehaviorLoadScene(InspectorEventComponent.InspectorEvent.InspectorEventAction iec) : base(iec)
	{
		this.propertyHeight = 16*3;
	}
	
	public override void resize()
	{
		Array.Resize(ref getInspectorEventAction().extraString,2);
		Array.Resize(ref getInspectorEventAction().extraGameObjects,0);
	}
	
	public string getScene()
	{
		return safeGetString(0);
	}
	
	public string setScene(string scene)
	{
		return safeSetString(0,scene);
	}
	
	public string getIntent()
	{
		return safeGetString(1);
	}
	
	public string setIntent(string intent)
	{
		return safeSetString(1,intent);
	}
	
	public override string ToString()
	{
		string scene = getScene();
		string intent = getIntent();
		
		if(scene.Length>0)
		{
			if(intent.Length>0)
			{
				return "Load Scene " + scene + " (" + intent + ")";
			}
			else
			{
				return "Load Scene " + scene;
			}
		}
		else
		{
			return "(!) Load no scene";
		}
	}
	
	public override bool run(InspectorEventComponent component)
	{
		ScreenFader fader = GameObject.FindGameObjectWithTag("SceneManager").GetComponent<ScreenFader>();

		if(fader != null)
		{
			GlobalGameController.Instance.LoadLevel(fader,getScene(),getIntent());
		}
		else
		{
			Application.LoadLevel(getScene());
		}
		
		return false;
	}
	
	#if UNITY_EDITOR
	
	public override void createExtraFields(ref Rect rect)
	{
		rect.height = 16*3;
		Rect temp = new Rect(rect);
		temp.height = 16;
		
		string a = getScene();
		string b;
		
		EditorGUI.LabelField(new Rect(temp),"Scene",a);
		
		temp.y+=16;
		UnityEngine.Object obj = EditorGUI.ObjectField(new Rect(temp),"Drop Scene Here",null,typeof(UnityEngine.Object),true);
		
		if(obj != null && obj.ToString().Contains(" (UnityEngine.SceneAsset)"))
		{
			b = obj.ToString().Substring(0,obj.ToString().IndexOf(" (UnityEngine.SceneAsset)"));
		}
		else
		{
			b = a;
		}
		
		setScene(b);
		
		if(!a.Equals(b))
		{
			setDirty();
		}
		
		temp.y+=16;
	
		a = getIntent();
		b = EditorGUI.TextField(new Rect(temp),"Intent", a);
		setIntent(b);
		
		if(!a.Equals(b))
		{
			setDirty();
		}
		
		rect.y += 16*2;
	}
	
	#endif
}
