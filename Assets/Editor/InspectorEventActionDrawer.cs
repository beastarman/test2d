﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Reflection;
using System.Collections.Generic;

#if UNITY_EDITOR

using UnityEditor;

#endif

[CustomPropertyDrawer(typeof(InspectorEventComponent.InspectorEvent.InspectorEventAction))]
public class InspectorEventActionDrawer : PropertyDrawer
{
	Dictionary<InspectorEventComponent.InspectorEvent.InspectorEventAction,Boolean> showContent = new Dictionary<InspectorEventComponent.InspectorEvent.InspectorEventAction,Boolean>();
	
	int localIndentLevel = 0;
	
	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
	{
		if(property.propertyPath.Contains(".actions.Array.data[0]"))
		{
			localIndentLevel = 0;
		}
		
		InspectorEventComponent.InspectorEvent.InspectorEventAction action = getInspectorEventAction(property);
		
		if(action == null)
		{
			return;
		}
		
		if(action.getActionTypeBehavior().closeBlock())
		{
			localIndentLevel--;
		}
		
		EditorGUI.indentLevel += localIndentLevel*2;
		
		label.text = action.ToString();
		
		if(!showContent.ContainsKey(action))
		{
			showContent[action] = false;
		}
		
		EditorGUI.BeginProperty(position,label,property);
		
		Rect foldoutRect = new Rect(position);
		foldoutRect.height = 16;
		
		showContent[action] = EditorGUI.Foldout(new Rect(foldoutRect),showContent[action],label,true);
		
		if(showContent[action])
		{
			action.assertExtra();
			
			int indent = EditorGUI.indentLevel;
			
			Rect typeRect = new Rect(position);
			typeRect.y += 16;
			typeRect.height = 16;
			
			EditorGUI.indentLevel++;
			
			EditorGUI.PropertyField(new Rect(typeRect),property.FindPropertyRelative("type"),new GUIContent("Type"));
			
			typeRect.y += 16;
			
			action.assertExtra();
			
			createExtraFields(action,ref typeRect,property);
			
			EditorGUI.indentLevel = indent;
		}
		
		if(action.getActionTypeBehavior().openBlock())
		{
			localIndentLevel++;
		}
		
		EditorGUI.indentLevel -= localIndentLevel*2;
		
		EditorGUI.EndProperty();
	}
	
	private void createExtraFields(InspectorEventComponent.InspectorEvent.InspectorEventAction action, ref Rect rect, SerializedProperty property)
	{
		EditorStyles.textField.wordWrap = true;
		
		ActionTypeBehavior beh = action.getActionTypeBehavior();
		beh.createExtraFields(ref rect);
		
		if(beh.dirty)
		{
			EditorUtility.SetDirty(property.serializedObject.targetObject);
		}
	}
	
	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		InspectorEventComponent.InspectorEvent.InspectorEventAction action = getInspectorEventAction(property);
		
		if(action == null)
		{
			return 16;
		}
		
		if(!showContent.ContainsKey(action))
		{
			showContent[action] = false;
		}
		
		if(showContent[action])
		{
			return 16*2 + action.getActionTypeBehavior().GetPropertyHeight();
		}
		else
		{
			return 16;
		}
	}
	
	public InspectorEventComponent.InspectorEvent.InspectorEventAction getInspectorEventAction(SerializedProperty property)
	{
		InspectorEventComponent.InspectorEvent.InspectorEventAction action = (InspectorEventComponent.InspectorEvent.InspectorEventAction)GetParent(property);
		
		return action;
	}
	
	public InspectorEventComponent.InspectorEvent getInspectorEvent(SerializedProperty property)
	{
		string path = property.propertyPath.Replace(".Array.data[", "[");
		
		string[] parts = path.Split('.');
		
		Array.Resize(ref parts,parts.Length-1);
		
		InspectorEventComponent.InspectorEvent inspector = (InspectorEventComponent.InspectorEvent)GetParent(property,string.Join(".",parts));
		
		return inspector;
	}
	
	public object GetParent(SerializedProperty prop)
	{
		string path = prop.propertyPath.Replace(".Array.data[", "[");
		
		return GetParent(prop,path);
	}
	
	public object GetParent(SerializedProperty prop, string path)
	{
		object obj = prop.serializedObject.targetObject;
		var elements = path.Split('.');
		
		foreach(var element in elements)
		{
			if(element.Contains("["))
			{
				var elementName = element.Substring(0, element.IndexOf("["));
				var index = Convert.ToInt32(element.Substring(element.IndexOf("[")).Replace("[","").Replace("]",""));
				obj = GetValue(obj, elementName, index);
			}
			else
			{
				obj = GetValue(obj, element);
			}
		}
		return obj;
	}
	
	public object GetValue(object source, string name)
	{
		if(source == null)
			return null;
		var type = source.GetType();
		var f = type.GetField(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
		if(f == null)
		{
			var p = type.GetProperty(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
			if(p == null)
				return null;
			return p.GetValue(source, null);
		}
		return f.GetValue(source);
	}
	
	public object GetValue(object source, string name, int index)
	{
		var enumerable = GetValue(source, name) as IEnumerable;
		var enm = enumerable.GetEnumerator();
		while(index-- >= 0)
			enm.MoveNext();
		try
		{
			return enm.Current;
		}
		catch(InvalidOperationException)
		{
			return null;
		}
	}
}
