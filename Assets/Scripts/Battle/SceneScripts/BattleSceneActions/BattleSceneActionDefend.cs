﻿using UnityEngine;
using System.Collections;

public class BattleSceneActionDefend : BattleSceneAction
{
	TargetBuilder targetBuilder = new TargetBuilderNoOne();

	protected override void runAction(PlayableCharacter pc)
	{
		commandIssuer.addCommand(new CommandDefend.Builder(pc).build(targetBuilder.build()));

		finishAction();
	}
}
