﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;

#if UNITY_EDITOR

using UnityEditor;

#endif

public class GlobalGameController
{
	private static GlobalGameController instance = null;

	Dictionary<string,QuestState> quests = new Dictionary<string,QuestState>();
	CharactersManager _charactersManager = null;
	GroupGeneratorManager _groupGeneratorManager = null;
	
	Memory mem;

	private GlobalGameController()
	{
		loadQuestFile();
		loadCharacterManagerFile();
		loadCharacterGroupFile();
		
		mem = new Memory();
	}
	
	private void loadQuestFile()
	{
		quests = new Dictionary<string,QuestState>();
		
		System.Xml.Serialization.XmlSerializer reader = new System.Xml.Serialization.XmlSerializer(typeof(List<QuestState>));
        TextAsset textAsset = (TextAsset) Resources.Load("quests");
        
		List<QuestState> states = new List<QuestState>();
		states = (List<QuestState>) reader.Deserialize(new StringReader(textAsset.text));
		
		foreach(QuestState q in states)
		{
			q.done = false;
			q.fix();
			quests.Add(q.id,q);
		}
	}
	
	private void loadCharacterManagerFile()
	{
		System.Xml.Serialization.XmlSerializer reader = new System.Xml.Serialization.XmlSerializer(typeof(CharactersManager));
        TextAsset textAsset = (TextAsset) Resources.Load("chars");
        
		_charactersManager = new CharactersManager();

		if (textAsset != null)
		{
			try
			{
				_charactersManager = (CharactersManager)reader.Deserialize (new StringReader (textAsset.text));
			}
			catch(XmlException)
			{
			}
		}
	}
	
	private void loadCharacterGroupFile()
	{
		System.Xml.Serialization.XmlSerializer reader = new System.Xml.Serialization.XmlSerializer(typeof(GroupGeneratorManager));
		TextAsset textAsset = (TextAsset) Resources.Load("char_group");
		
		_groupGeneratorManager = new GroupGeneratorManager();
		
		if (textAsset != null)
		{
			try
			{
				_groupGeneratorManager = (GroupGeneratorManager)reader.Deserialize (new StringReader (textAsset.text));
			}
			catch(XmlException)
			{
			}
		}
	}

	public static GlobalGameController Instance
	{
		get
		{
			if(instance == null)
			{
				instance = new GlobalGameController();
			}

			return instance;
		}
	}
	
	public bool questExists(string id)
	{
		return quests.ContainsKey(id);
	}

	public bool isQuestOpen(string id)
	{
		QuestState state = quests[id];
		
		if(state.done) return false;

		foreach(string quest in state.requires)
		{
			if(!quests[quest].done) return false;
		}

		return true;
	}
	
	public bool isQuestCompleted(string id)
	{
		return quests[id].done;
	}

	public bool finishQuest(string id)
	{
		if(!isQuestOpen(id)) return false;

		quests[id].done = true;

		return true;
	}

	public bool recursiveFinishQuest(string id)
	{
		QuestState state = quests[id];
		
		foreach(string quest in state.requires)
		{
			recursiveFinishQuest(quest);
		}
		
		quests[id].done = true;

		return true;
	}
	
	public Dictionary<string,QuestState> getQuests()
	{
		return new Dictionary<string,QuestState>(quests);
	}
	
	public void setQuests(Dictionary<string,QuestState> _quests)
	{
		quests = new Dictionary<string,QuestState>();
		
		System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(typeof(List<QuestState>));
        StreamWriter file = new StreamWriter(Application.dataPath + "/Resources/quests.xml");
        
        List<QuestState> states = new List<QuestState>();
		
		foreach(string key in _quests.Keys)
		{
			QuestState qTemp = new QuestState(_quests[key]);
			qTemp.done = false;
			
			quests.Add(key,qTemp);
			states.Add(qTemp);
		}
		
		writer.Serialize(file,states);
		
        file.Close();
		
		#if UNITY_EDITOR

		AssetDatabase.Refresh();
		
		#endif
        
        loadQuestFile();
	}
	
	public void setCharacterManager(CharactersManager manager)
	{
		_charactersManager = new CharactersManager(manager);
		
		System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(typeof(CharactersManager));
        StreamWriter file = new StreamWriter(Application.dataPath + "/Resources/chars.xml");
        
        writer.Serialize(file,_charactersManager);
        
        file.Close();
		
		#if UNITY_EDITOR

		AssetDatabase.Refresh();
		
		#endif
        
        loadCharacterManagerFile();
	}

	public void setGroupGeneratorManager(GroupGeneratorManager manager)
	{
		_groupGeneratorManager = new GroupGeneratorManager(manager);
		
		System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(typeof(GroupGeneratorManager));
		StreamWriter file = new StreamWriter(Application.dataPath + "/Resources/char_group.xml");
		
		writer.Serialize(file,_groupGeneratorManager);
		
		file.Close();
		
		#if UNITY_EDITOR
		
		AssetDatabase.Refresh();
		
		#endif
		
		loadCharacterGroupFile();
	}

	public void newGame()
	{
	}

	public string loadGame()
	{
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Open(Application.persistentDataPath + "/Save001.gs", FileMode.Open);
		GameState state = (GameState) bf.Deserialize(file);
		file.Close();
		
		mem = new Memory(state.memory);
		
		foreach(string quest in state.completedQuests)
		{
			recursiveFinishQuest(quest);
		}
		
		setIntent("onLoad");
		
		return state.scene;
	}

	public void saveGame()
	{
		GameState state = GameState.crateFromCurrentState();
		
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create (Application.persistentDataPath + "/Save001.gs");
		bf.Serialize(file, state);
		file.Close();
		
		Debug.Log("Saved to " + Application.persistentDataPath + "/Save001.gs");
	}
	
	public static string INTENT = "__SceneIntent";
	
	public string getIntent()
	{
		return getMemory().get(INTENT);
	}
	
	public string setIntent(string intent)
	{
		return getMemory().put(INTENT,intent);
	}
	
	public static string __BATTLE_GEN_INTENT = "__BattleGeneratorIntent";
	
	public string getGeneratorIntent()
	{
		return getMemory().get(__BATTLE_GEN_INTENT);
	}
	
	public string setGeneratorIntent(string intent)
	{
		return getMemory().put(__BATTLE_GEN_INTENT,intent);
	}
	
	public void LoadLevel(ScreenFader fader, string scene)
	{
		fader.LoadLevel(scene);
	}
	
	public void LoadLevel(ScreenFader fader, string scene, string intent)
	{
		setIntent(intent);
		fader.LoadLevel(scene);
	}
	
	public Memory getMemory()
	{
		return mem;
	}
	
	public CharactersManager charactersManager
	{
		get
		{
			if(_charactersManager == null)
			{
				_charactersManager = new CharactersManager();
			}
			
			return _charactersManager;
		}
	}
	
	public GroupGeneratorManager groupGeneratorManager
	{
		get
		{
			if(_groupGeneratorManager == null)
			{
				_groupGeneratorManager = new GroupGeneratorManager();
			}
			
			return _groupGeneratorManager;
		}
	}

	CharacterGroup _mainGroup = null;

	public CharacterGroup mainGroup
	{
		get
		{
			if(_mainGroup == null)
			{
				_mainGroup = new CharacterGroup();
				_mainGroup.characters.Add(new CharacterGroup.CharacterLocation(charactersManager.playableCharacterFactory["mainCharacter"].generatePlayableCharacter(),new Vector2()));
				_mainGroup.characters.Add(new CharacterGroup.CharacterLocation(charactersManager.playableCharacterFactory["mainCharacter"].generatePlayableCharacter(),new Vector2()));
				_mainGroup.characters.Add(new CharacterGroup.CharacterLocation(charactersManager.playableCharacterFactory["mainCharacter"].generatePlayableCharacter(),new Vector2()));
				//_mainGroup.characters.Add(new CharacterGroup.CharacterLocation(charactersManager.playableCharacterFactory["mainCharacter"].generatePlayableCharacter(),new Vector2()));
			}

			return _mainGroup;
		}
	}
}
