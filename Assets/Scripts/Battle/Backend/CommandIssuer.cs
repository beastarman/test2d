﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CommandIssuer : CommandIssuerInterface
{
	Battle battle;

	public CharacterGroup characterGroup;
	public CharacterGroupMask enemyGroup;

	public Dictionary<PlayableCharacter,Command> commands;

	private readonly object syncLock = new object();

	public CommandIssuer(Battle b, CharacterGroup cg, CharacterGroupMask cgm)
	{
		battle = b;
		characterGroup = cg;
		enemyGroup = cgm;

		reset();
	}
	
	public void addCommand(Command c)
	{
		lock(syncLock)
		{
			if(commands == null) return;

			if(c!=null && c.source.isAlive() && characterGroup.contains(c.source))
			{
				if(commands.ContainsKey(c.source))
				{
					commands.Remove(c.source);
				}
				
				commands.Add(c.source,c);
			}
		}
	}

	public void removeCommand(PlayableCharacter pc)
	{
		lock(syncLock)
		{
			if(pc == null) return;
			
			commands.Remove(pc);
		}
	}
	
	public bool addedCommand(PlayableCharacter pc)
	{
		lock(syncLock)
		{
			if(commands == null) return true;

			return commands.ContainsKey(pc);
		}
	}

	public bool isLocked()
	{
		return commands == null;
	}

	public bool issue()
	{
		List<Command> commandList = new List<Command>();

		foreach(Command c in commands.Values)
		{
			commandList.Add(c);
		}

		bool ret = battle.issue(characterGroup,commandList);

		return ret;
	}

	public bool issuedCommand()
	{
		return battle.issuedCommand(characterGroup);
	}

	public void reset()
	{
		commands = new Dictionary<PlayableCharacter,Command>();
	}
}
