﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

[XmlRoot("CharactersManager")]
public class CharactersManager
{
	SerializableDictionary<string,PlayableCharacter> playableCharacters = new SerializableDictionary<string,PlayableCharacter>();

	public SerializableDictionary<string,CharacterFactory> playableCharacterFactory = new SerializableDictionary<string,CharacterFactory>();
	
	public CharactersManager()
	{
	}
	
	public CharactersManager(CharactersManager manager)
	{
		foreach(KeyValuePair<string,CharacterFactory> entry in manager.playableCharacterFactory)
		{
			playableCharacterFactory.Add(entry.Key, entry.Value.Clone());
		}
	}
	
	public PlayableCharacter getPlayableCharacter(string id)
	{
		if(!playableCharacters.ContainsKey(id))
		{
			if(playableCharacterFactory.ContainsKey(id))
			{
				playableCharacters[id] = playableCharacterFactory[id].generatePlayableCharacter();
			}
			else
			{
				return null;
			}
		}
		
		return playableCharacters[id];
	}
}
