﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR

using UnityEditor;

#endif

public class Marker : MonoBehaviour
{
	public Color color = Color.red;
	public Color colorSelected = Color.cyan;
	
	void OnDrawGizmos()
	{
		Gizmos.color = Color.black;
		Gizmos.DrawLine(transform.position,transform.position + new Vector3(0,1,0));
		Gizmos.color = color;
		Gizmos.DrawSphere(transform.position + new Vector3(0,1.5f,0),0.5f);
	}
	
	void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.black;
		Gizmos.DrawLine(transform.position,transform.position + new Vector3(0,1,0));
		Gizmos.color = colorSelected;
		Gizmos.DrawSphere(transform.position + new Vector3(0,1.5f,0),0.5f);
		
		#if UNITY_EDITOR
		
		GUIStyle style = new GUIStyle();
        style.normal.textColor = colorSelected;
		Handles.BeginGUI();
        Vector3 pos = transform.position + new Vector3(1f,1.5f,0);
        Vector2 pos2D = HandleUtility.WorldToGUIPoint(pos);
        GUI.Label(new Rect(pos2D.x, pos2D.y, 100, 100), transform.name, style);
        Handles.EndGUI();
		
		#endif
	}
}
