﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Reflection;
using System.Collections.Generic;

#if UNITY_EDITOR

using UnityEditor;

#endif

public class ActionTypeBehaviorStartBattle : ActionTypeBehavior
{
	public ActionTypeBehaviorStartBattle(InspectorEventComponent.InspectorEvent.InspectorEventAction iec) : base(iec)
	{
		this.propertyHeight = 16;
	}
	
	public override void resize()
	{
		Array.Resize(ref getInspectorEventAction().extraString,1);
		Array.Resize(ref getInspectorEventAction().extraGameObjects,0);
	}
	
	public string getGroupGeneratorID()
	{
		string s = safeGetString(0);

		if(!GlobalGameController.Instance.groupGeneratorManager.generators.ContainsKey(s))
		{
			s = "__UNDEFINED";
		}

		return s;
	}
	
	public string setGroupGeneratorID(string gen)
	{
		return safeSetString(0,gen);
	}
	
	public override string ToString()
	{
		string gen = getGroupGeneratorID();

		if(gen.Equals("__UNDEFINED"))
		{
			return "(!) Start Battle UNDEFINED";
		}
		else
		{
			return "Start Battle " + gen;
		}
	}
	
	public override bool run(InspectorEventComponent component)
	{
		GlobalGameController.Instance.setGeneratorIntent(getGroupGeneratorID());
		Application.LoadLevelAdditive("BattleScene");
		
		return false;
	}

	#if UNITY_EDITOR

	public string[] getGenerators()
	{
		string[] ret = new string[GlobalGameController.Instance.groupGeneratorManager.generators.Keys.Count+1];
		ret[0] = "__UNDEFINED";
		GlobalGameController.Instance.groupGeneratorManager.generators.Keys.CopyTo(ret,1);
		return ret;
	}

	public override void createExtraFields(ref Rect rect)
	{
		rect.height = 16;

		string curGenID = getGroupGeneratorID();
		string newGenID = StringPopup(rect,"Group",curGenID,getGenerators());
		
		if(!curGenID.Equals(newGenID))
		{
			setDirty();
			setGroupGeneratorID(newGenID);
		}
		
		rect.y += 16;
	}

	public static string StringPopup(Rect rect, string label, string selected, string[] displayedOptions)
	{
		int i = Mathf.Max(0,(new List<string>(displayedOptions)).IndexOf(selected));
		i = EditorGUI.Popup(rect,label,i,displayedOptions);
		return displayedOptions[i];
	}

	#endif
}
