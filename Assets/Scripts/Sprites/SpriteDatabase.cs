﻿using UnityEngine;
using System.Collections;

public class SpriteDatabase
{
	public SerializableDictionary<string,CacheableSprite> sprites = new SerializableDictionary<string, CacheableSprite>();

	public SpriteDatabase(){}

	public SpriteDatabase(SpriteDatabase that)
	{
		foreach(string s in that.sprites.Keys)
		{
			sprites.Add(s,that.sprites[s]);
		}
	}

	public class SpriteID
	{
		private string _name;
		private string _id;

		public string name
		{
			get
			{
				return _name;
			}
		}

		public string id
		{
			get
			{
				return _id;
			}
		}

		public SpriteID(string _name,string _id)
		{
			this._name = _name;
			this._id = _id;
		}

		public override string ToString()
		{
			return name;
		}
	}
}
