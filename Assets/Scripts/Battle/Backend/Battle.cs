﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Battle
{
	protected CharacterGroup sideA;
	protected CharacterGroup sideB;
	
	protected List<Command> sideACommands;
	protected List<Command> sideBCommands;
	
	private readonly object syncLock = new object();

	public enum BattleState
	{
		ISSUE,
		FIGHT,
		FINISHED,
	};
	
	BattleState _state;
	int _turn;

	public int turn
	{
		get
		{
			return _turn;
		}
	}
	
	public BattleState state
	{
		get
		{
			return _state;
		}
	}

	public Battle(CharacterGroup sideA, CharacterGroup sideB)
	{
		this.sideA = sideA;
		this.sideB = sideB;

		_state = BattleState.ISSUE;
		_turn = 0;
	}

	CommandIssuer commandIssuerA = null;
	CommandIssuer commandIssuerB = null;

	public CommandIssuer getCommandIssuer(CharacterGroup cg)
	{
		if(cg == sideA)
		{
			if(commandIssuerA == null)
			{
				commandIssuerA = new CommandIssuer(this,cg,new CharacterGroupMask(sideB));
			}

			return commandIssuerA;
		}
		else if(cg == sideB)
		{
			if(commandIssuerB == null)
			{
				commandIssuerB = new CommandIssuer(this,cg,new CharacterGroupMask(sideA));
			}
			
			return commandIssuerB;
		}
		else
		{
			return null;
		}
	}

	public bool issue(CharacterGroup cg, List<Command> commands)
	{
		lock(syncLock)
		{
			if(state != BattleState.ISSUE)
			{
				return false;
			}

			bool ret = false;

			if(cg == sideA)
			{
				sideACommands = commands;

				ret = sideBCommands != null;
			}
			else if(cg == sideB)
			{
				sideBCommands = commands;
				
				ret = sideACommands != null;
			}

			if(ret)
			{
				_state = BattleState.FIGHT;
			}

			return ret;
		}
	}

	public bool issuedCommand(CharacterGroup cg)
	{
		lock(syncLock)
		{
			if(cg == sideA)
			{
				return sideACommands != null;
			}
			else if(cg == sideB)
			{
				return sideBCommands != null;
			}

			return false;
		}
	}

	List<Command> _finalCommands = null;

	private List<Command> getFinalCommands()
	{
		if(_finalCommands == null)
		{
			_finalCommands = new List<Command>();
			
			_finalCommands.AddRange(sideACommands);
			_finalCommands.AddRange(sideBCommands);
			
			_finalCommands.Sort(delegate(Command x, Command y)
			{
				int ret = x.getPriority().CompareTo(y.getPriority());
				
				if(ret == 0)
				{
					ret = x.source.state.speed.CompareTo(y.source.state.speed);
				}
				
				return -ret;
			});
		}

		return _finalCommands;
	}

	public BattleAction advanceFight()
	{
		if(state != BattleState.FIGHT)
		{
			return null;
		}

		List<Command> commands = getFinalCommands();
		BattleAction ret;
		
		if(commands.Count == 0)
		{
			newTurn();
			return null;
		}

		if(!commands[0].source.isAlive())
		{
			ret = new BattleActionDead(commands[0].source);
		}
		else
		{
			ret = commands[0].doIt();
		}
		
		commands.RemoveAt(0);

		if(commands.Count == 0)
		{
			newTurn();
		}

		return ret;
	}

	private void newTurn()
	{
		_finalCommands = null;
		sideACommands = null;
		sideBCommands = null;
		commandIssuerA.reset();
		commandIssuerB.reset();
		
		bool sideAAlive = false;
		bool sideBAlive = false;
		
		foreach (CharacterGroup.CharacterLocation pc in sideA.characters)
		{
			sideAAlive |= pc.pc.isAlive();
			pc.pc.newTurn();
		}
		foreach (CharacterGroup.CharacterLocation pc in sideB.characters)
		{
			sideBAlive |= pc.pc.isAlive();
			pc.pc.newTurn();
		}

		if(sideAAlive && sideBAlive)
		{
			_state = BattleState.ISSUE;
			_turn++;
		}
		else
		{
			_state = BattleState.FINISHED;
		}
	}

	public bool won(CharacterGroup cg)
	{
		if(state != BattleState.FINISHED)
		{
			return false;
		}

		if(cg != sideA && cg != sideB)
		{
			return false;
		}

		return cg.isAlive();
	}
}
