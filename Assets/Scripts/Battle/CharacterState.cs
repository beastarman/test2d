﻿using UnityEngine;
using System.Collections;

public class CharacterState
{
	public int maxHP;
	public int curHP;
	
	public int attack;
	public int defense;
	public int speed;
	
	public CharacterState(int _maxHP, int _attack, int _defense, int _speed)
	{
		maxHP = _maxHP;
		curHP = _maxHP;
		attack = _attack;
		defense = _defense;
		speed = _speed;
	}
	
	public int takeHit(int enemyAttack)
	{
		int temp = curHP;
		curHP = Mathf.Max(curHP-enemyAttack,0);

		return temp - curHP;
	}
}
