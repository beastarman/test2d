﻿using UnityEngine;
using UnityEditor;
using System.Threading;
using System.Collections;
using System.Collections.Generic;

public class BattleSimulatorWindow : EditorWindow {

	[MenuItem ("Game/Battle Simulator &b")]
	static void Init()
	{
		EditorWindow window = EditorWindow.GetWindow (typeof (BattleSimulatorWindow), true, "Battle Simulator");
		
		window.wantsMouseMove = true;
	}

	CharacterGroup left = new CharacterGroup();
	CharacterGroup right = new CharacterGroup();
	OpenBattle battle = null;

	void OnGUI ()
	{
		EditorGUILayout.BeginVertical();
		{
			GUI.enabled = battle == null;

			EditorGUILayout.BeginHorizontal();
			{
				EditorGUILayout.BeginVertical(GUILayout.Width(position.width / 2));
				{
					EditorGUILayout.LabelField("Side A");

					CharacterGroupGUI(ref left);
				}
				EditorGUILayout.EndVertical();

				EditorGUILayout.BeginVertical(GUILayout.Width(position.width / 2));
				{
					EditorGUILayout.LabelField("Side B");

					CharacterGroupGUI(ref right);
				}
				EditorGUILayout.EndVertical();
			}
			EditorGUILayout.EndHorizontal();

			GUI.enabled = true;

			if(battle == null)
			{
				GUI.enabled = left.characters.Count>0 && right.characters.Count>0;
				{
					if(GUILayout.Button("Start Battle"))
					{
						battle = new OpenBattle(left,right);
						log = "";
						resetCommandIssuerGUI();
					}
				}
				GUI.enabled = true;
			}
			else
			{
				if(GUILayout.Button("End Battle"))
				{
					battle = null;
				}
				else
				{
					BattleGUI();
				}
			}
		}
		EditorGUILayout.EndVertical();
	}

	Dictionary<CharacterGroup,int> _charSelected = new Dictionary<CharacterGroup,int>();
	
	int getCharSelectedPos(ref CharacterGroup group)
	{
		if(!_charSelected.ContainsKey(group))
		{
			_charSelected.Add(group,-1);
		}
		
		return _charSelected[group];
	}
	
	int setCharSelectedPos(ref CharacterGroup group, int i)
	{
		if(_charSelected.ContainsKey(group))
		{
			_charSelected.Remove(group);
		}

		_charSelected.Add(group,i);
		
		return i;
	}
	
	PlayableCharacter getCharSelected(ref CharacterGroup group)
	{
		int i = getCharSelectedPos(ref group);
		
		if(i < 0) return null;
		
		return group.characters[i].pc;
	}
	
	PlayableCharacter setCharSelected(ref CharacterGroup group, PlayableCharacter pc)
	{
		int i = getCharSelectedPos(ref group);
		
		if(i < 0) return null;

		group.characters[i].pc = pc;

		return pc;
	}

	Dictionary<CharacterGroup,Dictionary<PlayableCharacter,CharacterFactory>> _characterFactories = new Dictionary<CharacterGroup,Dictionary<PlayableCharacter,CharacterFactory>>();

	Dictionary<PlayableCharacter,CharacterFactory> getCharacterFactories(ref CharacterGroup group)
	{
		if(!_characterFactories.ContainsKey(group))
		{
			_characterFactories.Add(group,new Dictionary<PlayableCharacter,CharacterFactory>());
		}
		
		return _characterFactories[group];
	}

	void CharacterGroupGUI(ref CharacterGroup group)
	{
		Dictionary<PlayableCharacter,CharacterFactory> factories = getCharacterFactories(ref group);

		Rect r = EditorGUILayout.BeginVertical();
		{
			Color t = GUI.backgroundColor;
			GUI.backgroundColor = Color.white;
			GUI.Box(r,"");
			GUI.backgroundColor = t;
			
			GUIStyle style = new GUIStyle();
			style.padding = new RectOffset(4,4,2,2);
			GUIStyle selected = new GUIStyle(style);
			selected.normal.background = MakeTex(1000,20,new Color(0.5f,0.5f,1.0f));
			selected.active.background = MakeTex(1000,20,new Color(0.5f,0.5f,1.0f));

			bool enabledGUI = GUI.enabled;
			GUI.enabled = true;

			int charSel = 0;
			foreach(CharacterGroup.CharacterLocation pc in group.characters)
			{
				if(getCharSelectedPos(ref group) == charSel)
				{
					GUILayout.Button(pc.pc.name,selected);
				}
				else
				{
					if(GUILayout.Button(pc.pc.name,style))
					{
						setCharSelectedPos(ref group, charSel);
						GUI.FocusControl("");
					}
				}

				charSel++;
			}

			GUI.enabled = enabledGUI;

			if(GUILayout.Button("Add Character...",style))
			{
				Dictionary<string,CharacterFactory>.Enumerator ie = GlobalGameController.Instance.charactersManager.playableCharacterFactory.GetEnumerator();
				ie.MoveNext();
				CharacterFactory fac = ie.Current.Value;
				PlayableCharacter pc = fac.generatePlayableCharacter();

				group.characters.Add(new CharacterGroup.CharacterLocation(pc,new Vector2(0.5f,0.5f)));

				setCharSelectedPos(ref group, charSel);

				factories.Add(pc,fac);

				GUI.FocusControl("");
			}

			if(GUILayout.Button("",style))
			{
				setCharSelectedPos(ref group, -1);

				GUI.FocusControl("");
			}
		}
		EditorGUILayout.EndVertical();

		PlayableCharacter playableChar = getCharSelected(ref group);

		if(playableChar != null)
		{
			EditorGUILayout.BeginVertical();
			{
				playableChar.name = EditorGUILayout.TextField("Name:",playableChar.name);

				CharacterFactory fac = factories[playableChar];
				CharacterFactory tempFac = GUIExtension.Layout_GenericPopup("Factory:",fac,GlobalGameController.Instance.charactersManager.playableCharacterFactory);

				if(fac != tempFac)
				{
					PlayableCharacter newChar = tempFac.generatePlayableCharacter(playableChar.level);

					factories.Remove(playableChar);
					factories.Add(newChar,tempFac);

					setCharSelected(ref group, newChar);

					playableChar = newChar;
				}

				int nLevel = EditorGUILayout.IntSlider("Level:",playableChar.level,1,100);

				if(playableChar.level != nLevel)
				{
					PlayableCharacter newChar = fac.generatePlayableCharacter(nLevel);
					
					factories.Remove(playableChar);
					factories.Add(newChar,fac);
					
					newChar.name = playableChar.name;
					
					setCharSelected(ref group, newChar);
					
					playableChar = newChar;
				}

				CharacterState state = playableChar.state;

				float div = 0;

				foreach(CharacterGroup.CharacterLocation pc in left.characters)
				{
					CharacterState level100 = pc.pc.state;
					div = Mathf.Max(div,level100.maxHP,level100.attack,level100.defense,level100.speed);
				}
				foreach(CharacterGroup.CharacterLocation pc in right.characters)
				{
					CharacterState level100 = pc.pc.state;
					div = Mathf.Max(div,level100.maxHP,level100.attack,level100.defense,level100.speed);
				}
				
				EditorGUI.ProgressBar(EditorGUILayout.GetControlRect(false,16),state.maxHP/div,"MaxHP (" + state.maxHP + ")");
				EditorGUI.ProgressBar(EditorGUILayout.GetControlRect(false,16),state.attack/div,"Attack (" + state.attack + ")");
				EditorGUI.ProgressBar(EditorGUILayout.GetControlRect(false,16),state.defense/div,"Defense (" + state.defense + ")");
				EditorGUI.ProgressBar(EditorGUILayout.GetControlRect(false,16),state.speed/div,"Speed (" + state.speed + ")");
			}
			EditorGUILayout.EndVertical();
		}

		GUI.SetNextControlName("");
	}

	private Texture2D MakeTex(int width, int height, Color col)
	{
		Color[] pix = new Color[width*height];
		
		for(int i = 0; i < pix.Length; i++)
			pix[i] = col;
		
		Texture2D result = new Texture2D(width, height);
		result.SetPixels(pix);
		result.Apply();
		
		return result;
	}

	Vector2 battleLogScrollPosition = new Vector2();
	string log = "";

	void BattleGUI()
	{
		if(battle.state == Battle.BattleState.ISSUE)
		{
			GUILayout.BeginHorizontal();
			{
				GUILayout.BeginVertical(GUILayout.Width(position.width / 2));
				{
					CommandIssuerGUI(battle.getCommandIssuer(battle.left));
				}
				GUILayout.EndVertical();
				
				GUILayout.BeginVertical(GUILayout.Width(position.width / 2));
				{
					CommandIssuerGUI(battle.getCommandIssuer(battle.right));
				}
				GUILayout.EndVertical();
			}
			GUILayout.EndHorizontal();
		}

		battleLogScrollPosition = GUILayout.BeginScrollView(battleLogScrollPosition, GUILayout.Height(300));
		{
			GUI.skin.box.wordWrap = true;
			GUI.skin.box.alignment = TextAnchor.UpperLeft;
			GUILayout.Box(log, GUILayout.ExpandWidth(true));
		}
		GUILayout.EndScrollView();

		if(battle.state == Battle.BattleState.FIGHT)
		{
			BattleAction action = battle.advanceFight();

			if(action != null) log += action.ToString() + "\n";

			if(battle.state == Battle.BattleState.ISSUE)
			{
				resetCommandIssuerGUI();
			}
			else if(battle.state == Battle.BattleState.FINISHED)
			{
				log += "\nBattle finished\n";
				resetCommandIssuerGUI();
			}
		}
	}
	
	Dictionary<CommandIssuer,PlayableCharacter> selectedCharacter = new Dictionary<CommandIssuer,PlayableCharacter>();
	
	PlayableCharacter getSelectedCharacterIssuer(CommandIssuer issuer)
	{
		if(!selectedCharacter.ContainsKey(issuer))
		{
			selectedCharacter.Add(issuer,issuer.characterGroup.characters[0].pc);
		}
		
		return selectedCharacter[issuer];
	}
	
	void setSelectedCharacterIssuer(CommandIssuer issuer, PlayableCharacter pc)
	{
		if(selectedCharacter.ContainsKey(issuer))
		{
			selectedCharacter.Remove(issuer);
		}
		
		selectedCharacter.Add(issuer,pc);
	}

	Dictionary<PlayableCharacter,CommandBuilder> characterCommands = new Dictionary<PlayableCharacter,CommandBuilder>();

	CommandBuilder getCommandBuilder(PlayableCharacter pc)
	{
		if(!characterCommands.ContainsKey(pc))
		{
			characterCommands.Add(pc,pc.getAvailableCommands()[0]);
		}
		
		return characterCommands[pc];
	}
	
	void setCommandBuilder(PlayableCharacter pc, CommandBuilder builder)
	{
		if(characterCommands.ContainsKey(pc))
		{
			characterCommands.Remove(pc);
		}
		
		characterCommands.Add(pc,builder);
	}

	Dictionary<CommandBuilder,TargetBuilder> targetBuilders = new Dictionary<CommandBuilder, TargetBuilder>();

	TargetBuilder getTargetBuilder(CommandIssuer issuer, CommandBuilder commandBuilder)
	{
		if(!targetBuilders.ContainsKey(commandBuilder))
		{
			TargetBuilder targetBuilder = commandBuilder.getTargetBuilder();

			if(targetBuilder is TargetBuilderSingleEnemy)
			{
				TargetBuilderSingleEnemy t = ((TargetBuilderSingleEnemy)targetBuilder);

				t.target = issuer.enemyGroup.characters[0].pc;
			}

			targetBuilders.Add(commandBuilder,targetBuilder);
		}

		return targetBuilders[commandBuilder];
	}

	void resetCommandIssuerGUI()
	{
		selectedCharacter = new Dictionary<CommandIssuer,PlayableCharacter>();
		characterCommands = new Dictionary<PlayableCharacter,CommandBuilder>();
		targetBuilders = new Dictionary<CommandBuilder, TargetBuilder>();
		aiThreads = new Dictionary<CommandIssuer, Thread>();

		if(battle.turn != 0) log += "\n";

		if(battle.state != Battle.BattleState.FINISHED)
		{
			log += "Turn " + (battle.turn + 1) + "\n";
		}
	}

	Dictionary<CharacterMask,string> getEnemies(CommandIssuer issuer)
	{
		Dictionary<CharacterMask,string> enemies = new Dictionary<CharacterMask,string>();

		foreach(CharacterGroupMask.CharacterMaskLocation c in issuer.enemyGroup.characters)
		{
			string name = c.pc.name;
			int i = 0;
			
			while(enemies.ContainsValue(name))
			{
				name = c.pc.name + " (" + (++i) + ")";
			}
			
			enemies.Add(c.pc,name);
		}

		return enemies;
	}

	Dictionary<CommandIssuer,AI> aiList = new Dictionary<CommandIssuer, AI>();
	Dictionary<CommandIssuer,AI> invalidAI = new Dictionary<CommandIssuer,AI>();

	AI getInvalid(CommandIssuer issuer)
	{
		if(!invalidAI.ContainsKey(issuer))
		{
			invalidAI.Add(issuer,new AI(null,issuer));
		}

		return invalidAI[issuer];
	}
	
	AI getAI(CommandIssuer issuer)
	{
		if(!aiList.ContainsKey(issuer))
		{
			aiList.Add(issuer,getInvalid(issuer));
		}
		
		return aiList[issuer];
	}
	
	void setAI(CommandIssuer issuer, AI ai)
	{
		if(aiList.ContainsKey(issuer))
		{
			aiList.Remove(issuer);
		}
		
		aiList.Add(issuer, ai);
	}

	Dictionary<CommandIssuer,Dictionary<AI,string>> aiStringList = new Dictionary<CommandIssuer, Dictionary<AI,string>>();

	Dictionary<AI, string> getAIStrings(CommandIssuer issuer)
	{
		if(!aiStringList.ContainsKey(issuer))
		{
			Dictionary<AI, string> dict = new Dictionary<AI, string>();
			
			dict.Add(getInvalid(issuer),"No AI");
			dict.Add(new AI(new AIBaseRandom(),issuer),"Random AI");
			
			aiStringList.Add(issuer, dict);
		}
		
		return aiStringList[issuer];
	}

	Dictionary<CommandIssuer,Thread> aiThreads = new Dictionary<CommandIssuer, Thread>();

	void CommandIssuerGUI(CommandIssuer issuer)
	{
		AI ai = GUIExtension.Layout_GenericPopup("AI",getAI(issuer),getAIStrings(issuer));
		setAI(issuer,ai);

		Dictionary<PlayableCharacter,string> chars = new Dictionary<PlayableCharacter,string>();
		foreach(CharacterGroup.CharacterLocation c in issuer.characterGroup.characters)
		{
			string name = c.pc.name;
			int i = 0;

			while(chars.ContainsValue(name))
			{
				name = c.pc.name + " (" + (++i) + ")";
			}

			chars.Add(c.pc,name);
		}
		PlayableCharacter pc = GUIExtension.Layout_GenericPopup("Character",getSelectedCharacterIssuer(issuer),chars);
		setSelectedCharacterIssuer(issuer,pc);

		EditorGUI.ProgressBar(EditorGUILayout.GetControlRect(false,16),((float)pc.state.curHP)/pc.state.maxHP,"HP (" + pc.state.curHP + "/" + pc.state.maxHP + ")");

		if(ai == getInvalid(issuer))
		{
			if(issuer.issuedCommand())
			{
				GUI.enabled = false;
			}

			Dictionary<CommandBuilder,string> commands = new Dictionary<CommandBuilder,string>();
			foreach (CommandBuilder c in pc.getAvailableCommands())
			{
				string name = c.getName();
				int i = 0;
				
				while(chars.ContainsValue(name))
				{
					name = c.getName() + " (" + (++i) + ")";
				}
				
				commands.Add(c,name);
			}
			CommandBuilder command = GUIExtension.Layout_GenericPopup("Command",getCommandBuilder(pc),commands);
			setCommandBuilder(pc,command);

			TargetBuilder tb = getTargetBuilder(issuer,command);
			if(tb is TargetBuilderSingleEnemy)
			{
				TargetBuilderSingleEnemy t = (TargetBuilderSingleEnemy)tb;
				t.target = GUIExtension.Layout_GenericPopup("Target",t.target,getEnemies(issuer));
			}

			if(GUILayout.Button("Issue commands"))
			{
				foreach (PlayableCharacter pChar in issuer.characterGroup.getAliveCharacters())
				{
					CommandBuilder cBuilder = getCommandBuilder(pChar);
					Target t = getTargetBuilder(issuer,cBuilder).build();
					if(t != null)
					{
						Command c = cBuilder.build(t);
						if(c != null)
						{
							issuer.addCommand(c);
						}
					}
				}
				
				issuer.issue();
			}
			
			GUI.enabled = true;
		}
		else
		{
			GUI.enabled = !aiThreads.ContainsKey(issuer);

			if(GUILayout.Button("Run AI"))
			{
				Thread t = new Thread(new ThreadStart(ai.assignCommands));
				t.Start();
				aiThreads.Add(issuer,t);
			}
			
			GUI.enabled = true;
		}
	}

	private class OpenBattle : Battle
	{
		public OpenBattle(CharacterGroup left, CharacterGroup right) : base(left,right)
		{
			foreach (CharacterGroup.CharacterLocation pc in left.characters)
			{
				pc.pc.state.curHP = pc.pc.state.maxHP;
			}
			foreach (CharacterGroup.CharacterLocation pc in right.characters)
			{
				pc.pc.state.curHP = pc.pc.state.maxHP;
			}
		}
		
		public CharacterGroup left
		{
			get
			{
				return sideA;
			}
		}
		
		public CharacterGroup right
		{
			get
			{
				return sideB;
			}
		}
	}
}

public class AI
{
	AIBase aiBase;
	CommandIssuer issuer;

	public AI(AIBase _aiBase, CommandIssuer _issuer)
	{
		aiBase = _aiBase;
		issuer = _issuer;
	}

	public void assignCommands()
	{
		foreach (PlayableCharacter pc in issuer.characterGroup.getAliveCharacters())
		{
			issuer.addCommand(aiBase.getCommand(pc,issuer));
		}

		issuer.issue();
	}

	public bool isValid()
	{
		return aiBase != null;
	}
}