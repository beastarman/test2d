﻿using UnityEngine;
using System.Collections;

public class BattleTransform : MonoBehaviour
{
	public BattleSceneManager battleSceneManager;

	private Vector2 _position;

	public Vector2 position
	{
		get
		{
			return _position;
		}

		set
		{
			_position = value;
			transform.position = battleSceneManager.gridToScreen(value);
			float f = battleSceneManager.gridToScale(value);
			transform.localScale = new Vector3(f,f,f);
		}
	}
}
