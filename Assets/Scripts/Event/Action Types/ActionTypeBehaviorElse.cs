﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Reflection;
using System.Collections.Generic;

public class ActionTypeBehaviorElse : ActionTypeBehavior
{
	public ActionTypeBehaviorElse(InspectorEventComponent.InspectorEvent.InspectorEventAction iec) : base(iec)
	{
		this.isOpenBlock = true;
		this.isCloseBlock = true;
	}
	
	public override string ToString()
	{
		return "} Else {";
	}
	
	public override bool preRun(InspectorEventComponent component)
	{
		int last = component.ifResults.Pop();

		component.ifResults.Push(last+1);
		
		return true;
	}
}
