﻿using UnityEngine;
using System.Collections;
using System.Xml.Serialization;

#if UNITY_EDITOR

using UnityEditor;

#endif

public class CharacterFactoryLinear : CharacterFactory
{
	public int attackFactor = 1;
	public int defenseFactor = 1;
	public int maxHPFactor = 1;
	public int speedFactor = 1;
	
	public CharacterFactoryLinear() : base()
	{
	}

	public CharacterFactoryLinear(string name) : base(name)
	{
	}
	
	public CharacterFactoryLinear(CharacterFactory that) : base(that)
	{
		if (that is CharacterFactoryLinear)
		{
			attackFactor = ((CharacterFactoryLinear)that).attackFactor;
			defenseFactor = ((CharacterFactoryLinear)that).defenseFactor;
			maxHPFactor = ((CharacterFactoryLinear)that).maxHPFactor;
			speedFactor = ((CharacterFactoryLinear)that).speedFactor;
		}
	}

	public override PlayableCharacter generatePlayableCharacter(int level)
	{
		PlayableCharacterLinear pcl = new PlayableCharacterLinear(sprite,spriteDatabase,level,attackFactor,defenseFactor,maxHPFactor,speedFactor);
		pcl.name = name;
		return pcl;
	}
	
	public override CharacterFactory Clone()
	{
		return new CharacterFactoryLinear(this);
	}
	
	#if UNITY_EDITOR
	
	public override bool OnGUI()
	{
		bool dirty = base.OnGUI();

		int temp;
		
		temp = EditorGUILayout.IntSlider("Attack", attackFactor, 1, 100);
		dirty |= temp != attackFactor;
		attackFactor = temp;
		
		temp = EditorGUILayout.IntSlider("Defense", defenseFactor, 1, 100);
		dirty |= temp != defenseFactor;
		defenseFactor = temp;
		
		temp = EditorGUILayout.IntSlider("Max HP", maxHPFactor, 1, 100);
		dirty |= temp != maxHPFactor;
		maxHPFactor = temp;
		
		temp = EditorGUILayout.IntSlider("Speed", speedFactor, 1, 100);
		dirty |= temp != speedFactor;
		speedFactor = temp;

		return dirty;
	}
	
	#endif
}
