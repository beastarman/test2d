﻿using System.Collections.Generic;
using System;

public static class DictionaryExtensions
{
	public static TKey FindKeyByValue<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TValue value)
	{
		if (dictionary == null)
			throw new ArgumentNullException("dictionary");
		
		foreach (KeyValuePair<TKey, TValue> pair in dictionary)
			if (value.Equals(pair.Value)) return pair.Key;
		
		throw new Exception("the value is not found in the dictionary");
	}
}