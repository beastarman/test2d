﻿
using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Reflection;
using System.Collections.Generic;

public class ActionTypeBehaviorSaveGame : ActionTypeBehavior
{
	public ActionTypeBehaviorSaveGame(InspectorEventComponent.InspectorEvent.InspectorEventAction iec) : base(iec)
	{
	}
	
	public override void resize()
	{
	}
	
	public override string ToString()
	{
		return "Save Game";
	}
	
	public override bool run(InspectorEventComponent component)
	{
		GlobalGameController.Instance.saveGame();
		
		return true;
	}
}
