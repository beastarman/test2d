﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Reflection;
using System.Collections.Generic;

#if UNITY_EDITOR

using UnityEditor;

#endif

public class ActionTypeBehaviorFaceCharacter : ActionTypeBehavior
{
	public ActionTypeBehaviorFaceCharacter(InspectorEventComponent.InspectorEvent.InspectorEventAction iec) : base(iec)
	{
		this.propertyHeight = 16*2;
	}
	
	public override void resize()
	{
		Array.Resize(ref getInspectorEventAction().extraString,0);
		Array.Resize(ref getInspectorEventAction().extraGameObjects,2);
	}
	
	public GameObject getTarget()
	{
		return safeGetGameObject(0);
	}
	
	public GameObject setTarget(GameObject obj)
	{
		return safeSetGameObject(0,obj);
	}
	
	public GameObject getWho()
	{
		return safeGetGameObject(1);
	}
	
	public GameObject setWho(GameObject obj)
	{
		return safeSetGameObject(1,obj);
	}
	
	public override string ToString()
	{
		GameObject w = getWho();
		GameObject c = getTarget();
		
		if(c != null)
		{
			string wName = "self";
			
			if(w != null)
			{
				wName = w.name;
			}
			
			return "Face " + wName + " to " + c.name;
		}
		else
		{
			return "(!) Face No one";
		}
	}
	
	public override bool run(InspectorEventComponent component)
	{
		GameObject gameObject = getWho();
		
		if(gameObject == null) gameObject = component.gameObject;
		
		Character character = gameObject.GetComponent<Character>();
		
		Vector3 targetPosition;
		
		if(getTarget().GetComponent<Collider2D>() != null)
		{
			targetPosition = getTarget().GetComponent<Collider2D>().bounds.center;
		}
		else
		{
			targetPosition = getTarget().transform.position;
		}
		
		character.directionFacing = targetPosition - gameObject.GetComponent<Collider2D>().bounds.center;
		
		return false;
	}
	
	#if UNITY_EDITOR
	
	public override void createExtraFields(ref Rect rect)
	{
		rect.height = 16;
	
		GameObject a = getWho();
		
		string label = "Who";
		
		GameObject b = (GameObject) EditorGUI.ObjectField(new Rect(rect),label, a, typeof(GameObject), true);
		setWho(b);
		
		if(a!=b && ((a!=null && !a.Equals(b)) || (b!=null && !b.Equals(a))))
		{
			setDirty();
		}
		
		rect.y += 16;
		
		a = getTarget();
		
		label = "Target";
		
		b = (GameObject) EditorGUI.ObjectField(new Rect(rect),label, a, typeof(GameObject), true);
		setTarget(b);
		
		if(a!=b && ((a!=null && !a.Equals(b)) || (b!=null && !b.Equals(a))))
		{
			setDirty();
		}
		
		rect.y += 16;
	}
	
	#endif
}
