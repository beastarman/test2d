﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Reflection;
using System.Collections.Generic;

#if UNITY_EDITOR

using UnityEditor;

#endif

public class ActionTypeBehaviorPlaySound : ActionTypeBehavior
{
	public ActionTypeBehaviorPlaySound(InspectorEventComponent.InspectorEvent.InspectorEventAction iec) : base(iec)
	{
		this.propertyHeight = 16*1;
	}
	
	public override void resize()
	{
		Array.Resize(ref getInspectorEventAction().extraString,0);
		Array.Resize(ref getInspectorEventAction().extraGameObjects,1);
	}
	
	public AudioClip getSound()
	{
		return (AudioClip) safeGetObject(0);
	}
	
	public AudioClip setSound(AudioClip obj)
	{
		return (AudioClip) safeSetObject(0,obj);
	}
	
	public override string ToString()
	{
		AudioClip sound = getSound();
		
		if(sound != null)
		{
			
			return "Make sound " + sound.name;
		}
		else
		{
			return "(!) Make no sound";
		}
	}
	
	public override bool run(InspectorEventComponent component)
	{
		AudioClip sound = getSound();
		
		GameObject.FindGameObjectWithTag("SceneManager").GetComponent<AudioSource>().PlayOneShot(sound);
		
		component.StartEventCoroutine(wait(sound.length));
		
		return false;
	}
	
	public IEnumerator wait(float seconds)
	{
		yield return new WaitForSeconds(seconds);
	}
	
	#if UNITY_EDITOR
	
	public override void createExtraFields(ref Rect rect)
	{
		rect.height = 16;
	
		AudioClip a = getSound();
		
		string label = "Sound";
		
		AudioClip b = (AudioClip) EditorGUI.ObjectField(new Rect(rect),label, a, typeof(AudioClip), true);
		setSound(b);
		
		if(a!=b && ((a!=null && !a.Equals(b)) || (b!=null && !b.Equals(a))))
		{
			setDirty();
		}
		
		rect.y += 16;
	}
	
	#endif
}
