﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Reflection;
using System.Collections.Generic;

#if UNITY_EDITOR

using UnityEditor;

#endif

public class ActionTypeBehaviorShowDialogMessage : ActionTypeBehavior
{
	public ActionTypeBehaviorShowDialogMessage(InspectorEventComponent.InspectorEvent.InspectorEventAction iec) : base(iec)
	{
		this.propertyHeight = 16*4;
	}
	
	public override void resize()
	{
		Array.Resize(ref getInspectorEventAction().extraString,2);
		Array.Resize(ref getInspectorEventAction().extraGameObjects,0);
	}
	
	public string getMessage()
	{
		return safeGetString(0);
	}
	
	public string setMessage(string msg)
	{
		return safeSetString(0,msg);
	}
	
	public string getName()
	{
		return safeGetString(1);
	}
	
	public string setName(string msg)
	{
		return safeSetString(1,msg);
	}
	
	public override string ToString()
	{
		string s = getMessage();
		
		if(s.Length > 0)
		{
			string name = getName();
			
			if(name.Length > 0)
			{
				return "Dialog (" + name + "): " + s.Replace("\n"," ");
			}
			else
			{
				return "Dialog: " + s;
			}
		}
		else
		{
			return "(!) Empty Dialog";
		}
	}
	
	public override bool run(InspectorEventComponent component)
	{
		
		string name = getName();
		
		if(name.Length > 0)
		{
			GameObject.FindGameObjectWithTag("SceneManager").GetComponent<DialogController>().updateMessage(getMessage(),name);
		}
		else
		{
			GameObject.FindGameObjectWithTag("SceneManager").GetComponent<DialogController>().updateMessage(getMessage());
		}
		
		return true;
	}
	
	public override bool update(InspectorEventComponent component)
	{
		return component.next.isOnStateMax(State.DOWN,0);
	}
	
	#if UNITY_EDITOR
	
	public override void createExtraFields(ref Rect rect)
	{
		rect.height = 16;
		
		string oldName = getName();
		string newName = EditorGUI.TextField(new Rect(rect),"Name",oldName);
		setName(newName);
		
		if(!oldName.Equals(newName))
		{
			setDirty();
		}
		
		rect.y += 16;
		rect.height = 16*3;
		
		Rect r = EditorGUI.PrefixLabel(new Rect(rect),new GUIContent("Message"));
		
		int i = EditorGUI.indentLevel;
		EditorGUI.indentLevel = 0;
		
		string a = getMessage();
		string b = EditorGUI.TextArea(r,a);
		setMessage(b);
		
		if(!a.Equals(b))
		{
			setDirty();
		}
		
		EditorGUI.indentLevel = i;
		
		rect.y += 16*3;
	}
	
	#endif
}
