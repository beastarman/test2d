﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DialogController : MonoBehaviour
{
	private bool isOn = false;
	private string message = "";
	private string charName = "";

	GameObject dialogBox;
	GameObject nameBox;
	
	public Material dialogBoxMaterial;
	public GUIStyle dialogStyle;
	
	public Material nameBoxMaterial;
	public GUIStyle nameStyle;

	public void startDialog()
	{
		dialogBox = new GameObject();
		dialogBox.name = "DialogBox";

		Vector3 position = Camera.main.ScreenToWorldPoint(new Vector3(0,0,0));
		position.z = Camera.main.transform.position.z + 1;
		dialogBox.transform.position = position;

		MeshFilter meshFilter = (MeshFilter) dialogBox.AddComponent<MeshFilter>();
		MeshRenderer meshRenderer = (MeshRenderer) dialogBox.AddComponent<MeshRenderer>();

		meshFilter.mesh = createDialogBoxMesh();
		meshRenderer.material = dialogBoxMaterial;
		
		nameBox = new GameObject();
		nameBox.name = "NameBox";

		position = Camera.main.ScreenToWorldPoint(new Vector3(0,0,0));
		position.z = Camera.main.transform.position.z + 0.5f;
		nameBox.transform.position = position;

		MeshFilter nameMeshFilter = (MeshFilter) nameBox.AddComponent<MeshFilter>();
		MeshRenderer nameMeshRenderer = (MeshRenderer) nameBox.AddComponent<MeshRenderer>();

		nameMeshFilter.mesh = createNameBoxMesh();
		nameMeshRenderer.material = nameBoxMaterial;

		isOn = true;
	}

	public void endDialog()
	{
		isOn = false;
		message = "";
		charName = "";

		Destroy(dialogBox);
		dialogBox = null;

		Destroy(nameBox);
		nameBox = null;
	}

	public void updateMessage(string message)
	{
		this.message = message;
	}

	public void updateMessage(string message, string charName)
	{
		this.message = message;
		
		if(charName == null) charName = "";
		
		this.charName = charName;
	}

	private Vector3[] getDialogBoxArea()
	{
		Vector3[] vertices = new Vector3[4];
		vertices[0] = new Vector3(20,10);
		vertices[1] = new Vector3(40,160);
		vertices[2] = new Vector3(Screen.width-20,140);
		vertices[3] = new Vector3(Screen.width-40,30);
		
		return vertices;
	}

	private Vector3[] getNameBoxArea()
	{
		Vector3[] vertices = new Vector3[4];
		vertices[0] = new Vector3(60,150);
		vertices[1] = new Vector3(80,220);
		vertices[2] = new Vector3(400,210);
		vertices[3] = new Vector3(460,140);
		
		return vertices;
	}

	private Rect getDialogBoxTextArea()
	{
		float[] rect = new float[4];

		rect[0] = 50;
		rect[1] = Screen.height - 135;
		rect[2] = Screen.width - 100;
		rect[3] = 130;

		return new Rect(rect[0],rect[1],rect[2],rect[3]);
	}
	
	private Rect getNameBoxTextArea()
	{
		float[] rect = new float[4];

		rect[0] = 90;
		rect[1] = Screen.height - 200;
		rect[2] = 310;
		rect[3] = 40;

		return new Rect(rect[0],rect[1],rect[2],rect[3]);
	}

	private Mesh createDialogBoxMesh()
	{
		Vector3[] vertices = getDialogBoxArea();
	
		int i=0;

		Vector3 baseVector = Camera.main.ViewportToWorldPoint(new Vector3());

		for(i=0; i<vertices.Length; i++)
		{
			vertices[i].x = vertices[i].x/Screen.width;
			vertices[i].y = vertices[i].y/Screen.height;

			vertices[i] = Camera.main.ViewportToWorldPoint(vertices[i]);

			vertices[i].x = vertices[i].x - baseVector.x;
			vertices[i].y = vertices[i].y - baseVector.y;

			vertices[i].z = 0;
		}

		Vector2[] uvs = new Vector2[vertices.Length];
		
		while (i < uvs.Length)
		{
			uvs[i] = new Vector2(vertices[i].x, vertices[i].z);
			i++;
		}

		Mesh mesh = new Mesh();
		mesh.vertices = vertices;
		mesh.triangles = new int[] {0,1,2, 0,2,3};
		mesh.normals = new Vector3[] {new Vector3(0,0,-1),new Vector3(0,0,-1),new Vector3(0,0,-1),new Vector3(0,0,-1)};
		mesh.uv = uvs;

		return mesh;
	}
	
	private Mesh createNameBoxMesh()
	{
		Vector3[] vertices = getNameBoxArea();
	
		int i=0;

		Vector3 baseVector = Camera.main.ViewportToWorldPoint(new Vector3());

		for(i=0; i<vertices.Length; i++)
		{
			vertices[i].x = vertices[i].x/Screen.width;
			vertices[i].y = vertices[i].y/Screen.height;

			vertices[i] = Camera.main.ViewportToWorldPoint(vertices[i]);

			vertices[i].x = vertices[i].x - baseVector.x;
			vertices[i].y = vertices[i].y - baseVector.y;

			vertices[i].z = 0;
		}

		Vector2[] uvs = new Vector2[vertices.Length];
		
		while (i < uvs.Length)
		{
			uvs[i] = new Vector2(vertices[i].x, vertices[i].z);
			i++;
		}

		Mesh mesh = new Mesh();
		mesh.vertices = vertices;
		mesh.triangles = new int[] {0,1,2, 0,2,3};
		mesh.normals = new Vector3[] {new Vector3(0,0,-1),new Vector3(0,0,-1),new Vector3(0,0,-1),new Vector3(0,0,-1)};
		mesh.uv = uvs;

		return mesh;
	}

	private bool isOnOption = false;
	private List<string> options = new List<string>();
	
	GameObject optionBox;
	
	public Material optionBoxMaterial;
	public GUIStyle optionStyle;
	public GUIStyle highlightedOptionStyle;

	int highlightedOption = 0;

	public void ShowOptions(List<string> options)
	{
		optionBox = new GameObject();
		optionBox.name = "OptionBox";
		
		Vector3 position = Camera.main.ScreenToWorldPoint(new Vector3(0,0,0));
		position.z = Camera.main.transform.position.z + 2;
		optionBox.transform.position = position;
		
		MeshFilter meshFilter = (MeshFilter) optionBox.AddComponent<MeshFilter>();
		MeshRenderer meshRenderer = (MeshRenderer) optionBox.AddComponent<MeshRenderer>();
		
		meshFilter.mesh = createOptionBoxMesh();
		meshRenderer.material = optionBoxMaterial;
		
		this.isOnOption = true;
		this.options = options;
		highlightedOption = 0;
	}

	public int hideOptions()
	{
		isOnOption = false;
		options = new List<string>(){};
		
		Destroy(optionBox);
		optionBox = null;

		return highlightedOption;
	}

	private Rect getOptionBoxTextArea()
	{
		float[] rect = new float[4];
		
		rect[0] = Screen.width - 600;
		rect[1] = Screen.height - 240;
		rect[2] = 500;
		rect[3] = 30;
		
		return new Rect(rect[0],rect[1],rect[2],rect[3]);
	}

	private Rect getOptionBoxTextArea(int option)
	{
		Rect r = getOptionBoxTextArea();

		for(;option>0;option--)
		{
			r.y+=r.height;
		}

		return r;
	}

	private Vector3[] getOptionBoxArea()
	{
		Vector3[] vertices = new Vector3[4];
		vertices[0] = new Vector3(Screen.width-650,140);
		vertices[1] = new Vector3(Screen.width-610,265);
		vertices[2] = new Vector3(Screen.width-100,245);
		vertices[3] = new Vector3(Screen.width-80,130);
		
		return vertices;
	}

	private Mesh createOptionBoxMesh()
	{
		Vector3[] vertices = getOptionBoxArea();
		
		int i=0;
		
		Vector3 baseVector = Camera.main.ViewportToWorldPoint(new Vector3());
		
		for(i=0; i<vertices.Length; i++)
		{
			vertices[i].x = vertices[i].x/Screen.width;
			vertices[i].y = vertices[i].y/Screen.height;
			
			vertices[i] = Camera.main.ViewportToWorldPoint(vertices[i]);
			
			vertices[i].x = vertices[i].x - baseVector.x;
			vertices[i].y = vertices[i].y - baseVector.y;
			
			vertices[i].z = 0;
		}
		
		Vector2[] uvs = new Vector2[vertices.Length];
		
		while (i < uvs.Length)
		{
			uvs[i] = new Vector2(vertices[i].x, vertices[i].z);
			i++;
		}
		
		Mesh mesh = new Mesh();
		mesh.vertices = vertices;
		mesh.triangles = new int[] {0,1,2, 0,2,3};
		mesh.normals = new Vector3[] {new Vector3(0,0,-1),new Vector3(0,0,-1),new Vector3(0,0,-1),new Vector3(0,0,-1)};
		mesh.uv = uvs;
		
		return mesh;
	}
	
	public void highlightNext()
	{
		highlightOption(highlightedOption+1);
	}
	
	public void highlightPrev()
	{
		highlightOption(highlightedOption-1);
	}

	public void highlightOption(int option)
	{
		highlightedOption = (option+options.Count)%options.Count;
	}

	void OnGUI()
	{
		if(isOn)
		{
			Vector3 position = Camera.main.ScreenToWorldPoint(new Vector3(0,0,0));
			position.z = Camera.main.transform.position.z + 1;
			dialogBox.transform.position = position;
			nameBox.transform.position = position;
			
			GUI.Label(getDialogBoxTextArea(),message,dialogStyle);
			GUI.Label(getNameBoxTextArea(),charName,nameStyle);
		}

		if(isOnOption)
		{
			Vector3 position = Camera.main.ScreenToWorldPoint(new Vector3(0,0,0));
			position.z = Camera.main.transform.position.z + 1;
			optionBox.transform.position = position;

			int i=0;

			for(i=0; i<options.Count; i++)
			{
				if(i == highlightedOption)
				{
					GUI.Label(getOptionBoxTextArea(i),options[i],highlightedOptionStyle);
				}
				else
				{
					GUI.Label(getOptionBoxTextArea(i),options[i],optionStyle);
				}
			}
		}
	}
}
