﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class SingleEnemyPicker : Picker
{
	public RectTransform pickerTransform = null;

	List<int> pickableCharacters = null;
	int _hoverCharacterIndex = 0;

	public ControllerManager.Key _nextEnemyKey = ControllerManager.Key.LEFT;
	
	public ControllerManager.Key _prevEnemyKey = ControllerManager.Key.RIGHT;
	
	public ControllerManager.Key _pickEnemyKey = ControllerManager.Key.ACTION;
	
	public ControllerManager.Key _cancelKey = ControllerManager.Key.CANCEL;

	public override void onFocus()
	{
		pickableCharacters = new List<int>();

		for(int i=0; i<issuer.ci.enemyGroup.characters.Count; i++)
		{
			if(issuer.ci.enemyGroup.characters[i].pc.isAlive())
			{
				pickableCharacters.Add(i);
			}
		}

		pickableCharacters.Sort(delegate(int char1, int char2)
		{
			float pos1 = getEnemyObject(char1).position.x;
			float pos2 = getEnemyObject(char2).position.x;

			return pos2.CompareTo(pos1);
		});

		if(pickableCharacters.Count==0) return;

		Image img = pickerTransform.GetComponent<Image>();
		Color c = img.color;
		c.a = 1f;
		img.color = c;

		pickerTransform.position = getArrowPosition(pickableCharacters[0]);
		_hoverCharacterIndex = 0;
		hoverCharacterIndex = 0;
	}

	Coroutine coroutine = null;
	int hoverCharacterIndex
	{
		get
		{
			return _hoverCharacterIndex;
		}

		set
		{
			if(pickableCharacters.Count == 0)
			{
				_hoverCharacterIndex = 0;

				return;
			}

			if(coroutine != null)
			{
				StopCoroutine(coroutine);
			}

			while(value < 0) value += pickableCharacters.Count;

			int prev = _hoverCharacterIndex;

			_hoverCharacterIndex = value % pickableCharacters.Count;

			coroutine = StartCoroutine(selectSmooth(pickableCharacters[prev],pickableCharacters[_hoverCharacterIndex]));
		}
	}

	private RectTransform getEnemyObject(int pos)
	{
		return GameObject.FindGameObjectWithTag("BattleSceneManager").GetComponent<BattleSceneManager>().enemies[pos].GetComponent<RectTransform>();
	}

	private Vector2 getArrowPosition(int character)
	{
		RectTransform charPosition = getEnemyObject(character);

		Vector2 diff = new Vector2(0.5f,0f) - charPosition.pivot;

		Vector2 size = new Vector2(charPosition.sizeDelta.x * charPosition.localScale.x,charPosition.sizeDelta.y * charPosition.localScale.y);

		return new Vector2(charPosition.position.x,charPosition.position.y) - new Vector2(diff.x*size.x,diff.y*size.y);
	}

	public float animationTime = 0.2f;

	private IEnumerator selectSmooth(int source, int target)
	{
		float animationTime = this.animationTime;
		float beginTime = Time.time;

		Vector2 arrowPositionSource = getArrowPosition(source);
		Vector2 arrowPositionTarget = getArrowPosition(target);
		
		yield return null;
		
		while(true)
		{
			float step = Mathf.Min(1f,(Time.time - beginTime) / animationTime);

			step = AnimationTransactions.SinTransaction(step);

			Vector2 pos = (1f-step)*arrowPositionSource + step*arrowPositionTarget;

			pickerTransform.position = pos;
			
			if(step == 1f) break;
			
			yield return null;
		}
	}

	void Update()
	{
		if(!isFocused) return;

		if(controllerManager.getState(gameObject,_nextEnemyKey).isOnStateMax(State.DOWN,0))
		{
			hoverCharacterIndex++;
		}
		
		if(controllerManager.getState(gameObject,_prevEnemyKey).isOnStateMax(State.DOWN,0))
		{
			hoverCharacterIndex--;
		}
		
		if(controllerManager.getState(gameObject,_pickEnemyKey).isOnStateMax(State.DOWN,0))
		{
			select(pickableCharacters[hoverCharacterIndex]);
		}
		
		if(controllerManager.getState(gameObject,_cancelKey).isOnStateMax(State.DOWN,0))
		{
			cancel();
		}
	}
	
	public void select(int character)
	{
		Image img = pickerTransform.GetComponent<Image>();
		Color c = img.color;
		c.a = 0;
		img.color = c;
		
		TargetBuilderSingleEnemy targetBuilder = new TargetBuilderSingleEnemy();
		targetBuilder.target = issuer.ci.enemyGroup.characters[character].pc;
		builder.setTarget(targetBuilder.build());
		releaseFocus();
	}
	
	public void cancel()
	{
		Image img = pickerTransform.GetComponent<Image>();
		Color c = img.color;
		c.a = 0;
		img.color = c;

		releaseFocus();
	}
}
