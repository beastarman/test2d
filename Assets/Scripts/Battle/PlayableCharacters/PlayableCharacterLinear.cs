﻿using UnityEngine;
using System.Collections;

public class PlayableCharacterLinear : PlayableCharacter
{
	int attackFactor = 1;
	int defenseFactor = 1;
	int maxHPFactor = 1;
	int speedFactor = 1;
	
	public PlayableCharacterLinear(CacheableSprite sp, SpriteDatabase sd) : this(sp,sd,1)
	{
	}
	
	public PlayableCharacterLinear(CacheableSprite sp, SpriteDatabase sd, int _level) : this(sp,sd,_level,1,1,1,1)
	{
	}
	
	public PlayableCharacterLinear(CacheableSprite sp, SpriteDatabase sd, int _level, int aF, int dF, int hF, int sF) : base(sp,sd,_level)
	{
		attackFactor = aF;
		defenseFactor = dF;
		maxHPFactor = hF;
		speedFactor = sF;
	}
	
	public override CharacterState generateCharacterState()
	{
		return new CharacterState(attackFactor*level, defenseFactor*level, maxHPFactor*level, speedFactor*level);
	}
}
