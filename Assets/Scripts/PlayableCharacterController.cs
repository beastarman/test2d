﻿using UnityEngine;
using System.Collections;

public class PlayableCharacterController : MonoBehaviour
{
	public ControllerManager.Key _moveUpButton = ControllerManager.Key.UP;
	private State moveUp
	{
		get
		{
			return controllerManager.getState(gameObject,_moveUpButton);
		}
	}
	
	public ControllerManager.Key _moveLeftButton = ControllerManager.Key.LEFT;
	private State moveLeft
	{
		get
		{
			return controllerManager.getState(gameObject,_moveLeftButton);
		}
	}
	
	public ControllerManager.Key _moveDownButton = ControllerManager.Key.DOWN;
	private State moveDown
	{
		get
		{
			return controllerManager.getState(gameObject,_moveDownButton);
		}
	}
	
	public ControllerManager.Key _moveRightButton = ControllerManager.Key.RIGHT;
	private State moveRight
	{
		get
		{
			return controllerManager.getState(gameObject,_moveRightButton);
		}
	}
	
	public ControllerManager.Key _examineButton = ControllerManager.Key.ACTION;
	private State examine
	{
		get
		{
			return controllerManager.getState(gameObject,_examineButton);
		}
	}

	public float acceleration = 0.1f;
	public float maxSpeed = 1;
	
	public float eventCheckLength = 1;

	public LayerMask eventLayer;

	ControllerManager controllerManager;

	private Character character;

	void Start()
	{
		controllerManager = GameObject.FindGameObjectWithTag("SceneManager").GetComponent<ControllerManager>();
		character = gameObject.GetComponent<Character>();
	}

	void updateVelocity()
	{
		float localAcceleration = acceleration * Time.deltaTime;
		
		Vector2 velocity = transform.GetComponent<Rigidbody2D>().velocity;
		
		bool up = moveUp.isOnState(State.DOWN) && !moveDown.isOnState(State.DOWN);
		bool down = moveDown.isOnState(State.DOWN) && !moveUp.isOnState(State.DOWN);
		bool left = moveLeft.isOnState(State.DOWN) && !moveRight.isOnState(State.DOWN);
		bool right = moveRight.isOnState(State.DOWN) && !moveLeft.isOnState(State.DOWN);
		
		if(up)
		{
			velocity.y = Mathf.Min(velocity.y + localAcceleration,maxSpeed);
		}
		else if(down)
		{
			velocity.y = Mathf.Max(velocity.y - localAcceleration,-maxSpeed);
		}
		else
		{
			float y = velocity.y - Mathf.Sign(velocity.y)*localAcceleration;
			
			if(Mathf.Sign(y) != Mathf.Sign(velocity.y))
			{
				y = 0;
			}
			
			velocity.y = y;
		}
		
		if(right)
		{
			velocity.x = Mathf.Min(velocity.x + localAcceleration,maxSpeed);
		}
		else if(left)
		{
			velocity.x = Mathf.Max(velocity.x - localAcceleration,-maxSpeed);
		}
		else
		{
			float x = velocity.x - Mathf.Sign(velocity.x)*localAcceleration;
			
			if(Mathf.Sign(x) != Mathf.Sign(velocity.x))
			{
				x = 0;
			}
			
			velocity.x = x;
		}

		transform.GetComponent<Rigidbody2D>().velocity = velocity.normalized * Mathf.Min(maxSpeed,velocity.magnitude);
	}

	void updateFacingDirection()
	{
		Vector2 velocity = transform.GetComponent<Rigidbody2D>().velocity;

		if(velocity.magnitude>maxSpeed/4)
		{
			character.directionFacing.x = Mathf.Sign(velocity.x) * (Mathf.Abs(velocity.normalized.x)>0.1?1:0);
			character.directionFacing.y = Mathf.Sign(velocity.y) * (Mathf.Abs(velocity.normalized.y)>0.1?1:0);
			
			character.directionFacing.Normalize();
		}
	}

	Ray2D[] getEventCheckRays()
	{
		Ray2D[] rays = new Ray2D[5];

		Vector3 perpendicular = Quaternion.AngleAxis(90.0f, new Vector3(0,0,1)) * character.directionFacing;
		perpendicular = perpendicular.normalized * (transform.GetComponent<Renderer>().bounds.size.y*0.4f);

		rays[0] = new Ray2D(transform.position, character.directionFacing);
		rays[1] = new Ray2D(transform.position + perpendicular, character.directionFacing);
		rays[2] = new Ray2D(transform.position - perpendicular, character.directionFacing);
		rays[3] = new Ray2D(transform.position + (perpendicular/2), character.directionFacing);
		rays[4] = new Ray2D(transform.position - (perpendicular/2), character.directionFacing);

		return rays;
	}

	void drawEventCheckers()
	{
		foreach(Ray2D ray in getEventCheckRays())
		{
			Debug.DrawLine(ray.origin, ray.origin + ray.direction*eventCheckLength);
		}
	}

	RaycastHit2D checkForHit()
	{
		RaycastHit2D hit = new RaycastHit2D();

		foreach(Ray2D ray in getEventCheckRays())
		{
			hit = Physics2D.Raycast(ray.origin, ray.direction, eventCheckLength, eventLayer);

			if(hit.collider != null)
			{
				return hit;
			}
		}

		return hit;
	}

	void checkForEvent()
	{
		if(!examine.isOnStateMax(State.DOWN,0))
		{
			return;
		}

		checkForHit();

		RaycastHit2D hit = checkForHit();

		if(hit.collider != null)
		{
			ActionKeyEventTrigger trigger = hit.collider.GetComponent<ActionKeyEventTrigger>();

			if(trigger != null)
			{
				trigger.trigger(this.gameObject);
			}
		}
	}

	// Update is called once per frame
	void Update ()
	{
		updateVelocity();

		updateFacingDirection();

		checkForEvent();

		drawEventCheckers();
	}
}
