﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayableCharacter
{
	private CharacterState currentState = null;
	private int currentLevel = 1;

	CacheableSprite _sprite = new CacheableSprite();
	SpriteDatabase _spriteDatabase = new SpriteDatabase();
	
	public Sprite sprite
	{
		get
		{
			return _sprite.sprite;
		}
	}
	
	public SpriteDatabase spriteDatabase
	{
		get
		{
			return _spriteDatabase;
		}
	}

	public string name = "?";

	List<Modifier> modifiers = new List<Modifier>();
	
	public PlayableCharacter(string sp) : this(sp,1)
	{
	}
	
	public PlayableCharacter(string sp,int _level)
	{
		currentLevel = _level;
		_sprite = new CacheableSprite(sp);
	}

	public PlayableCharacter(CacheableSprite sp, SpriteDatabase sd) : this(sp,sd,1)
	{
	}
	
	public PlayableCharacter(CacheableSprite sp, SpriteDatabase sd,int _level)
	{
		currentLevel = _level;
		_sprite = new CacheableSprite(sp);
		_spriteDatabase = new SpriteDatabase(sd);
	}
	
	public int level
	{
		get
		{
			return currentLevel;
		}
	}
	
	public CharacterState state
	{
		get
		{
			if(currentState == null)
			{
				currentState = generateCharacterState();
			}
			
			return currentState;
		}
	}
	
	public virtual CharacterState generateCharacterState()
	{
		return new CharacterState(10*level, 20*level, 10*level, 15*level);
	}
	
	public void nextLevel()
	{
		currentLevel++;
		
		currentState = generateCharacterState();
	}

	List<CommandBuilder> _commands = null;

	public virtual List<CommandBuilder> getAvailableCommands()
	{
		if(_commands == null)
		{
			_commands = new List<CommandBuilder>();
			
			_commands.Add(new CommandDefend.Builder(this));
			_commands.Add(new CommandAttack.Builder(this));
		}

		return _commands;
	}
	
	public void addModifier(Modifier mod)
	{
		if(mod.turns > 0) modifiers.Add(mod);
	}

	public int takeHit(Hit hit)
	{
		foreach (Modifier mod in modifiers)
		{
			mod.onHit(ref hit);
		}

		return currentState.takeHit(Mathf.CeilToInt(hit.strength * (1.0f/currentState.defense)));
	}

	public void newTurn()
	{
		int i = 0;

		for(i=modifiers.Count-1; i>=0; i--)
		{
			modifiers[i].turns--;

			if(modifiers[i].turns <=0)
			{
				modifiers.RemoveAt(i);
			}
		}
	}

	public bool isAlive()
	{
		return state.curHP > 0;
	}
}
