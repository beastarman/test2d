﻿using UnityEngine;
using System.Collections;

public abstract class TargetBuilder
{
	protected static System.Random random = new System.Random();

	public abstract Target build();
}
