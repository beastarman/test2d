﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Reflection;
using System.Collections.Generic;

public class ActionTypeBehavior
{
	public static string LAST_SELECTED_OPTION = "__LastSelectedOption";
	
	InspectorEventComponent.InspectorEvent.InspectorEventAction inspectorEventAction;
	
	public ActionTypeBehavior(InspectorEventComponent.InspectorEvent.InspectorEventAction iec)
	{
		inspectorEventAction = iec;
	}
	
	public InspectorEventComponent.InspectorEvent.InspectorEventAction getInspectorEventAction()
	{
		return inspectorEventAction;
	}
	
	public bool hasAction()
	{
		return inspectorEventAction != null;
	}
	
	public virtual void resize()
	{
		Array.Resize(ref inspectorEventAction.extraString,0);
		Array.Resize(ref inspectorEventAction.extraGameObjects,0);
	}
	
	public string safeGetString(int i)
	{
		if(i >= inspectorEventAction.extraString.Length) return "";
		
		string s = inspectorEventAction.extraString[i];
		
		if(s == null)
		{
			return "";
		}
		
		return s.Trim();
	}
	
	public string safeSetString(int i, string s)
	{
		resize();
		
		if(s == null) s="";
		
		s = s.Trim();
		
		if(i < inspectorEventAction.extraString.Length)
		{
			inspectorEventAction.extraString[i] = s;
		}
		
		return s;
	}
	
	public GameObject safeGetGameObject(int i)
	{
		if(i >= inspectorEventAction.extraGameObjects.Length) return null;
		
		return (GameObject) inspectorEventAction.extraGameObjects[i];
	}
	
	public GameObject safeSetGameObject(int i, GameObject gb)
	{
		resize();
		
		if(i < inspectorEventAction.extraGameObjects.Length)
		{
			inspectorEventAction.extraGameObjects[i] = (UnityEngine.Object) gb;
		}
		
		return gb;
	}
	
	public UnityEngine.Object safeGetObject(int i)
	{
		if(i >= inspectorEventAction.extraGameObjects.Length) return null;
		
		return inspectorEventAction.extraGameObjects[i];
	}
	
	public UnityEngine.Object safeSetObject(int i, UnityEngine.Object gb)
	{
		resize();
		
		if(i < inspectorEventAction.extraGameObjects.Length)
		{
			inspectorEventAction.extraGameObjects[i] = gb;
		}
		
		return gb;
	}
	
	public override string ToString()
	{
		return "Undefined Behavior";
	}
	
	protected bool isOpenBlock = false;
	protected bool isCloseBlock = false;
	
	public virtual bool openBlock()
	{
		return isOpenBlock;
	}
	
	public virtual bool closeBlock()
	{
		return isCloseBlock;
	}
	
	protected int propertyHeight = 0;
	
	public virtual int GetPropertyHeight()
	{
		return propertyHeight;
	}
	
	public virtual bool preRun(InspectorEventComponent component)
	{
		return false;
	}
	
	public virtual bool run(InspectorEventComponent component)
	{
		return false;
	}
	
	public virtual bool update(InspectorEventComponent component)
	{
		return true;
	}
	
	#if UNITY_EDITOR
	
	public virtual void createExtraFields(ref Rect rect)
	{
		return;
	}
	
	public bool dirty = false;
	
	public void setDirty()
	{
		dirty = true;
	}
	
	#endif
}
