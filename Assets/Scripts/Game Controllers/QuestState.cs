﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class QuestState
{
	public string id = "";
	public string[] requires = new string[]{};
	public bool done = false;
	public string description  = "";

	public QuestState()
	{
	}
	
	public QuestState(string id)
	{
		this.id = id;
	}

	public QuestState(string id, string[] requires)
	{
		this.id = id;
		this.requires = requires;
	}

	public QuestState(QuestState q)
	{
		this.id = q.id;
		this.requires = (string[])q.requires.Clone();
		this.position = q.position;
		this.color = q.color;
		this.description = q.description;
	}
	
	public void fix()
	{
		List<string> req = new List<string>(requires);
		req.RemoveAll(s => s.Equals(id));
		requires = req.ToArray();
	}
	
	public Vector2 position = new Vector2();
	public Color color = Color.green;
}
