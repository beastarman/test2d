﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Memory
{
	Dictionary<string,string> dict = new Dictionary<string,string>();
	
	public Memory()
	{
	}
	
	public Memory(Memory m)
	{
		foreach(string key in m.dict.Keys)
		{
			dict.Add(key,m.dict[key]);
		}
	}
	
	public string put(string key, string value)
	{
		if(dict.ContainsKey(key))
		{
			dict.Remove(key);
		}
		
		dict.Add(key,value);
		
		return value;
	}
	
	public int put(string key, int value)
	{
		if(dict.ContainsKey(key))
		{
			dict.Remove(key);
		}
		
		dict.Add(key,value.ToString());
		
		return value;
	}
	
	public string get(string key)
	{
		if(dict.ContainsKey(key))
		{
			return dict[key];
		}
		else
		{
			return "";
		}
	}
	
	public int getInt(string key)
	{
		if(dict.ContainsKey(key))
		{
			try
			{
				return Int32.Parse(dict[key]);
			}
			catch (FormatException)
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}
	}
}
