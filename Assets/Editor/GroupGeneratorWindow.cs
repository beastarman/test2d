﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR

using UnityEditor;

#endif

public class GroupGeneratorWindow : EditorWindow
{
	[MenuItem ("Game/Group Generators &g")]
	static void Init()
	{
		EditorWindow window = EditorWindow.GetWindow (typeof (GroupGeneratorWindow), true, "Group Generators");
		
		window.wantsMouseMove = true;
	}

	bool isDirty = false;
	
	GroupGeneratorManager _manager = null;
	GroupGeneratorManager manager
	{
		get
		{
			if(_manager == null)
			{
				_manager = new GroupGeneratorManager(GlobalGameController.Instance.groupGeneratorManager);
			}
			
			return _manager;
		}
	}

	void save()
	{
		isDirty = false;
		GlobalGameController.Instance.setGroupGeneratorManager(manager);
	}

	string generateGroupGeneratorID()
	{
		string baseName = "group";
		int i = 1;

		while(true)
		{
			if(!manager.generators.ContainsKey(baseName+i))
			{
				return baseName+i;
			}

			i++;
		}
	}

	string selectedGroupGeneratorName = "";
	string newGroupGeneratorName = "";

	GroupGeneratorBase selectedgroupGenerator
	{
		get
		{
			GroupGeneratorBase groupGenerator = null;

			if(manager.generators.ContainsKey(selectedGroupGeneratorName))
			{
				groupGenerator = manager.generators[selectedGroupGeneratorName];
			}

			return groupGenerator;
		}
	}



	public string[] getAvailableGenerators()
	{
		return new string[]{GroupGeneratorSimple.classId,RandomGroupGenerator.classId};
	}

	public GroupGeneratorBase newGeneratorFromClassId(string classID)
	{
		if(classID.Equals(RandomGroupGenerator.classId))
		{
			return new RandomGroupGenerator();
		}
		else
		{
			return new GroupGeneratorSimple();
		}
	}

	void OnGUI()
	{
		EditorGUILayout.BeginVertical();
		{
			EditorGUILayout.BeginHorizontal();
			{
				if(GUILayout.Button("Save" + (isDirty? "*":""),GUI.skin.label,GUILayout.ExpandWidth(false)))
				{
					save();
				}
			}
			EditorGUILayout.EndHorizontal();
			
			Rect tabArea = EditorGUILayout.BeginHorizontal();
			{
				Color t = GUI.backgroundColor;
				GUI.backgroundColor = Color.gray;
				GUI.Box(tabArea,"");
				GUI.backgroundColor = t;
				
				foreach (string factory in manager.generators.Keys)
				{
					if(GUILayout.Button(factory,GUILayout.ExpandWidth(false)))
					{
						selectedGroupGeneratorName = factory;
						newGroupGeneratorName = selectedGroupGeneratorName;
						
						GUI.FocusControl("");
					}
				}
				
				if(GUILayout.Button("Create Generator",GUILayout.ExpandWidth(false)))
				{
					selectedGroupGeneratorName = generateGroupGeneratorID();
					newGroupGeneratorName = selectedGroupGeneratorName;
					
					manager.generators[selectedGroupGeneratorName] = new GroupGeneratorSimple();
					
					isDirty = true;
					
					GUI.FocusControl("");
				}
			}
			EditorGUILayout.EndHorizontal();

			if(selectedgroupGenerator != null)
			{
				GUILayout.Label(selectedGroupGeneratorName,EditorStyles.boldLabel);
				
				GUILayout.BeginHorizontal();
				{
					GUILayout.Label("New Id",GUILayout.Width(50));

					newGroupGeneratorName = EditorGUILayout.TextField("",newGroupGeneratorName,GUILayout.Width(180));
					
					bool validName = true;
					
					if(manager.generators.ContainsKey(newGroupGeneratorName) || newGroupGeneratorName.Trim().Length==0) validName = false;
					
					if(GUILayout.Button(validName?"Save Id":"Invalid",GUILayout.Width(70)) && validName && !selectedGroupGeneratorName.Equals(newGroupGeneratorName))
					{
						manager.generators[newGroupGeneratorName] = manager.generators[selectedGroupGeneratorName];
						manager.generators.Remove(selectedGroupGeneratorName);
						
						selectedGroupGeneratorName = newGroupGeneratorName;
						
						isDirty = true;
					}
				}
				GUILayout.EndHorizontal();

				string newType = GUIExtension.Layout_StringPopup("Generator type",selectedgroupGenerator.getID(),getAvailableGenerators(),GUILayout.Width(300));
				if(!newType.Equals(selectedgroupGenerator.getID()))
				{
					isDirty = true;
					manager.generators[selectedGroupGeneratorName] = newGeneratorFromClassId(newType);
				}

				isDirty |= selectedgroupGenerator.OnGUI(selectedGroupGeneratorName);

				GUILayout.Space(20);
				if(GUILayout.Button("Remove Generator"))
				{
					manager.generators.Remove(selectedGroupGeneratorName);

					isDirty = true;
				}
			}
			
			GUI.SetNextControlName("");
		}
		EditorGUILayout.EndVertical();
	}

	void OnDestroy()
	{
		if(isDirty)
		{
			if(EditorUtility.DisplayDialog("Save changes?","Changes were made. Do you want to save them?","Save","Discard"))
			{
				save();
			}
		}
	}
}
