﻿using UnityEngine;
using System.Collections;

public abstract class CommandBuilder
{
	protected PlayableCharacter source;

	public CommandBuilder(PlayableCharacter _source)
	{
		source = _source;
	}

	public Command build(Target target)
	{
		setTarget(target);
		return build();
	}

	public abstract void setTarget(Target target);

	public abstract Command build();

	public abstract TargetBuilder getTargetBuilder();

	public abstract string getName();

	public abstract string getDescription();
}
